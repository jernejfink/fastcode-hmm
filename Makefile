export MAKE=make
export PDFDIR=pdf
export OBJDIR=bin
export TESTDIR=tests
export PROFILEDIR=profiles
export INCLUDEDIR=include
export RESULTSDIR=results
export LIBDIR=lib
export OTHERDIR=other

all: code

code: recursive
	@echo "----------------------"
	@echo "Generating code"
	mkdir -p $(OBJDIR) $(LIBDIR)
	mkdir -p $(OBJDIR)/$(TESTDIR)
	mkdir -p $(OBJDIR)/$(PROFILEDIR)	
	mkdir -p $(PROFILEDIR)
	make -C src

clean:
	rm -rf $(OBJDIR) $(LIBDIR) $(PROFILEDIR)/*.pdf

clean_code:
	rm -rf $(OBJDIR) $(LIBDIR)

test_% : code
	@echo "----------------------"
	@echo "** Running test: $@"
	./$(OBJDIR)/$(TESTDIR)/$@.o
	@echo "** Completed test: $@"
	@echo ""


FVAR=$(PROFILEDIR)/$@_$(shell hostname)
profile_% : code
	@echo "----------------------"
	@echo "** Profiling: $@"
	@echo "** Output: ${ARGS}"
	./$(OBJDIR)/$(PROFILEDIR)/$@.o '$(FVAR)${ARGS}'
	@echo "** Completed profiling: $@"
	@echo ""

profiles : code
	@echo "----------------------"
	@echo "** Profiling: $@"
	./$(OBJDIR)/$(PROFILEDIR)/*.o '$(FVAR)'
	@echo "** Completed profiling: $@"
	@echo ""



plots : recursive
	@echo "----------------------"
	@echo "Plotting profiles"
	make -C profiles

recursive:
	true