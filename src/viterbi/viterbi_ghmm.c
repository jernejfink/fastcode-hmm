#include "viterbi.h"
#include "utility.h"

#ifndef GHMM
void viterbi_ghmm(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer) {

}
#else 

#include <ghmm/matrix.h>
#include <ghmm/rng.h>
#include <ghmm/sequence.h>
#include <ghmm/model.h>
#include <ghmm/viterbi.h>
#include <ghmm/foba.h>
#include <ghmm/obsolete.h>

void viterbi_ghmm(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer) {

	const unsigned n = sequence->size;
	const unsigned k = hmm->num_states;

	double * ep = malloc(k*k*sizeof(double));
   	double * tp = malloc(k*k*sizeof(double));
   	double * tpt = malloc(k*k*sizeof(double));

   	int *states = malloc(sizeof(int)*k);
   	for(int i = 0; i < k; ++i)
   		states[i] = i;

   	int *silent_array = calloc(k, sizeof(int));

   	double (* E)[k] = (double (*)[k]) ep;
	double (* P)[k] = (double (*)[k]) tp;
	double (* PT)[k] = (double (*)[k]) tpt;

	int *viterbi_path;

	ghmm_dmodel my_model;
	ghmm_dstate * model_states = calloc(k, sizeof(ghmm_dstate));

	for(int i = 0; i < k; ++i){
		for(int j = 0; j < k; ++j) {
			P[i][j]  = (double) hmm->transition_probability[i*k + j];
			E[i][j]  = (double) hmm->emission_probability[i*k + j];
			PT[j][i] = P[i][j];			
		}
	}


	my_model.model_type = 0;
	for(int i = 0; i < k; ++i) {
		model_states[i].pi = hmm->initial_probability[i];
		model_states[i].b  = (ep + k*i);
  		model_states[i].out_a = (tp + k*i);
  		model_states[i].in_a = (tpt + k*i);

  		model_states[i].out_states = k;
  		model_states[i].in_states = k;
  		model_states[i].in_id = states;
  		model_states[i].out_id = states;
  		model_states[i].fix = 1;
  	}
	my_model.N = k;
  	my_model.M = k;
  	my_model.s = model_states;
  	my_model.prior = -1;
  	my_model.silent = silent_array;

  	int pathlen;
  	double log_p_viterbi;

	timer_tic(timer);
	viterbi_path = ghmm_dmodel_viterbi(&my_model, (int *) sequence->emissions, n, &pathlen, &log_p_viterbi);
	timer_toc(timer);

	for(int i = 0; i < n; ++i)
		sequence->states[i] = (unsigned) viterbi_path[i];

	free(ep);
	free(tpt);
	free(tp);
	free(viterbi_path);
	free(model_states);
	free(states);
	free(silent_array);
}
#endif