#include "viterbi.h"
#include "utility.h"

#ifdef GCC
__attribute__((optimize("no-tree-vectorize")))
__attribute__((optimize("no-unroll-loops")))
#endif
void viterbi_log_fink_1(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer) {
	const unsigned n = sequence->size;
	const unsigned k = hmm->num_states;

	real_t   (* cost_matrix)[k] = (real_t (*)[k]) calloc(sizeof(real_t), k*2);
	unsigned (* policy_matrix)[k] = (unsigned (*)[k]) calloc(sizeof(unsigned), k*n);

	real_t (* P)[k] = (real_t (*)[k]) hmm->transition_probability;
	real_t (* E)[k] = (real_t (*)[k]) hmm->emission_probability;


	/** Newly added timer. @date May 10 2016 */
	timer_tic(timer);

	/** Initial probability part */
	const unsigned zN = sequence->emissions[n-1];
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif	
	for(int i = 0; i < k; ++i){
		const real_t emission_prob = hmm->emission_probability[i*k + zN];	
		cost_matrix[(n-1)%2][i] = emission_prob;
	}		

	/** Backwards part */
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif	
	for(int T = n-2; T >= 1; --T) {
		const unsigned z = sequence->emissions[T];	
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif			
		for(int i = 0; i < k; ++i) {
			const real_t emission_prob = E[i][z];

			real_t min_cost = P[i][0] + cost_matrix[(T+1)%2][0];
			int min_j = 0;
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif	
			for(int j = 1; j < k; ++j) {
				const real_t transition_prob = P[i][j];
				const real_t old_cost = cost_matrix[(T+1)%2][j];

				const real_t new_cost = old_cost + transition_prob;

				if(new_cost < min_cost) {
					min_j = j;
					min_cost = new_cost;
				}
			}

			cost_matrix[T % 2][i] = min_cost + emission_prob;
			policy_matrix[T][i] = min_j;
		}
	}

	/** Backwards part, first state */
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif	
	for(int i = 0; i < k; ++i) {
		const unsigned z = sequence->emissions[0];			
		const real_t emission_prob = E[i][z];
		const real_t initial_prob = hmm->initial_probability[i];
		real_t min_cost = P[i][0] + cost_matrix[1][0];
		int min_j = 0;
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif	
		for(int j = 1; j < k; ++j) {
			const real_t transition_prob = P[i][j];
			const real_t old_cost = cost_matrix[1][j];

			const real_t new_cost = old_cost + transition_prob;

			if(new_cost < min_cost) {
				min_j = j;
				min_cost = new_cost;
			}
		}

		cost_matrix[0][i] = min_cost + emission_prob + initial_prob;
		policy_matrix[0][i] = min_j;
	}

	/** Forward part */

	real_t min_cost = cost_matrix[0][0];
	int min_i = 0;
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif	
	for(int i = 1; i < k; ++i) {
		real_t cost = cost_matrix[0][i];
		if(cost < min_cost) {
			min_i = i;
			min_cost = cost;
		}
	}
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif
	for(int T = 0; T < n; ++T) {
		sequence->states[T] = min_i;
		min_i = policy_matrix[T][min_i];
	}
	timer_toc(timer);

	free(cost_matrix);
	free(policy_matrix);
	
}

#ifdef GCC
__attribute__((optimize("no-tree-vectorize")))
#endif
void viterbi_log_fink_1_opt(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer) {
	const unsigned n = sequence->size;
	const unsigned k = hmm->num_states;

	real_t   (* cost_matrix)[k] = (real_t (*)[k]) calloc(sizeof(real_t), k*2);
	unsigned (* policy_matrix)[k] = (unsigned (*)[k]) calloc(sizeof(unsigned), k*n);

	real_t (* P)[k] = (real_t (*)[k]) hmm->transition_probability;
	real_t (* E)[k] = (real_t (*)[k]) hmm->emission_probability;


	/** Newly added timer. @date May 10 2016 */
	timer_tic(timer);

	/** Initial probability part */
	const unsigned zN = sequence->emissions[n-1];
#ifdef ICC
#pragma novector
#pragma unroll_and_jam	
#endif	
	for(int i = 0; i < k; ++i){
		const real_t emission_prob = hmm->emission_probability[i*k + zN];	
		cost_matrix[(n-1)%2][i] = emission_prob;
	}		

	/** Backwards part */
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif	
	for(int T = n-2; T >= 1; --T) {
		const unsigned z = sequence->emissions[T];	
#ifdef ICC
#pragma novector
#pragma unroll_and_jam		
#endif			
		for(int i = 0; i < k; ++i) {
			const real_t emission_prob = E[i][z];

			real_t min_cost = P[i][0] + cost_matrix[(T+1)%2][0];
			int min_j = 0;
#ifdef ICC
#pragma novector
#pragma unroll_and_jam			
#endif	
			for(int j = 1; j < k; ++j) {
				const real_t transition_prob = P[i][j];
				const real_t old_cost = cost_matrix[(T+1)%2][j];

				const real_t new_cost = old_cost + transition_prob;

				if(new_cost < min_cost) {
					min_j = j;
					min_cost = new_cost;
				}
			}

			cost_matrix[T % 2][i] = min_cost + emission_prob;
			policy_matrix[T][i] = min_j;
		}
	}

	/** Backwards part, first state */
#ifdef ICC
#pragma novector
#pragma unroll_and_jam	
#endif	
	for(int i = 0; i < k; ++i) {
		const unsigned z = sequence->emissions[0];			
		const real_t emission_prob = E[i][z];
		const real_t initial_prob = hmm->initial_probability[i];
		real_t min_cost = P[i][0] + cost_matrix[1][0];
		int min_j = 0;
#ifdef ICC
#pragma novector
#pragma unroll_and_jam		
#endif	
		for(int j = 1; j < k; ++j) {
			const real_t transition_prob = P[i][j];
			const real_t old_cost = cost_matrix[1][j];

			const real_t new_cost = old_cost + transition_prob;

			if(new_cost < min_cost) {
				min_j = j;
				min_cost = new_cost;
			}
		}

		cost_matrix[0][i] = min_cost + emission_prob + initial_prob;
		policy_matrix[0][i] = min_j;
	}

	/** Forward part */

	real_t min_cost = cost_matrix[0][0];
	int min_i = 0;
#ifdef ICC
#pragma novector
#pragma unroll_and_jam	
#endif	
	for(int i = 1; i < k; ++i) {
		real_t cost = cost_matrix[0][i];
		if(cost < min_cost) {
			min_i = i;
			min_cost = cost;
		}
	}
#ifdef ICC
#pragma novector
#pragma unroll_and_jam	
#endif
	for(int T = 0; T < n; ++T) {
		sequence->states[T] = min_i;
		min_i = policy_matrix[T][min_i];
	}
	timer_toc(timer);

	free(cost_matrix);
	free(policy_matrix);
	
}
