#include "viterbi.h"
#include "utility.h"
#include <immintrin.h>

/** Performs vectorized version of backward-forward search
 * Note requires hmm to be aligned using @hmm_align32 before running */
#ifdef GCC
__attribute__((optimize("no-unroll-loops")))
#endif 
void viterbi_log_sam_aligned(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer) {
	const unsigned n = sequence->size;
	const unsigned k = hmm->num_states;

    real_t * mem;
    uint32_t *memi; 
    posix_memalign((void **) &mem,  8*sizeof(real_t),   2*k*sizeof(real_t));
    posix_memalign((void **) &memi, 8*sizeof(uint32_t), n*k*sizeof(uint32_t));

    real_t (* cost_matrix)[k] = (real_t (*)[k]) mem;
    uint32_t (* policy_matrix)[k] = (uint32_t (*)[k]) memi;

    real_t (* P)[k] = (real_t (*)[k]) hmm->transition_probability;
    real_t (* E)[k] = (real_t (*)[k]) hmm->emission_probability;


	/** Newly added timer. @date May 10 2016 */

	timer_tic(timer);
	/** Initial probability part */
    const unsigned zN = sequence->emissions[n-1];
#ifdef ICC
#pragma nounroll_and_jam
#endif    
    for(int i = 0; i < k; ++i){
        const real_t emission_prob = hmm->emission_probability[i*k + zN];   
        cost_matrix[(n-1)%2][i] = emission_prob;
    }   		

	/** Backwards part */

    // Store indices
    const __m256 mi1 = _mm256_castsi256_ps(_mm256_set_epi32(7, 6, 5, 4, 3, 2, 1, 0));
    const __m256 mi2 = _mm256_castsi256_ps(_mm256_set_epi32(15, 14, 13, 12, 11, 10, 9, 8));
    const __m256 mi3 = _mm256_castsi256_ps(_mm256_set_epi32(23, 22, 21, 20, 19, 18, 17, 16));
    const __m256 mi4 = _mm256_castsi256_ps(_mm256_set_epi32(31, 30, 29, 28, 27, 26, 25, 24));
    
    // INFINITY mask to add to mnc3 so that only the first three are used
//    __m256 mnc3IFTY = _mm256_set_ps(INFINITY, INFINITY, INFINITY, INFINITY, INFINITY, 0., 0., 0.);
#ifdef ICC
#pragma nounroll_and_jam
#endif
	for(int T = n-2; T >= 0; --T) {
		const unsigned z = sequence->emissions[T];
#ifdef ICC
#pragma nounroll_and_jam
#endif
		for(int i = 0; i < 32; ++i) {

			const real_t emission_prob = E[i][z];
			const real_t initial_prob = (T == 0 ? hmm->initial_probability[i] : 0);

			const __m256 mP1  = _mm256_load_ps(&P[i][0]); // transition prob
			const __m256 mC1  = _mm256_load_ps(&cost_matrix[(T+1)%2][0]); // old cost
			const __m256 mnc1 = _mm256_add_ps(mP1, mC1); // old cost + tran prob

			const __m256 mP2  = _mm256_load_ps(&P[i][8]); // transition prob
			const __m256 mC2  = _mm256_load_ps(&cost_matrix[(T+1)%2][8]); // old cost
			const __m256 mnc2 = _mm256_add_ps(mP2, mC2); // old cost + tran prob

			const __m256 mP3  = _mm256_load_ps(&P[i][16]); // transition prob
			const __m256 mC3  = _mm256_load_ps(&cost_matrix[(T+1)%2][16]); // old cost
			const __m256 mnc3 = _mm256_add_ps(mP3, mC3); // old cost + tran prob

			const __m256 mP4  = _mm256_load_ps(&P[i][24]);
			const __m256 mC4  = _mm256_load_ps(&cost_matrix[(T+1)%2][24]);
			const __m256 mnc4 = _mm256_add_ps(mP4, mC4);

			const __m256 mcmp_msk1 = _mm256_cmp_ps(mnc1,    mnc2,      0x01);
			const __m256 mcmp1     = _mm256_blendv_ps(mnc2, mnc1, mcmp_msk1);
            const __m256 mmi1      = _mm256_blendv_ps(mi2,   mi1, mcmp_msk1);

			const __m256 mcmp_msk2 = _mm256_cmp_ps(mnc3,    mnc4,      0x01);
			const __m256 mcmp2     = _mm256_blendv_ps(mnc4, mnc3, mcmp_msk2);
            const __m256 mmi2      = _mm256_blendv_ps(mi4,  mi3,  mcmp_msk2);

			const __m256 mcmp_msk3 = _mm256_cmp_ps(mcmp1,    mcmp2,      0x01);
			const __m256 mcmp3     = _mm256_blendv_ps(mcmp2, mcmp1,  mcmp_msk3);
            const __m256 mmi3      = _mm256_blendv_ps(mmi2,   mmi1,  mcmp_msk3);

			// Horizontal min
			const __m256 h1mc  = _mm256_permute2f128_ps(mcmp3, mcmp3, 0x01);
			const __m256 h1mi  = _mm256_permute2f128_ps(mmi3,   mmi3, 0x01);

			const __m256 h2cmp = _mm256_cmp_ps(mcmp3,   h1mc,  0x01);
            const __m256 h2mc  = _mm256_blendv_ps(h1mc, mcmp3, h2cmp);
            const __m256 h2mi  = _mm256_blendv_ps(h1mi, mmi3,  h2cmp);

			const __m256 h3mi  = _mm256_permute_ps(h2mi, 0xb1);
			const __m256 h3mc  = _mm256_permute_ps(h2mc, 0xb1);

			const __m256 h4cmp = _mm256_cmp_ps(h2mc, h3mc, 0x01);
			const __m256 h4mc  = _mm256_blendv_ps(h3mc, h2mc, h4cmp);
			const __m256 h4mi  = _mm256_blendv_ps(h3mi, h2mi, h4cmp);

            const __m256 h5mc  = _mm256_permute_ps(h4mc, 0x1b);
            const __m256 h5mi  = _mm256_permute_ps(h4mi, 0x1b);

            const __m256 h6cmp = _mm256_cmp_ps(h4mc, h5mc, 0x01);
            const __m256 h6mc  = _mm256_blendv_ps(h5mc, h4mc, h6cmp);
            const __m256 h6mi  = _mm256_blendv_ps(h5mi, h4mi, h6cmp);

            const real_t * min_cost_p = (const real_t *) &h6mc;            
            const int * min_j_p = (const int *) &h6mi;
            cost_matrix[T % 2][i] = min_cost_p[0] + initial_prob + emission_prob;
            policy_matrix[T][i] = min_j_p[0];
		}
	}
	timer_toc(timer);

	/** Forward part */

	real_t min_cost_sc = cost_matrix[0][0];
	int min_i = 0;
#ifdef ICC
#pragma nounroll_and_jam
#endif	
	for(int i = 1; i < k; ++i) {
		real_t cost = cost_matrix[0][i];
		if(cost < min_cost_sc) {
			min_i = i;
			min_cost_sc = cost;
		}
	}

	assert(min_i > -1 && min_i < k);
#ifdef ICC
#pragma nounroll_and_jam
#endif
	for(int T = 0; T < n; ++T) {
		sequence->states[T] = min_i;
		min_i = policy_matrix[T][min_i];
	}

    free(mem);
    free(memi);	

}

/** Performs vectorized version of backward-forward search
 * Note requires hmm to be aligned using @hmm_align32 before running */
#ifdef GCC
__attribute__((optimize("unroll-loops")))
#endif 
void viterbi_log_sam_aligned_opt(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer) {
	const unsigned n = sequence->size;
	const unsigned k = hmm->num_states;

    real_t * mem;
    uint32_t *memi; 
    posix_memalign((void **) &mem,  8*sizeof(real_t),   2*k*sizeof(real_t));
    posix_memalign((void **) &memi, 8*sizeof(uint32_t), n*k*sizeof(uint32_t));

    real_t (* cost_matrix)[k] = (real_t (*)[k]) mem;
    uint32_t (* policy_matrix)[k] = (uint32_t (*)[k]) memi;

    real_t (* P)[k] = (real_t (*)[k]) hmm->transition_probability;
    real_t (* E)[k] = (real_t (*)[k]) hmm->emission_probability;


	/** Newly added timer. @date May 10 2016 */

	timer_tic(timer);
	/** Initial probability part */
    const unsigned zN = sequence->emissions[n-1];
    for(int i = 0; i < k; ++i){
        const real_t emission_prob = hmm->emission_probability[i*k + zN];   
        cost_matrix[(n-1)%2][i] = emission_prob;
    }   		

	/** Backwards part */

    // Store indices
    const __m256 mi1 = _mm256_castsi256_ps(_mm256_set_epi32(7, 6, 5, 4, 3, 2, 1, 0));
    const __m256 mi2 = _mm256_castsi256_ps(_mm256_set_epi32(15, 14, 13, 12, 11, 10, 9, 8));
    const __m256 mi3 = _mm256_castsi256_ps(_mm256_set_epi32(23, 22, 21, 20, 19, 18, 17, 16));
    const __m256 mi4 = _mm256_castsi256_ps(_mm256_set_epi32(31, 30, 29, 28, 27, 26, 25, 24));
    
    // INFINITY mask to add to mnc3 so that only the first three are used
//    __m256 mnc3IFTY = _mm256_set_ps(INFINITY, INFINITY, INFINITY, INFINITY, INFINITY, 0., 0., 0.);
#ifdef ICC
#pragma nounroll_and_jam
#endif
	for(int T = n-2; T >= 0; --T) {
		const unsigned z = sequence->emissions[T];
#ifdef ICC
#pragma unroll_and_jam
#endif				
		for(int i = 0; i < 32; ++i) {

			const real_t emission_prob = E[i][z];
			const real_t initial_prob = (T == 0 ? hmm->initial_probability[i] : 0);

			const __m256 mP1  = _mm256_load_ps(&P[i][0]); // transition prob
			const __m256 mC1  = _mm256_load_ps(&cost_matrix[(T+1)%2][0]); // old cost
			const __m256 mnc1 = _mm256_add_ps(mP1, mC1); // old cost + tran prob

			const __m256 mP2  = _mm256_load_ps(&P[i][8]); // transition prob
			const __m256 mC2  = _mm256_load_ps(&cost_matrix[(T+1)%2][8]); // old cost
			const __m256 mnc2 = _mm256_add_ps(mP2, mC2); // old cost + tran prob

			const __m256 mP3  = _mm256_load_ps(&P[i][16]); // transition prob
			const __m256 mC3  = _mm256_load_ps(&cost_matrix[(T+1)%2][16]); // old cost
			const __m256 mnc3 = _mm256_add_ps(mP3, mC3); // old cost + tran prob

			const __m256 mP4  = _mm256_load_ps(&P[i][24]);
			const __m256 mC4  = _mm256_load_ps(&cost_matrix[(T+1)%2][24]);
			const __m256 mnc4 = _mm256_add_ps(mP4, mC4);

			const __m256 mcmp_msk1 = _mm256_cmp_ps(mnc1,    mnc2,      0x01);
			const __m256 mcmp1     = _mm256_blendv_ps(mnc2, mnc1, mcmp_msk1);
            const __m256 mmi1      = _mm256_blendv_ps(mi2,   mi1, mcmp_msk1);

			const __m256 mcmp_msk2 = _mm256_cmp_ps(mnc3,    mnc4,      0x01);
			const __m256 mcmp2     = _mm256_blendv_ps(mnc4, mnc3, mcmp_msk2);
            const __m256 mmi2      = _mm256_blendv_ps(mi4,  mi3,  mcmp_msk2);

			const __m256 mcmp_msk3 = _mm256_cmp_ps(mcmp1,    mcmp2,      0x01);
			const __m256 mcmp3     = _mm256_blendv_ps(mcmp2, mcmp1,  mcmp_msk3);
            const __m256 mmi3      = _mm256_blendv_ps(mmi2,   mmi1,  mcmp_msk3);

			// Horizontal min
			const __m256 h1mc  = _mm256_permute2f128_ps(mcmp3, mcmp3, 0x01);
			const __m256 h1mi  = _mm256_permute2f128_ps(mmi3,   mmi3, 0x01);

			const __m256 h2cmp = _mm256_cmp_ps(mcmp3,   h1mc,  0x01);
            const __m256 h2mc  = _mm256_blendv_ps(h1mc, mcmp3, h2cmp);
            const __m256 h2mi  = _mm256_blendv_ps(h1mi, mmi3,  h2cmp);

			const __m256 h3mi  = _mm256_permute_ps(h2mi, 0xb1);
			const __m256 h3mc  = _mm256_permute_ps(h2mc, 0xb1);

			const __m256 h4cmp = _mm256_cmp_ps(h2mc, h3mc, 0x01);
			const __m256 h4mc  = _mm256_blendv_ps(h3mc, h2mc, h4cmp);
			const __m256 h4mi  = _mm256_blendv_ps(h3mi, h2mi, h4cmp);

            const __m256 h5mc  = _mm256_permute_ps(h4mc, 0x1b);
            const __m256 h5mi  = _mm256_permute_ps(h4mi, 0x1b);

            const __m256 h6cmp = _mm256_cmp_ps(h4mc, h5mc, 0x01);
            const __m256 h6mc  = _mm256_blendv_ps(h5mc, h4mc, h6cmp);
            const __m256 h6mi  = _mm256_blendv_ps(h5mi, h4mi, h6cmp);

            const real_t * min_cost_p = (const real_t *) &h6mc;            
            const int * min_j_p = (const int *) &h6mi;
            cost_matrix[T % 2][i] = min_cost_p[0] + initial_prob + emission_prob;
            policy_matrix[T][i] = min_j_p[0];
		}
	}
	timer_toc(timer);

	/** Forward part */

	real_t min_cost_sc = cost_matrix[0][0];
	int min_i = 0;
#ifdef ICC
#pragma unroll_and_jam
#endif				

	for(int i = 1; i < k; ++i) {
		real_t cost = cost_matrix[0][i];
		if(cost < min_cost_sc) {
			min_i = i;
			min_cost_sc = cost;
		}
	}

	assert(min_i > -1 && min_i < k);
#ifdef ICC
#pragma unroll_and_jam
#endif					
	for(int T = 0; T < n; ++T) {
		sequence->states[T] = min_i;
		min_i = policy_matrix[T][min_i];
	}

    free(mem);
    free(memi);	

}
