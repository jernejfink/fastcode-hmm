#include "viterbi.h"
#include "utility.h"
#include <immintrin.h>

#ifdef GCC
__attribute__((optimize("no-unroll-loops")))
#endif 
void viterbi_log_mojmir_1(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer) {
	const unsigned n = sequence->size;
	//const unsigned k = hmm->num_states;
	// static fix to 32 
	const unsigned k = 32; 

    real_t * mem; 
    posix_memalign((void **) &mem, 8*sizeof(real_t), 2*k*sizeof(real_t));
    real_t (* cost_matrix)[k] = (real_t (*)[k]) mem;
    unsigned (* policy_matrix)[k] = (unsigned (*)[k]) calloc(k*n, sizeof(unsigned));

	//unsigned (* emission) =  (unsigned (*)) calloc(sizeof(unsigned),n);
	
	real_t (* P)[k] = (real_t (*)[k]) hmm->transition_probability;
	real_t (* E)[k] = (real_t (*)[k]) hmm->emission_probability_tr;

	/* Timer Start */	
	timer_tic(timer);

	const __m256 ZERO     = _mm256_set_ps(0.,   0.,  0.,  0.,  0.,  0.,  0.,  0.);
    const __m256 LADDER   = _mm256_set_ps(7.,   6.,  5.,  4.,  3.,  2.,  1.,  0.);
    const __m256 LADDER_2 = _mm256_set_ps(15., 14., 13., 12., 11., 10.,  9.,  8.);
    const __m256 LADDER_3 = _mm256_set_ps(23., 22., 21., 20., 19., 18., 17., 16.);
    const __m256 LADDER_4 = _mm256_set_ps(31., 30., 29., 28., 27., 26., 25., 24.);

	// previous values 4*8 = 32 values

	/** Initial probability part */
	const unsigned zN = sequence->emissions[n-1];
	// accessing without stride
#ifdef ICC
#pragma nounroll_and_jam
#endif
	for(int i = 0; i < k; ++i){
		const real_t emission_prob = E[zN][i];	
		cost_matrix[(n-1)%2][i] = emission_prob;
	}

	/** Backwards part */
	
	const int VV = 8; 
#ifdef ICC
#pragma nounroll_and_jam
#endif

	for(int T = n-2; T >= 1; --T) {
		
		const unsigned z = sequence->emissions[T];	
        const __m256 s1 = _mm256_load_ps(&cost_matrix[(T+1)%2][0]);
        const __m256 s2 = _mm256_load_ps(&cost_matrix[(T+1)%2][VV]);
        const __m256 s3 = _mm256_load_ps(&cost_matrix[(T+1)%2][2*VV]);
        const __m256 s4 = _mm256_load_ps(&cost_matrix[(T+1)%2][3*VV]);
#ifdef ICC
#pragma nounroll_and_jam
#endif

		for(int i = 0; i < k; i=i+VV) {

			const __m256 tp1_1= _mm256_load_ps(&P[i+0][0]);
			const __m256 tp2_1= _mm256_load_ps(&P[i+0][VV]);
			const __m256 tp3_1= _mm256_load_ps(&P[i+0][2*VV]);
			const __m256 tp4_1= _mm256_load_ps(&P[i+0][3*VV]);

			const __m256 s1_1= _mm256_add_ps(tp1_1,s1);
			const __m256 s2_1= _mm256_add_ps(tp2_1,s2);
			const __m256 s3_1= _mm256_add_ps(tp3_1,s3);
			const __m256 s4_1= _mm256_add_ps(tp4_1,s4);

			const __m256 A12_1= _mm256_min_ps(s1_1,s2_1);
			const __m256 A34_1= _mm256_min_ps(s3_1,s4_1);
			__m256 A_s_1= _mm256_min_ps(A12_1,A34_1);

			/*
			printf("=================================================================\n");
			float* v = (float*)&tp1_1;
			printf("tp: %f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			v = (float*)&s1;
			printf("s1: %f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);

			v = (float*)&A12_1;
			printf("%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			v = (float*)&A34_1;
			printf("%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			v = (float*)&A_s_1;
			printf("%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			*/
			const __m256 tp1_2= _mm256_load_ps(&P[i+1][0]);
			const __m256 tp2_2= _mm256_load_ps(&P[i+1][VV]);
			const __m256 tp3_2= _mm256_load_ps(&P[i+1][2*VV]);
			const __m256 tp4_2= _mm256_load_ps(&P[i+1][3*VV]);

			const __m256 s1_2= _mm256_add_ps(tp1_2,s1);
			const __m256 s2_2= _mm256_add_ps(tp2_2,s2);
			const __m256 s3_2= _mm256_add_ps(tp3_2,s3);
			const __m256 s4_2= _mm256_add_ps(tp4_2,s4);

			const __m256 A12_2= _mm256_min_ps(s1_2,s2_2);
			const __m256 A34_2= _mm256_min_ps(s3_2,s4_2);
			__m256 A_s_2= _mm256_min_ps(A12_2,A34_2);

			const __m256 tp1_3= _mm256_load_ps(&P[i+2][0]);
			const __m256 tp2_3= _mm256_load_ps(&P[i+2][VV]);
			const __m256 tp3_3= _mm256_load_ps(&P[i+2][2*VV]);
			const __m256 tp4_3= _mm256_load_ps(&P[i+2][3*VV]);

			const __m256 s1_3= _mm256_add_ps(tp1_3,s1);
			const __m256 s2_3= _mm256_add_ps(tp2_3,s2);
			const __m256 s3_3= _mm256_add_ps(tp3_3,s3);
			const __m256 s4_3= _mm256_add_ps(tp4_3,s4);

			const __m256 A12_3= _mm256_min_ps(s1_3,s2_3);
			const __m256 A34_3= _mm256_min_ps(s3_3,s4_3);
			__m256 A_s_3= _mm256_min_ps(A12_3,A34_3);

			const __m256 tp1_4= _mm256_load_ps(&P[i+3][0]);
			const __m256 tp2_4= _mm256_load_ps(&P[i+3][VV]);
			const __m256 tp3_4= _mm256_load_ps(&P[i+3][2*VV]);
			const __m256 tp4_4= _mm256_load_ps(&P[i+3][3*VV]);

			const __m256 s1_4= _mm256_add_ps(tp1_4,s1);
			const __m256 s2_4= _mm256_add_ps(tp2_4,s2);
			const __m256 s3_4= _mm256_add_ps(tp3_4,s3);
			const __m256 s4_4= _mm256_add_ps(tp4_4,s4);

			const __m256 A12_4= _mm256_min_ps(s1_4,s2_4);
			const __m256 A34_4= _mm256_min_ps(s3_4,s4_4);
			__m256 A_s_4= _mm256_min_ps(A12_4,A34_4);

			const __m256 tp1_5= _mm256_load_ps(&P[i+4][0]);
			const __m256 tp2_5= _mm256_load_ps(&P[i+4][VV]);
			const __m256 tp3_5= _mm256_load_ps(&P[i+4][2*VV]);
			const __m256 tp4_5= _mm256_load_ps(&P[i+4][3*VV]);

			const __m256 s1_5= _mm256_add_ps(tp1_5,s1);
			const __m256 s2_5= _mm256_add_ps(tp2_5,s2);
			const __m256 s3_5= _mm256_add_ps(tp3_5,s3);
			const __m256 s4_5= _mm256_add_ps(tp4_5,s4);

			const __m256 A12_5= _mm256_min_ps(s1_5,s2_5);
			const __m256 A34_5= _mm256_min_ps(s3_5,s4_5);
			__m256 A_s_5= _mm256_min_ps(A12_5,A34_5);

			const __m256 tp1_6= _mm256_load_ps(&P[i+5][0]);
			const __m256 tp2_6= _mm256_load_ps(&P[i+5][VV]);
			const __m256 tp3_6= _mm256_load_ps(&P[i+5][2*VV]);
			const __m256 tp4_6= _mm256_load_ps(&P[i+5][3*VV]);

			const __m256 s1_6= _mm256_add_ps(tp1_6,s1);
			const __m256 s2_6= _mm256_add_ps(tp2_6,s2);
			const __m256 s3_6= _mm256_add_ps(tp3_6,s3);
			const __m256 s4_6= _mm256_add_ps(tp4_6,s4);

			const __m256 A12_6= _mm256_min_ps(s1_6,s2_6);
			const __m256 A34_6= _mm256_min_ps(s3_6,s4_6);
			__m256 A_s_6= _mm256_min_ps(A12_6,A34_6);

			const __m256 tp1_7= _mm256_load_ps(&P[i+6][0]);
			const __m256 tp2_7= _mm256_load_ps(&P[i+6][VV]);
			const __m256 tp3_7= _mm256_load_ps(&P[i+6][2*VV]);
			const __m256 tp4_7= _mm256_load_ps(&P[i+6][3*VV]);

			const __m256 s1_7= _mm256_add_ps(tp1_7,s1);
			const __m256 s2_7= _mm256_add_ps(tp2_7,s2);
			const __m256 s3_7= _mm256_add_ps(tp3_7,s3);
			const __m256 s4_7= _mm256_add_ps(tp4_7,s4);

			const __m256 A12_7= _mm256_min_ps(s1_7,s2_7);
			const __m256 A34_7= _mm256_min_ps(s3_7,s4_7);
			__m256 A_s_7= _mm256_min_ps(A12_7,A34_7);

			const __m256 tp1_8= _mm256_load_ps(&P[i+7][0]);
			const __m256 tp2_8= _mm256_load_ps(&P[i+7][VV]);
			const __m256 tp3_8= _mm256_load_ps(&P[i+7][2*VV]);
			const __m256 tp4_8= _mm256_load_ps(&P[i+7][3*VV]);

			const __m256 s1_8= _mm256_add_ps(tp1_8,s1);
			const __m256 s2_8= _mm256_add_ps(tp2_8,s2);
			const __m256 s3_8= _mm256_add_ps(tp3_8,s3);
			const __m256 s4_8= _mm256_add_ps(tp4_8,s4);

			const __m256 A12_8= _mm256_min_ps(s1_8,s2_8);
			const __m256 A34_8= _mm256_min_ps(s3_8,s4_8);
			__m256 A_s_8= _mm256_min_ps(A12_8,A34_8);


			// ---- Horizontal Add --------


			//transpose8_ps(A_s_1,A_s_2,A_s_3,A_s_4,A_s_5,A_s_6,A_s_7,A_s_8);
			
			/** TRANSPOSE FUNCTION **/
			{
			__m256 __t0, __t1, __t2, __t3, __t4, __t5, __t6, __t7;
			__m256 __tt0, __tt1, __tt2, __tt3, __tt4, __tt5, __tt6, __tt7;
			__t0 = _mm256_unpacklo_ps(A_s_1, A_s_2);
			__t1 = _mm256_unpackhi_ps(A_s_1, A_s_2);
			__t2 = _mm256_unpacklo_ps(A_s_3, A_s_4);
			__t3 = _mm256_unpackhi_ps(A_s_3, A_s_4);
			__t4 = _mm256_unpacklo_ps(A_s_5, A_s_6);
			__t5 = _mm256_unpackhi_ps(A_s_5, A_s_6);
			__t6 = _mm256_unpacklo_ps(A_s_7, A_s_8);
			__t7 = _mm256_unpackhi_ps(A_s_7, A_s_8);
			__tt0 = _mm256_shuffle_ps(__t0,__t2,_MM_SHUFFLE(1,0,1,0));
			__tt1 = _mm256_shuffle_ps(__t0,__t2,_MM_SHUFFLE(3,2,3,2));
			__tt2 = _mm256_shuffle_ps(__t1,__t3,_MM_SHUFFLE(1,0,1,0));
			__tt3 = _mm256_shuffle_ps(__t1,__t3,_MM_SHUFFLE(3,2,3,2));
			__tt4 = _mm256_shuffle_ps(__t4,__t6,_MM_SHUFFLE(1,0,1,0));
			__tt5 = _mm256_shuffle_ps(__t4,__t6,_MM_SHUFFLE(3,2,3,2));
			__tt6 = _mm256_shuffle_ps(__t5,__t7,_MM_SHUFFLE(1,0,1,0));
			__tt7 = _mm256_shuffle_ps(__t5,__t7,_MM_SHUFFLE(3,2,3,2));
			A_s_1 = _mm256_permute2f128_ps(__tt0, __tt4, 0x20);
			A_s_2 = _mm256_permute2f128_ps(__tt1, __tt5, 0x20);
			A_s_3 = _mm256_permute2f128_ps(__tt2, __tt6, 0x20);
			A_s_4 = _mm256_permute2f128_ps(__tt3, __tt7, 0x20);
			A_s_5 = _mm256_permute2f128_ps(__tt0, __tt4, 0x31);
			A_s_6 = _mm256_permute2f128_ps(__tt1, __tt5, 0x31);
			A_s_7 = _mm256_permute2f128_ps(__tt2, __tt6, 0x31);
			A_s_8 = _mm256_permute2f128_ps(__tt3, __tt7, 0x31);
			}
			/** =================  **/



			__m256 m1 = _mm256_min_ps(A_s_1,A_s_2);
			const __m256 m2 = _mm256_min_ps(A_s_3,A_s_4);
			__m256 m3 = _mm256_min_ps(A_s_5,A_s_6);
			const __m256 m4 = _mm256_min_ps(A_s_7,A_s_8);
			m1 = _mm256_min_ps(m1,m2);
			m3 = _mm256_min_ps(m3,m4);
			m1 = _mm256_min_ps(m1,m3);

		// different blends

			__m256 m1_1 =  _mm256_permute_ps(m1,_MM_SHUFFLE(0, 0, 0, 0));
				   m1_1 =  _mm256_permute2f128_ps(m1_1,m1_1,_MM_SHUFFLE(0, 0, 0, 0));
			__m256 m1_2 =  _mm256_permute_ps(m1,_MM_SHUFFLE(1, 1, 1, 1));
				   m1_2 =  _mm256_permute2f128_ps(m1_2,m1_2,_MM_SHUFFLE(0, 0, 0, 0));
			__m256 m1_3 =  _mm256_permute_ps(m1,_MM_SHUFFLE(2, 2, 2, 2));
				   m1_3 =  _mm256_permute2f128_ps(m1_3,m1_3,_MM_SHUFFLE(0, 0, 0, 0));
			__m256 m1_4 =  _mm256_permute_ps(m1,_MM_SHUFFLE(3, 3, 3, 3));
				   m1_4 =  _mm256_permute2f128_ps(m1_4,m1_4,_MM_SHUFFLE(0, 0, 0, 0));
			__m256 m1_5 =  _mm256_permute_ps(m1,_MM_SHUFFLE(0, 0, 0, 0));
				   m1_5 =  _mm256_permute2f128_ps(m1_5,m1_5,_MM_SHUFFLE(1, 1, 1, 1));
			__m256 m1_6 =  _mm256_permute_ps(m1,_MM_SHUFFLE(1, 1, 1, 1));
				   m1_6 =  _mm256_permute2f128_ps(m1_6,m1_6,_MM_SHUFFLE(1, 1, 1, 1));
			__m256 m1_7 =  _mm256_permute_ps(m1,_MM_SHUFFLE(2, 2, 2, 2));
				   m1_7 =  _mm256_permute2f128_ps(m1_7,m1_7,_MM_SHUFFLE(1, 1, 1, 1));
			__m256 m1_8 =  _mm256_permute_ps(m1,_MM_SHUFFLE(3, 3, 3, 3));
				   m1_8 =  _mm256_permute2f128_ps(m1_8,m1_8,_MM_SHUFFLE(1, 1, 1, 1));

				// We compare m1_1 with s1_1,s2_1,s3_1,s4_1 
			const __m256 mask1_1 = _mm256_cmp_ps(m1_1,s1_1,0);
			const __m256 ind1_1 = _mm256_blendv_ps(ZERO,LADDER,mask1_1);
			const __m256 mask2_1 = _mm256_cmp_ps(m1_1,s2_1,0);
			const __m256 ind2_1 = _mm256_blendv_ps(ZERO,LADDER_2,mask2_1);
			const __m256 mask3_1 = _mm256_cmp_ps(m1_1,s3_1,0);
			const __m256 ind3_1 = _mm256_blendv_ps(ZERO,LADDER_3,mask3_1);
			const __m256 mask4_1 = _mm256_cmp_ps(m1_1,s4_1,0);
			const __m256 ind4_1 = _mm256_blendv_ps(ZERO,LADDER_4,mask4_1);

			__m256 ind1 = _mm256_max_ps(ind1_1,ind2_1);
			const __m256 temp1 = _mm256_max_ps(ind3_1,ind4_1);
			ind1 =  _mm256_max_ps(ind1,temp1);

			/*
			printf("Masking: \n");
			v = (float*)&m1_1;
			printf("mm:%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			v = (float*)&s1_1;
			printf("s1:%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			v = (float*)&mask1_1;
			printf("ms:%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			v = (float*)&ind1_1;
			printf("i1:%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			v = (float*)&ind2_1;
			printf("i2:%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			v = (float*)&ind3_1;
			printf("i3:%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			v = (float*)&ind4_1;
			printf("i3:%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			*/

			const __m256 mask1_2 = _mm256_cmp_ps(m1_2,s1_2,0);
			const __m256 ind1_2 = _mm256_blendv_ps(ZERO,LADDER,mask1_2);
			const __m256 mask2_2 = _mm256_cmp_ps(m1_2,s2_2,0);
			const __m256 ind2_2 = _mm256_blendv_ps(ZERO,LADDER_2,mask2_2);
			const __m256 mask3_2 = _mm256_cmp_ps(m1_2,s3_2,0);
			const __m256 ind3_2 = _mm256_blendv_ps(ZERO,LADDER_3,mask3_2);
			const __m256 mask4_2 = _mm256_cmp_ps(m1_2,s4_2,0);
			const __m256 ind4_2 = _mm256_blendv_ps(ZERO,LADDER_4,mask4_2);

			__m256 ind2 = _mm256_max_ps(ind1_2,ind2_2);
			const __m256 temp2 = _mm256_max_ps(ind3_2,ind4_2);
			ind2 =  _mm256_max_ps(ind2,temp2);

			const __m256 mask1_3 = _mm256_cmp_ps(m1_3,s1_3,0);
			const __m256 ind1_3 = _mm256_blendv_ps(ZERO,LADDER,mask1_3);
			const __m256 mask2_3 = _mm256_cmp_ps(m1_3,s2_3,0);
			const __m256 ind2_3 = _mm256_blendv_ps(ZERO,LADDER_2,mask2_3);
			const __m256 mask3_3 = _mm256_cmp_ps(m1_3,s3_3,0);
			const __m256 ind3_3 = _mm256_blendv_ps(ZERO,LADDER_3,mask3_3);
			const __m256 mask4_3 = _mm256_cmp_ps(m1_3,s4_3,0);
			const __m256 ind4_3 = _mm256_blendv_ps(ZERO,LADDER_4,mask4_3);

			__m256 ind3 = _mm256_max_ps(ind1_3,ind2_3);
			const __m256 temp3 = _mm256_max_ps(ind3_3,ind4_3);
			ind3 =  _mm256_max_ps(ind3,temp3);

			const __m256 mask1_4 = _mm256_cmp_ps(m1_4,s1_4,0);
			const __m256 ind1_4 = _mm256_blendv_ps(ZERO,LADDER,mask1_4);
			const __m256 mask2_4 = _mm256_cmp_ps(m1_4,s2_4,0);
			const __m256 ind2_4 = _mm256_blendv_ps(ZERO,LADDER_2,mask2_4);
			const __m256 mask3_4 = _mm256_cmp_ps(m1_4,s3_4,0);
			const __m256 ind3_4 = _mm256_blendv_ps(ZERO,LADDER_3,mask3_4);
			const __m256 mask4_4 = _mm256_cmp_ps(m1_4,s4_4,0);
			const __m256 ind4_4 = _mm256_blendv_ps(ZERO,LADDER_4,mask4_4);

			__m256 ind4 = _mm256_max_ps(ind1_4,ind2_4);
			const __m256 temp4 = _mm256_max_ps(ind3_4,ind4_4);
			ind4 =  _mm256_max_ps(ind4,temp4);

			const __m256 mask1_5 = _mm256_cmp_ps(m1_5,s1_5,0);
			const __m256 ind1_5 = _mm256_blendv_ps(ZERO,LADDER,mask1_5);
			const __m256 mask2_5 = _mm256_cmp_ps(m1_5,s2_5,0);
			const __m256 ind2_5 = _mm256_blendv_ps(ZERO,LADDER_2,mask2_5);
			const __m256 mask3_5 = _mm256_cmp_ps(m1_5,s3_5,0);
			const __m256 ind3_5 = _mm256_blendv_ps(ZERO,LADDER_3,mask3_5);
			const __m256 mask4_5 = _mm256_cmp_ps(m1_5,s4_5,0);
			const __m256 ind4_5 = _mm256_blendv_ps(ZERO,LADDER_4,mask4_5);

			__m256 ind5 = _mm256_max_ps(ind1_5,ind2_5);
			const __m256 temp5 = _mm256_max_ps(ind3_5,ind4_5);
			ind5 =  _mm256_max_ps(ind5,temp5);

			const __m256 mask1_6 = _mm256_cmp_ps(m1_6,s1_6,0);
			const __m256 ind1_6 = _mm256_blendv_ps(ZERO,LADDER,mask1_6);
			const __m256 mask2_6 = _mm256_cmp_ps(m1_6,s2_6,0);
			const __m256 ind2_6 = _mm256_blendv_ps(ZERO,LADDER_2,mask2_6);
			const __m256 mask3_6 = _mm256_cmp_ps(m1_6,s3_6,0);
			const __m256 ind3_6 = _mm256_blendv_ps(ZERO,LADDER_3,mask3_6);
			const __m256 mask4_6 = _mm256_cmp_ps(m1_6,s4_6,0);
			const __m256 ind4_6 = _mm256_blendv_ps(ZERO,LADDER_4,mask4_6);

			__m256 ind6 = _mm256_max_ps(ind1_6,ind2_6);
			const __m256 temp6 = _mm256_max_ps(ind3_6,ind4_6);
			ind6 =  _mm256_max_ps(ind6,temp6);

			const __m256 mask1_7 = _mm256_cmp_ps(m1_7,s1_7,0);
			const __m256 ind1_7 = _mm256_blendv_ps(ZERO,LADDER,mask1_7);
			const __m256 mask2_7 = _mm256_cmp_ps(m1_7,s2_7,0);
			const __m256 ind2_7 = _mm256_blendv_ps(ZERO,LADDER_2,mask2_7);
			const __m256 mask3_7 = _mm256_cmp_ps(m1_7,s3_7,0);
			const __m256 ind3_7 = _mm256_blendv_ps(ZERO,LADDER_3,mask3_7);
			const __m256 mask4_7 = _mm256_cmp_ps(m1_7,s4_7,0);
			const __m256 ind4_7 = _mm256_blendv_ps(ZERO,LADDER_4,mask4_7);

			__m256 ind7 = _mm256_max_ps(ind1_7,ind2_7);
			const __m256 temp7 = _mm256_max_ps(ind3_7,ind4_7);
			ind7 =  _mm256_max_ps(ind7,temp7);

			const __m256 mask1_8 = _mm256_cmp_ps(m1_8,s1_8,0);
			const __m256 ind1_8 = _mm256_blendv_ps(ZERO,LADDER,mask1_8);
			const __m256 mask2_8 = _mm256_cmp_ps(m1_8,s2_8,0);
			const __m256 ind2_8 = _mm256_blendv_ps(ZERO,LADDER_2,mask2_8);
			const __m256 mask3_8 = _mm256_cmp_ps(m1_8,s3_8,0);
			const __m256 ind3_8 = _mm256_blendv_ps(ZERO,LADDER_3,mask3_8);
			const __m256 mask4_8 = _mm256_cmp_ps(m1_8,s4_8,0);
			const __m256 ind4_8 = _mm256_blendv_ps(ZERO,LADDER_4,mask4_8);

			__m256 ind8 = _mm256_max_ps(ind1_8,ind2_8);
			const __m256 temp8 = _mm256_max_ps(ind3_8,ind4_8);
			ind8 =  _mm256_max_ps(ind8,temp8);



			// transpose again
			//transpose8_ps(ind1,ind2,ind3,ind4,ind5,ind6,ind7,ind8);

			/**     TRANSPOSE    **/

			__m256 __t0, __t1, __t2, __t3, __t4, __t5, __t6, __t7;
			__m256 __tt0, __tt1, __tt2, __tt3, __tt4, __tt5, __tt6, __tt7;
			__t0 = _mm256_unpacklo_ps(ind1, ind2);
			__t1 = _mm256_unpackhi_ps(ind1, ind2);
			__t2 = _mm256_unpacklo_ps(ind3, ind4);
			__t3 = _mm256_unpackhi_ps(ind3, ind4);
			__t4 = _mm256_unpacklo_ps(ind5, ind6);
			__t5 = _mm256_unpackhi_ps(ind5, ind6);
			__t6 = _mm256_unpacklo_ps(ind7, ind8);
			__t7 = _mm256_unpackhi_ps(ind7, ind8);
			__tt0 = _mm256_shuffle_ps(__t0,__t2,_MM_SHUFFLE(1,0,1,0));
			__tt1 = _mm256_shuffle_ps(__t0,__t2,_MM_SHUFFLE(3,2,3,2));
			__tt2 = _mm256_shuffle_ps(__t1,__t3,_MM_SHUFFLE(1,0,1,0));
			__tt3 = _mm256_shuffle_ps(__t1,__t3,_MM_SHUFFLE(3,2,3,2));
			__tt4 = _mm256_shuffle_ps(__t4,__t6,_MM_SHUFFLE(1,0,1,0));
			__tt5 = _mm256_shuffle_ps(__t4,__t6,_MM_SHUFFLE(3,2,3,2));
			__tt6 = _mm256_shuffle_ps(__t5,__t7,_MM_SHUFFLE(1,0,1,0));
			__tt7 = _mm256_shuffle_ps(__t5,__t7,_MM_SHUFFLE(3,2,3,2));
			ind1 = _mm256_permute2f128_ps(__tt0, __tt4, 0x20);
			ind2 = _mm256_permute2f128_ps(__tt1, __tt5, 0x20);
			ind3 = _mm256_permute2f128_ps(__tt2, __tt6, 0x20);
			ind4 = _mm256_permute2f128_ps(__tt3, __tt7, 0x20);
			ind5 = _mm256_permute2f128_ps(__tt0, __tt4, 0x31);
			ind6 = _mm256_permute2f128_ps(__tt1, __tt5, 0x31);
			ind7 = _mm256_permute2f128_ps(__tt2, __tt6, 0x31);
			ind8 = _mm256_permute2f128_ps(__tt3, __tt7, 0x31);

			/** ================ **/
			__m256 c1 = _mm256_max_ps(ind1,ind2);
			const __m256 c2 = _mm256_max_ps(ind3,ind4);
			__m256 c3 = _mm256_max_ps(ind5,ind6);
			const __m256 c4 = _mm256_max_ps(ind7,ind8);
			c1 = _mm256_max_ps(c1,c2);
			c3 = _mm256_max_ps(c3,c4);
			c1 = _mm256_max_ps(c1,c3);

			// ----------------------------
			
			const __m256 emission_prob = _mm256_load_ps(&E[z][i]);
			const __m256 sum = _mm256_add_ps(emission_prob,m1);
			_mm256_storeu_ps(&cost_matrix[T % 2][i],sum);
			

			const __m256i path = _mm256_cvttps_epi32(c1);
			_mm256_storeu_si256((__m256i *)&policy_matrix[T][i],path);
  			
		}

	}


	//-------------
	/** Backwards part, first state */
#ifdef ICC
#pragma nounroll_and_jam
#endif	
	for(int i = 0; i < k; ++i) {
		const unsigned z = sequence->emissions[0];			
		const real_t emission_prob = E[z][i];
		const real_t initial_prob = hmm->initial_probability[i];
		real_t min_cost = P[i][0] + cost_matrix[1][0];
		int min_j = 0;

		for(int j = 1; j < k; ++j) {
			const real_t transition_prob = P[i][j];
			const real_t old_cost = cost_matrix[1][j];

			const real_t new_cost = old_cost + transition_prob;

			if(new_cost < min_cost) {
				min_j = j;
				min_cost = new_cost;
			}
		}

		cost_matrix[0][i] = min_cost + emission_prob + initial_prob;
		policy_matrix[0][i] = min_j;
	}

	/** Forward part */

	real_t min_cost = cost_matrix[0][0];
	int min_i = 0;
#ifdef ICC
#pragma nounroll_and_jam
#endif	
	for(int i = 1; i < k; ++i) {
		real_t cost = cost_matrix[0][i];
		if(cost < min_cost) {
			min_i = i;
			min_cost = cost;
		}
	}
#ifdef ICC
#pragma nounroll_and_jam
#endif
	for(int T = 0; T < n; ++T) {
		sequence->states[T] = min_i;
		min_i = policy_matrix[T][min_i];
	}
	//---------------------------------------

    timer_toc(timer);

	free(mem);
	free(policy_matrix);	
}


void viterbi_log_mojmir_1_opt(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer) {
	const unsigned n = sequence->size;
	//const unsigned k = hmm->num_states;
	// static fix to 32 
	const unsigned k = 32; 

    real_t * mem; 
    posix_memalign((void **) &mem, 8*sizeof(real_t), 2*k*sizeof(real_t));
    real_t (* cost_matrix)[k] = (real_t (*)[k]) mem;
    unsigned (* policy_matrix)[k] = (unsigned (*)[k]) calloc(k*n, sizeof(unsigned));

	//unsigned (* emission) =  (unsigned (*)) calloc(sizeof(unsigned),n);
	
	real_t (* P)[k] = (real_t (*)[k]) hmm->transition_probability;
	real_t (* E)[k] = (real_t (*)[k]) hmm->emission_probability_tr;

	/* Timer Start */	
	timer_tic(timer);

	const __m256 ZERO     = _mm256_set_ps(0.,   0.,  0.,  0.,  0.,  0.,  0.,  0.);
    const __m256 LADDER   = _mm256_set_ps(7.,   6.,  5.,  4.,  3.,  2.,  1.,  0.);
    const __m256 LADDER_2 = _mm256_set_ps(15., 14., 13., 12., 11., 10.,  9.,  8.);
    const __m256 LADDER_3 = _mm256_set_ps(23., 22., 21., 20., 19., 18., 17., 16.);
    const __m256 LADDER_4 = _mm256_set_ps(31., 30., 29., 28., 27., 26., 25., 24.);

	// previous values 4*8 = 32 values

	/** Initial probability part */
	const unsigned zN = sequence->emissions[n-1];
	// accessing without stride
	for(int i = 0; i < k; ++i){
		const real_t emission_prob = E[zN][i];	
		cost_matrix[(n-1)%2][i] = emission_prob;
	}

	/** Backwards part */
	
	const int VV = 8; 
#ifdef ICC
#pragma nounroll_and_jam
#endif	
	for(int T = n-2; T >= 1; --T) {
		
		const unsigned z = sequence->emissions[T];	
        const __m256 s1 = _mm256_load_ps(&cost_matrix[(T+1)%2][0]);
        const __m256 s2 = _mm256_load_ps(&cost_matrix[(T+1)%2][VV]);
        const __m256 s3 = _mm256_load_ps(&cost_matrix[(T+1)%2][2*VV]);
        const __m256 s4 = _mm256_load_ps(&cost_matrix[(T+1)%2][3*VV]);
#ifdef ICC
#pragma unroll_and_jam
#endif	
		for(int i = 0; i < k; i=i+VV) {

			const __m256 tp1_1= _mm256_load_ps(&P[i+0][0]);
			const __m256 tp2_1= _mm256_load_ps(&P[i+0][VV]);
			const __m256 tp3_1= _mm256_load_ps(&P[i+0][2*VV]);
			const __m256 tp4_1= _mm256_load_ps(&P[i+0][3*VV]);

			const __m256 s1_1= _mm256_add_ps(tp1_1,s1);
			const __m256 s2_1= _mm256_add_ps(tp2_1,s2);
			const __m256 s3_1= _mm256_add_ps(tp3_1,s3);
			const __m256 s4_1= _mm256_add_ps(tp4_1,s4);

			const __m256 A12_1= _mm256_min_ps(s1_1,s2_1);
			const __m256 A34_1= _mm256_min_ps(s3_1,s4_1);
			__m256 A_s_1= _mm256_min_ps(A12_1,A34_1);

			/*
			printf("=================================================================\n");
			float* v = (float*)&tp1_1;
			printf("tp: %f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			v = (float*)&s1;
			printf("s1: %f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);

			v = (float*)&A12_1;
			printf("%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			v = (float*)&A34_1;
			printf("%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			v = (float*)&A_s_1;
			printf("%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			*/
			const __m256 tp1_2= _mm256_load_ps(&P[i+1][0]);
			const __m256 tp2_2= _mm256_load_ps(&P[i+1][VV]);
			const __m256 tp3_2= _mm256_load_ps(&P[i+1][2*VV]);
			const __m256 tp4_2= _mm256_load_ps(&P[i+1][3*VV]);

			const __m256 s1_2= _mm256_add_ps(tp1_2,s1);
			const __m256 s2_2= _mm256_add_ps(tp2_2,s2);
			const __m256 s3_2= _mm256_add_ps(tp3_2,s3);
			const __m256 s4_2= _mm256_add_ps(tp4_2,s4);

			const __m256 A12_2= _mm256_min_ps(s1_2,s2_2);
			const __m256 A34_2= _mm256_min_ps(s3_2,s4_2);
			__m256 A_s_2= _mm256_min_ps(A12_2,A34_2);

			const __m256 tp1_3= _mm256_load_ps(&P[i+2][0]);
			const __m256 tp2_3= _mm256_load_ps(&P[i+2][VV]);
			const __m256 tp3_3= _mm256_load_ps(&P[i+2][2*VV]);
			const __m256 tp4_3= _mm256_load_ps(&P[i+2][3*VV]);

			const __m256 s1_3= _mm256_add_ps(tp1_3,s1);
			const __m256 s2_3= _mm256_add_ps(tp2_3,s2);
			const __m256 s3_3= _mm256_add_ps(tp3_3,s3);
			const __m256 s4_3= _mm256_add_ps(tp4_3,s4);

			const __m256 A12_3= _mm256_min_ps(s1_3,s2_3);
			const __m256 A34_3= _mm256_min_ps(s3_3,s4_3);
			__m256 A_s_3= _mm256_min_ps(A12_3,A34_3);

			const __m256 tp1_4= _mm256_load_ps(&P[i+3][0]);
			const __m256 tp2_4= _mm256_load_ps(&P[i+3][VV]);
			const __m256 tp3_4= _mm256_load_ps(&P[i+3][2*VV]);
			const __m256 tp4_4= _mm256_load_ps(&P[i+3][3*VV]);

			const __m256 s1_4= _mm256_add_ps(tp1_4,s1);
			const __m256 s2_4= _mm256_add_ps(tp2_4,s2);
			const __m256 s3_4= _mm256_add_ps(tp3_4,s3);
			const __m256 s4_4= _mm256_add_ps(tp4_4,s4);

			const __m256 A12_4= _mm256_min_ps(s1_4,s2_4);
			const __m256 A34_4= _mm256_min_ps(s3_4,s4_4);
			__m256 A_s_4= _mm256_min_ps(A12_4,A34_4);

			const __m256 tp1_5= _mm256_load_ps(&P[i+4][0]);
			const __m256 tp2_5= _mm256_load_ps(&P[i+4][VV]);
			const __m256 tp3_5= _mm256_load_ps(&P[i+4][2*VV]);
			const __m256 tp4_5= _mm256_load_ps(&P[i+4][3*VV]);

			const __m256 s1_5= _mm256_add_ps(tp1_5,s1);
			const __m256 s2_5= _mm256_add_ps(tp2_5,s2);
			const __m256 s3_5= _mm256_add_ps(tp3_5,s3);
			const __m256 s4_5= _mm256_add_ps(tp4_5,s4);

			const __m256 A12_5= _mm256_min_ps(s1_5,s2_5);
			const __m256 A34_5= _mm256_min_ps(s3_5,s4_5);
			__m256 A_s_5= _mm256_min_ps(A12_5,A34_5);

			const __m256 tp1_6= _mm256_load_ps(&P[i+5][0]);
			const __m256 tp2_6= _mm256_load_ps(&P[i+5][VV]);
			const __m256 tp3_6= _mm256_load_ps(&P[i+5][2*VV]);
			const __m256 tp4_6= _mm256_load_ps(&P[i+5][3*VV]);

			const __m256 s1_6= _mm256_add_ps(tp1_6,s1);
			const __m256 s2_6= _mm256_add_ps(tp2_6,s2);
			const __m256 s3_6= _mm256_add_ps(tp3_6,s3);
			const __m256 s4_6= _mm256_add_ps(tp4_6,s4);

			const __m256 A12_6= _mm256_min_ps(s1_6,s2_6);
			const __m256 A34_6= _mm256_min_ps(s3_6,s4_6);
			__m256 A_s_6= _mm256_min_ps(A12_6,A34_6);

			const __m256 tp1_7= _mm256_load_ps(&P[i+6][0]);
			const __m256 tp2_7= _mm256_load_ps(&P[i+6][VV]);
			const __m256 tp3_7= _mm256_load_ps(&P[i+6][2*VV]);
			const __m256 tp4_7= _mm256_load_ps(&P[i+6][3*VV]);

			const __m256 s1_7= _mm256_add_ps(tp1_7,s1);
			const __m256 s2_7= _mm256_add_ps(tp2_7,s2);
			const __m256 s3_7= _mm256_add_ps(tp3_7,s3);
			const __m256 s4_7= _mm256_add_ps(tp4_7,s4);

			const __m256 A12_7= _mm256_min_ps(s1_7,s2_7);
			const __m256 A34_7= _mm256_min_ps(s3_7,s4_7);
			__m256 A_s_7= _mm256_min_ps(A12_7,A34_7);

			const __m256 tp1_8= _mm256_load_ps(&P[i+7][0]);
			const __m256 tp2_8= _mm256_load_ps(&P[i+7][VV]);
			const __m256 tp3_8= _mm256_load_ps(&P[i+7][2*VV]);
			const __m256 tp4_8= _mm256_load_ps(&P[i+7][3*VV]);

			const __m256 s1_8= _mm256_add_ps(tp1_8,s1);
			const __m256 s2_8= _mm256_add_ps(tp2_8,s2);
			const __m256 s3_8= _mm256_add_ps(tp3_8,s3);
			const __m256 s4_8= _mm256_add_ps(tp4_8,s4);

			const __m256 A12_8= _mm256_min_ps(s1_8,s2_8);
			const __m256 A34_8= _mm256_min_ps(s3_8,s4_8);
			__m256 A_s_8= _mm256_min_ps(A12_8,A34_8);


			// ---- Horizontal Add --------


			//transpose8_ps(A_s_1,A_s_2,A_s_3,A_s_4,A_s_5,A_s_6,A_s_7,A_s_8);
			
			/** TRANSPOSE FUNCTION **/
			{
			__m256 __t0, __t1, __t2, __t3, __t4, __t5, __t6, __t7;
			__m256 __tt0, __tt1, __tt2, __tt3, __tt4, __tt5, __tt6, __tt7;
			__t0 = _mm256_unpacklo_ps(A_s_1, A_s_2);
			__t1 = _mm256_unpackhi_ps(A_s_1, A_s_2);
			__t2 = _mm256_unpacklo_ps(A_s_3, A_s_4);
			__t3 = _mm256_unpackhi_ps(A_s_3, A_s_4);
			__t4 = _mm256_unpacklo_ps(A_s_5, A_s_6);
			__t5 = _mm256_unpackhi_ps(A_s_5, A_s_6);
			__t6 = _mm256_unpacklo_ps(A_s_7, A_s_8);
			__t7 = _mm256_unpackhi_ps(A_s_7, A_s_8);
			__tt0 = _mm256_shuffle_ps(__t0,__t2,_MM_SHUFFLE(1,0,1,0));
			__tt1 = _mm256_shuffle_ps(__t0,__t2,_MM_SHUFFLE(3,2,3,2));
			__tt2 = _mm256_shuffle_ps(__t1,__t3,_MM_SHUFFLE(1,0,1,0));
			__tt3 = _mm256_shuffle_ps(__t1,__t3,_MM_SHUFFLE(3,2,3,2));
			__tt4 = _mm256_shuffle_ps(__t4,__t6,_MM_SHUFFLE(1,0,1,0));
			__tt5 = _mm256_shuffle_ps(__t4,__t6,_MM_SHUFFLE(3,2,3,2));
			__tt6 = _mm256_shuffle_ps(__t5,__t7,_MM_SHUFFLE(1,0,1,0));
			__tt7 = _mm256_shuffle_ps(__t5,__t7,_MM_SHUFFLE(3,2,3,2));
			A_s_1 = _mm256_permute2f128_ps(__tt0, __tt4, 0x20);
			A_s_2 = _mm256_permute2f128_ps(__tt1, __tt5, 0x20);
			A_s_3 = _mm256_permute2f128_ps(__tt2, __tt6, 0x20);
			A_s_4 = _mm256_permute2f128_ps(__tt3, __tt7, 0x20);
			A_s_5 = _mm256_permute2f128_ps(__tt0, __tt4, 0x31);
			A_s_6 = _mm256_permute2f128_ps(__tt1, __tt5, 0x31);
			A_s_7 = _mm256_permute2f128_ps(__tt2, __tt6, 0x31);
			A_s_8 = _mm256_permute2f128_ps(__tt3, __tt7, 0x31);
			}
			/** =================  **/



			__m256 m1 = _mm256_min_ps(A_s_1,A_s_2);
			const __m256 m2 = _mm256_min_ps(A_s_3,A_s_4);
			__m256 m3 = _mm256_min_ps(A_s_5,A_s_6);
			const __m256 m4 = _mm256_min_ps(A_s_7,A_s_8);
			m1 = _mm256_min_ps(m1,m2);
			m3 = _mm256_min_ps(m3,m4);
			m1 = _mm256_min_ps(m1,m3);

		// different blends

			__m256 m1_1 =  _mm256_permute_ps(m1,_MM_SHUFFLE(0, 0, 0, 0));
				   m1_1 =  _mm256_permute2f128_ps(m1_1,m1_1,_MM_SHUFFLE(0, 0, 0, 0));
			__m256 m1_2 =  _mm256_permute_ps(m1,_MM_SHUFFLE(1, 1, 1, 1));
				   m1_2 =  _mm256_permute2f128_ps(m1_2,m1_2,_MM_SHUFFLE(0, 0, 0, 0));
			__m256 m1_3 =  _mm256_permute_ps(m1,_MM_SHUFFLE(2, 2, 2, 2));
				   m1_3 =  _mm256_permute2f128_ps(m1_3,m1_3,_MM_SHUFFLE(0, 0, 0, 0));
			__m256 m1_4 =  _mm256_permute_ps(m1,_MM_SHUFFLE(3, 3, 3, 3));
				   m1_4 =  _mm256_permute2f128_ps(m1_4,m1_4,_MM_SHUFFLE(0, 0, 0, 0));
			__m256 m1_5 =  _mm256_permute_ps(m1,_MM_SHUFFLE(0, 0, 0, 0));
				   m1_5 =  _mm256_permute2f128_ps(m1_5,m1_5,_MM_SHUFFLE(1, 1, 1, 1));
			__m256 m1_6 =  _mm256_permute_ps(m1,_MM_SHUFFLE(1, 1, 1, 1));
				   m1_6 =  _mm256_permute2f128_ps(m1_6,m1_6,_MM_SHUFFLE(1, 1, 1, 1));
			__m256 m1_7 =  _mm256_permute_ps(m1,_MM_SHUFFLE(2, 2, 2, 2));
				   m1_7 =  _mm256_permute2f128_ps(m1_7,m1_7,_MM_SHUFFLE(1, 1, 1, 1));
			__m256 m1_8 =  _mm256_permute_ps(m1,_MM_SHUFFLE(3, 3, 3, 3));
				   m1_8 =  _mm256_permute2f128_ps(m1_8,m1_8,_MM_SHUFFLE(1, 1, 1, 1));

				// We compare m1_1 with s1_1,s2_1,s3_1,s4_1 
			const __m256 mask1_1 = _mm256_cmp_ps(m1_1,s1_1,0);
			const __m256 ind1_1 = _mm256_blendv_ps(ZERO,LADDER,mask1_1);
			const __m256 mask2_1 = _mm256_cmp_ps(m1_1,s2_1,0);
			const __m256 ind2_1 = _mm256_blendv_ps(ZERO,LADDER_2,mask2_1);
			const __m256 mask3_1 = _mm256_cmp_ps(m1_1,s3_1,0);
			const __m256 ind3_1 = _mm256_blendv_ps(ZERO,LADDER_3,mask3_1);
			const __m256 mask4_1 = _mm256_cmp_ps(m1_1,s4_1,0);
			const __m256 ind4_1 = _mm256_blendv_ps(ZERO,LADDER_4,mask4_1);

			__m256 ind1 = _mm256_max_ps(ind1_1,ind2_1);
			const __m256 temp1 = _mm256_max_ps(ind3_1,ind4_1);
			ind1 =  _mm256_max_ps(ind1,temp1);

			/*
			printf("Masking: \n");
			v = (float*)&m1_1;
			printf("mm:%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			v = (float*)&s1_1;
			printf("s1:%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			v = (float*)&mask1_1;
			printf("ms:%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			v = (float*)&ind1_1;
			printf("i1:%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			v = (float*)&ind2_1;
			printf("i2:%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			v = (float*)&ind3_1;
			printf("i3:%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			v = (float*)&ind4_1;
			printf("i3:%f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7]);
			*/

			const __m256 mask1_2 = _mm256_cmp_ps(m1_2,s1_2,0);
			const __m256 ind1_2 = _mm256_blendv_ps(ZERO,LADDER,mask1_2);
			const __m256 mask2_2 = _mm256_cmp_ps(m1_2,s2_2,0);
			const __m256 ind2_2 = _mm256_blendv_ps(ZERO,LADDER_2,mask2_2);
			const __m256 mask3_2 = _mm256_cmp_ps(m1_2,s3_2,0);
			const __m256 ind3_2 = _mm256_blendv_ps(ZERO,LADDER_3,mask3_2);
			const __m256 mask4_2 = _mm256_cmp_ps(m1_2,s4_2,0);
			const __m256 ind4_2 = _mm256_blendv_ps(ZERO,LADDER_4,mask4_2);

			__m256 ind2 = _mm256_max_ps(ind1_2,ind2_2);
			const __m256 temp2 = _mm256_max_ps(ind3_2,ind4_2);
			ind2 =  _mm256_max_ps(ind2,temp2);

			const __m256 mask1_3 = _mm256_cmp_ps(m1_3,s1_3,0);
			const __m256 ind1_3 = _mm256_blendv_ps(ZERO,LADDER,mask1_3);
			const __m256 mask2_3 = _mm256_cmp_ps(m1_3,s2_3,0);
			const __m256 ind2_3 = _mm256_blendv_ps(ZERO,LADDER_2,mask2_3);
			const __m256 mask3_3 = _mm256_cmp_ps(m1_3,s3_3,0);
			const __m256 ind3_3 = _mm256_blendv_ps(ZERO,LADDER_3,mask3_3);
			const __m256 mask4_3 = _mm256_cmp_ps(m1_3,s4_3,0);
			const __m256 ind4_3 = _mm256_blendv_ps(ZERO,LADDER_4,mask4_3);

			__m256 ind3 = _mm256_max_ps(ind1_3,ind2_3);
			const __m256 temp3 = _mm256_max_ps(ind3_3,ind4_3);
			ind3 =  _mm256_max_ps(ind3,temp3);

			const __m256 mask1_4 = _mm256_cmp_ps(m1_4,s1_4,0);
			const __m256 ind1_4 = _mm256_blendv_ps(ZERO,LADDER,mask1_4);
			const __m256 mask2_4 = _mm256_cmp_ps(m1_4,s2_4,0);
			const __m256 ind2_4 = _mm256_blendv_ps(ZERO,LADDER_2,mask2_4);
			const __m256 mask3_4 = _mm256_cmp_ps(m1_4,s3_4,0);
			const __m256 ind3_4 = _mm256_blendv_ps(ZERO,LADDER_3,mask3_4);
			const __m256 mask4_4 = _mm256_cmp_ps(m1_4,s4_4,0);
			const __m256 ind4_4 = _mm256_blendv_ps(ZERO,LADDER_4,mask4_4);

			__m256 ind4 = _mm256_max_ps(ind1_4,ind2_4);
			const __m256 temp4 = _mm256_max_ps(ind3_4,ind4_4);
			ind4 =  _mm256_max_ps(ind4,temp4);

			const __m256 mask1_5 = _mm256_cmp_ps(m1_5,s1_5,0);
			const __m256 ind1_5 = _mm256_blendv_ps(ZERO,LADDER,mask1_5);
			const __m256 mask2_5 = _mm256_cmp_ps(m1_5,s2_5,0);
			const __m256 ind2_5 = _mm256_blendv_ps(ZERO,LADDER_2,mask2_5);
			const __m256 mask3_5 = _mm256_cmp_ps(m1_5,s3_5,0);
			const __m256 ind3_5 = _mm256_blendv_ps(ZERO,LADDER_3,mask3_5);
			const __m256 mask4_5 = _mm256_cmp_ps(m1_5,s4_5,0);
			const __m256 ind4_5 = _mm256_blendv_ps(ZERO,LADDER_4,mask4_5);

			__m256 ind5 = _mm256_max_ps(ind1_5,ind2_5);
			const __m256 temp5 = _mm256_max_ps(ind3_5,ind4_5);
			ind5 =  _mm256_max_ps(ind5,temp5);

			const __m256 mask1_6 = _mm256_cmp_ps(m1_6,s1_6,0);
			const __m256 ind1_6 = _mm256_blendv_ps(ZERO,LADDER,mask1_6);
			const __m256 mask2_6 = _mm256_cmp_ps(m1_6,s2_6,0);
			const __m256 ind2_6 = _mm256_blendv_ps(ZERO,LADDER_2,mask2_6);
			const __m256 mask3_6 = _mm256_cmp_ps(m1_6,s3_6,0);
			const __m256 ind3_6 = _mm256_blendv_ps(ZERO,LADDER_3,mask3_6);
			const __m256 mask4_6 = _mm256_cmp_ps(m1_6,s4_6,0);
			const __m256 ind4_6 = _mm256_blendv_ps(ZERO,LADDER_4,mask4_6);

			__m256 ind6 = _mm256_max_ps(ind1_6,ind2_6);
			const __m256 temp6 = _mm256_max_ps(ind3_6,ind4_6);
			ind6 =  _mm256_max_ps(ind6,temp6);

			const __m256 mask1_7 = _mm256_cmp_ps(m1_7,s1_7,0);
			const __m256 ind1_7 = _mm256_blendv_ps(ZERO,LADDER,mask1_7);
			const __m256 mask2_7 = _mm256_cmp_ps(m1_7,s2_7,0);
			const __m256 ind2_7 = _mm256_blendv_ps(ZERO,LADDER_2,mask2_7);
			const __m256 mask3_7 = _mm256_cmp_ps(m1_7,s3_7,0);
			const __m256 ind3_7 = _mm256_blendv_ps(ZERO,LADDER_3,mask3_7);
			const __m256 mask4_7 = _mm256_cmp_ps(m1_7,s4_7,0);
			const __m256 ind4_7 = _mm256_blendv_ps(ZERO,LADDER_4,mask4_7);

			__m256 ind7 = _mm256_max_ps(ind1_7,ind2_7);
			const __m256 temp7 = _mm256_max_ps(ind3_7,ind4_7);
			ind7 =  _mm256_max_ps(ind7,temp7);

			const __m256 mask1_8 = _mm256_cmp_ps(m1_8,s1_8,0);
			const __m256 ind1_8 = _mm256_blendv_ps(ZERO,LADDER,mask1_8);
			const __m256 mask2_8 = _mm256_cmp_ps(m1_8,s2_8,0);
			const __m256 ind2_8 = _mm256_blendv_ps(ZERO,LADDER_2,mask2_8);
			const __m256 mask3_8 = _mm256_cmp_ps(m1_8,s3_8,0);
			const __m256 ind3_8 = _mm256_blendv_ps(ZERO,LADDER_3,mask3_8);
			const __m256 mask4_8 = _mm256_cmp_ps(m1_8,s4_8,0);
			const __m256 ind4_8 = _mm256_blendv_ps(ZERO,LADDER_4,mask4_8);

			__m256 ind8 = _mm256_max_ps(ind1_8,ind2_8);
			const __m256 temp8 = _mm256_max_ps(ind3_8,ind4_8);
			ind8 =  _mm256_max_ps(ind8,temp8);



			// transpose again
			//transpose8_ps(ind1,ind2,ind3,ind4,ind5,ind6,ind7,ind8);

			/**     TRANSPOSE    **/

			__m256 __t0, __t1, __t2, __t3, __t4, __t5, __t6, __t7;
			__m256 __tt0, __tt1, __tt2, __tt3, __tt4, __tt5, __tt6, __tt7;
			__t0 = _mm256_unpacklo_ps(ind1, ind2);
			__t1 = _mm256_unpackhi_ps(ind1, ind2);
			__t2 = _mm256_unpacklo_ps(ind3, ind4);
			__t3 = _mm256_unpackhi_ps(ind3, ind4);
			__t4 = _mm256_unpacklo_ps(ind5, ind6);
			__t5 = _mm256_unpackhi_ps(ind5, ind6);
			__t6 = _mm256_unpacklo_ps(ind7, ind8);
			__t7 = _mm256_unpackhi_ps(ind7, ind8);
			__tt0 = _mm256_shuffle_ps(__t0,__t2,_MM_SHUFFLE(1,0,1,0));
			__tt1 = _mm256_shuffle_ps(__t0,__t2,_MM_SHUFFLE(3,2,3,2));
			__tt2 = _mm256_shuffle_ps(__t1,__t3,_MM_SHUFFLE(1,0,1,0));
			__tt3 = _mm256_shuffle_ps(__t1,__t3,_MM_SHUFFLE(3,2,3,2));
			__tt4 = _mm256_shuffle_ps(__t4,__t6,_MM_SHUFFLE(1,0,1,0));
			__tt5 = _mm256_shuffle_ps(__t4,__t6,_MM_SHUFFLE(3,2,3,2));
			__tt6 = _mm256_shuffle_ps(__t5,__t7,_MM_SHUFFLE(1,0,1,0));
			__tt7 = _mm256_shuffle_ps(__t5,__t7,_MM_SHUFFLE(3,2,3,2));
			ind1 = _mm256_permute2f128_ps(__tt0, __tt4, 0x20);
			ind2 = _mm256_permute2f128_ps(__tt1, __tt5, 0x20);
			ind3 = _mm256_permute2f128_ps(__tt2, __tt6, 0x20);
			ind4 = _mm256_permute2f128_ps(__tt3, __tt7, 0x20);
			ind5 = _mm256_permute2f128_ps(__tt0, __tt4, 0x31);
			ind6 = _mm256_permute2f128_ps(__tt1, __tt5, 0x31);
			ind7 = _mm256_permute2f128_ps(__tt2, __tt6, 0x31);
			ind8 = _mm256_permute2f128_ps(__tt3, __tt7, 0x31);

			/** ================ **/
			__m256 c1 = _mm256_max_ps(ind1,ind2);
			const __m256 c2 = _mm256_max_ps(ind3,ind4);
			__m256 c3 = _mm256_max_ps(ind5,ind6);
			const __m256 c4 = _mm256_max_ps(ind7,ind8);
			c1 = _mm256_max_ps(c1,c2);
			c3 = _mm256_max_ps(c3,c4);
			c1 = _mm256_max_ps(c1,c3);

			// ----------------------------
			
			const __m256 emission_prob = _mm256_load_ps(&E[z][i]);
			const __m256 sum = _mm256_add_ps(emission_prob,m1);
			_mm256_storeu_ps(&cost_matrix[T % 2][i],sum);
			

			const __m256i path = _mm256_cvttps_epi32(c1);
			_mm256_storeu_si256((__m256i *)&policy_matrix[T][i],path);
  			
		}

	}


	//-------------
	/** Backwards part, first state */
#ifdef ICC
#pragma unroll_and_jam
#endif		
	for(int i = 0; i < k; ++i) {
		const unsigned z = sequence->emissions[0];			
		const real_t emission_prob = E[z][i];
		const real_t initial_prob = hmm->initial_probability[i];
		real_t min_cost = P[i][0] + cost_matrix[1][0];
		int min_j = 0;

		for(int j = 1; j < k; ++j) {
			const real_t transition_prob = P[i][j];
			const real_t old_cost = cost_matrix[1][j];

			const real_t new_cost = old_cost + transition_prob;

			if(new_cost < min_cost) {
				min_j = j;
				min_cost = new_cost;
			}
		}

		cost_matrix[0][i] = min_cost + emission_prob + initial_prob;
		policy_matrix[0][i] = min_j;
	}

	/** Forward part */

	real_t min_cost = cost_matrix[0][0];
	int min_i = 0;
#ifdef ICC
#pragma unroll_and_jam
#endif			
	for(int i = 1; i < k; ++i) {
		real_t cost = cost_matrix[0][i];
		if(cost < min_cost) {
			min_i = i;
			min_cost = cost;
		}
	}
#ifdef ICC
#pragma unroll_and_jam
#endif		
	for(int T = 0; T < n; ++T) {
		sequence->states[T] = min_i;
		min_i = policy_matrix[T][min_i];
	}
	//---------------------------------------

    timer_toc(timer);

	free(mem);
	free(policy_matrix);

	
}

