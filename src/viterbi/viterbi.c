#include "viterbi.h"
#include "utility.h"

#ifdef GCC
__attribute__((optimize("no-tree-vectorize")))
__attribute__((optimize("no-unroll-loops")))
#endif
void viterbi_backward_forward_search(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer) {
	const unsigned n = sequence->size;
	const unsigned k = hmm->num_states;
	const unsigned l = hmm->num_emissions;

	real_t   (* cost_matrix)[k] = (real_t (*)[k]) calloc(sizeof(real_t), k*n);
	unsigned (* policy_matrix)[k] = (unsigned (*)[k]) calloc(sizeof(unsigned), k*n);

	real_t (* P)[k] = (real_t (*)[k]) hmm->transition_probability;
	real_t (* E)[l] = (real_t (*)[l]) hmm->emission_probability;


	/** Newly added timer. @date May 10 2016 */


	/** Initial probability part */
	const unsigned zN = sequence->emissions[n-1];
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif	
	for(int i = 0; i < k; ++i){
		const real_t emission_prob = hmm->emission_probability[i*k + zN];	
		const real_t new_cost = -log(emission_prob);
		cost_matrix[n-1][i] = new_cost;
	}		
	timer_tic(timer);

	/** Backwards part */
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif	
	for(int T = n-2; T >= 0; --T) {
		const unsigned z = sequence->emissions[T];	
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif			
		for(int i = 0; i < k; ++i) {
			const real_t emission_prob = E[i][z];
			const real_t initial_prob = T == 0 ? hmm->initial_probability[i] : 1.;
			real_t min_cost = INFINITY;
			int min_j = -1;
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif	
			for(int j = 0; j < k; ++j) {
				const real_t transition_prob = P[i][j];
				const real_t old_cost = cost_matrix[T+1][j];

				const real_t new_cost = old_cost - log(initial_prob * emission_prob * transition_prob);

				if(new_cost < min_cost) {
					min_j = j;
					min_cost = new_cost;
				}
			}

			cost_matrix[T][i] = min_cost;
			policy_matrix[T][i] = min_j;
		}
	}
	timer_toc(timer);

	/** Forward part */

	real_t min_cost = INFINITY;
	int min_i = 0;
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif	
	for(int i = 0; i < k; ++i) {
		real_t cost = cost_matrix[0][i];
		if(cost < min_cost) {
			min_i = i;
			min_cost = cost;
		}
	}
	assert(min_i > -1 && min_i < k);
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif
	for(int T = 0; T < n; ++T) {
		sequence->states[T] = min_i;
		min_i = policy_matrix[T][min_i];
	}

	free(cost_matrix);
	free(policy_matrix);


}
