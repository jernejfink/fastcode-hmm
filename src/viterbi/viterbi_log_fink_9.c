#include "viterbi.h"
#include "utility.h"
#include <immintrin.h>

/* Only aligned state space allowed. Matrix transposition idea. Built on 7. */

#ifdef GCC
__attribute__((optimize("no-unroll-loops")))
#endif 
void viterbi_log_fink_9(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer) {
    const unsigned n = sequence->size;
    const unsigned k = hmm->num_states;

    real_t (* P)[k] = (real_t (*)[k]) hmm->transition_probability_tr;
    real_t (* E)[k] = (real_t (*)[k]) hmm->emission_probability_tr;

    real_t * mem;
    uint32_t *memi; 
    posix_memalign((void **) &mem,  8*sizeof(real_t),   2*k*sizeof(real_t));
    posix_memalign((void **) &memi, 8*sizeof(uint32_t), n*k*sizeof(uint32_t));

    real_t (* cost_matrix)[k] = (real_t (*)[k]) mem;
    uint32_t (* policy_matrix)[k] = (uint32_t (*)[k]) memi;

    /** Newly added timer. @date May 10 2016 */
    timer_tic(timer);

    /** Initial probability part */
    const unsigned zN = sequence->emissions[n-1];
    for(int i = 0; i < k; ++i){
        const real_t emission_prob = E[zN][i];
        cost_matrix[(n-1)%2][i] = emission_prob;
    }       

    /** Backwards part */
const __m256 DIFF_J = _mm256_set1_ps(1.);
#ifdef ICC
#pragma nounroll_and_jam
#endif
for(int T = n-2; T >= 0; --T) {
    const unsigned z = sequence->emissions[T];
#ifdef ICC
#pragma nounroll_and_jam
#endif
    for(int i = 0; i < k; i += 32) {     

        const __m256 oc1 = _mm256_set1_ps(cost_matrix[(T+1)%2][0]);
        const __m256 tp1_1 = _mm256_load_ps(&P[0][i + 0]);
        const __m256 tp2_1 = _mm256_load_ps(&P[0][i + 8]);
        const __m256 tp3_1 = _mm256_load_ps(&P[0][i + 16]);
        const __m256 tp4_1 = _mm256_load_ps(&P[0][i + 24]);
        
        __m256 mc1_1 = _mm256_add_ps(oc1, tp1_1);
        __m256 mc2_1 = _mm256_add_ps(oc1, tp2_1);
        __m256 mc3_1 = _mm256_add_ps(oc1, tp3_1);
        __m256 mc4_1 = _mm256_add_ps(oc1, tp4_1);
                
        __m256 mj1_1 = _mm256_set1_ps(0.);
        __m256 mj2_1 = _mm256_set1_ps(0.);
        __m256 mj3_1 = _mm256_set1_ps(0.);
        __m256 mj4_1 = _mm256_set1_ps(0.);
                
        __m256 jj1 = _mm256_set1_ps(1.);
#ifdef ICC
#pragma nounroll_and_jam
#endif        
        for(int j = 1; j < k; ++j) {
    
            const __m256 oc1 = _mm256_set1_ps(cost_matrix[(T+1)%2][j+0]);

            const __m256 tp1_1 = _mm256_load_ps(&P[j+0][i + 0]);
            const __m256 tp2_1 = _mm256_load_ps(&P[j+0][i + 8]);
            const __m256 tp3_1 = _mm256_load_ps(&P[j+0][i + 16]);
            const __m256 tp4_1 = _mm256_load_ps(&P[j+0][i + 24]);
                    
            const __m256 nc1_1 = _mm256_add_ps(oc1, tp1_1);
            const __m256 nc2_1 = _mm256_add_ps(oc1, tp2_1);
            const __m256 nc3_1 = _mm256_add_ps(oc1, tp3_1);
            const __m256 nc4_1 = _mm256_add_ps(oc1, tp4_1);
                    
            const __m256 cmp1_1 = _mm256_cmp_ps(nc1_1, mc1_1, 0x01);
            const __m256 cmp2_1 = _mm256_cmp_ps(nc2_1, mc2_1, 0x01);
            const __m256 cmp3_1 = _mm256_cmp_ps(nc3_1, mc3_1, 0x01);
            const __m256 cmp4_1 = _mm256_cmp_ps(nc4_1, mc4_1, 0x01);
                    
            mc1_1 = _mm256_blendv_ps(mc1_1, nc1_1, cmp1_1);
            mc2_1 = _mm256_blendv_ps(mc2_1, nc2_1, cmp2_1);
            mc3_1 = _mm256_blendv_ps(mc3_1, nc3_1, cmp3_1);
            mc4_1 = _mm256_blendv_ps(mc4_1, nc4_1, cmp4_1);
                    
            mj1_1 = _mm256_blendv_ps(mj1_1, jj1, cmp1_1);
            mj2_1 = _mm256_blendv_ps(mj2_1, jj1, cmp2_1);
            mj3_1 = _mm256_blendv_ps(mj3_1, jj1, cmp3_1);
            mj4_1 = _mm256_blendv_ps(mj4_1, jj1, cmp4_1);
                    
            jj1 = _mm256_add_ps(jj1, DIFF_J);
            
        }
        const __m256 mc__1__1__1 = mc1_1;
        const __m256 mc__1__2__1 = mc2_1;
        const __m256 mc__1__3__1 = mc3_1;
        const __m256 mc__1__4__1 = mc4_1;
        const __m256 mj__1__1__1 = mj1_1;
        const __m256 mj__1__2__1 = mj2_1;
        const __m256 mj__1__3__1 = mj3_1;
        const __m256 mj__1__4__1 = mj4_1;
        

              
        

        const __m256 mc1 = mc__1__1__1;
        const __m256 mc2 = mc__1__2__1;
        const __m256 mc3 = mc__1__3__1;
        const __m256 mc4 = mc__1__4__1;
                
        const __m256 mj1 = mj__1__1__1;
        const __m256 mj2 = mj__1__2__1;
        const __m256 mj3 = mj__1__3__1;
        const __m256 mj4 = mj__1__4__1;
                
        const __m256 ep1 = _mm256_load_ps(&E[z][i + 0]);
        const __m256 ep2 = _mm256_load_ps(&E[z][i + 8]);
        const __m256 ep3 = _mm256_load_ps(&E[z][i + 16]);
        const __m256 ep4 = _mm256_load_ps(&E[z][i + 24]);
                
        __m256 mc1fin = _mm256_add_ps(mc1, ep1);
        __m256 mc2fin = _mm256_add_ps(mc2, ep2);
        __m256 mc3fin = _mm256_add_ps(mc3, ep3);
        __m256 mc4fin = _mm256_add_ps(mc4, ep4);
                
        const __m256i mj1fin = _mm256_cvtps_epi32(mj1);
        const __m256i mj2fin = _mm256_cvtps_epi32(mj2);
        const __m256i mj3fin = _mm256_cvtps_epi32(mj3);
        const __m256i mj4fin = _mm256_cvtps_epi32(mj4);
                
        if(T == 0) {
            const __m256 ip1 = _mm256_load_ps(&hmm->initial_probability[i + 0]);
            const __m256 ip2 = _mm256_load_ps(&hmm->initial_probability[i + 8]);
            const __m256 ip3 = _mm256_load_ps(&hmm->initial_probability[i + 16]);
            const __m256 ip4 = _mm256_load_ps(&hmm->initial_probability[i + 24]);
                    
            mc1fin = _mm256_add_ps(mc1fin, ip1);
            mc2fin = _mm256_add_ps(mc2fin, ip2);
            mc3fin = _mm256_add_ps(mc3fin, ip3);
            mc4fin = _mm256_add_ps(mc4fin, ip4);
                    
        }
        _mm256_store_ps(&cost_matrix[T%2][i + 0], mc1fin);        
        _mm256_store_ps(&cost_matrix[T%2][i + 8], mc2fin);        
        _mm256_store_ps(&cost_matrix[T%2][i + 16], mc3fin);        
        _mm256_store_ps(&cost_matrix[T%2][i + 24], mc4fin);        
                
        _mm256_store_si256((__m256i *) &policy_matrix[T][i + 0], mj1fin);
        _mm256_store_si256((__m256i *) &policy_matrix[T][i + 8], mj2fin);
        _mm256_store_si256((__m256i *) &policy_matrix[T][i + 16], mj3fin);
        _mm256_store_si256((__m256i *) &policy_matrix[T][i + 24], mj4fin);
                
    }
}
    /** Forward part */
    real_t min_cost1 = cost_matrix[0][0];
    real_t min_cost2 = cost_matrix[0][1];
    int min_i1 = 0;
    int min_i2 = 1;
#ifdef ICC
#pragma nounroll_and_jam
#endif
    for(int i = 2; i < k; i += 2) {
        real_t cost1 = cost_matrix[0][i];
        real_t cost2 = cost_matrix[0][i+1];
        _Bool b1 = cost1 < min_cost1;
        _Bool b2 = cost2 < min_cost2;

        min_cost1 = b1 ? cost1 : min_cost1; 
        min_cost2 = b2 ? cost2 : min_cost2; 
        min_i1 = b1 ? i   : min_i1; 
        min_i2 = b2 ? i+1 : min_i2; 
    }
    _Bool b = min_cost1 < min_cost2;
    int min_i = b ? min_i1 : min_i2;
#ifdef ICC
#pragma nounroll_and_jam
#endif
    for(int T = 0; T < n; ++T) {
        sequence->states[T] = min_i;
        min_i = policy_matrix[T][min_i];
    }    
    timer_toc(timer);

    free(mem);
    free(memi);
}


#ifdef GCC
__attribute__((optimize("unroll-loops")))
#endif 
void viterbi_log_fink_9_opt(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer) {
    const unsigned n = sequence->size;
    const unsigned k = hmm->num_states;

    real_t (* P)[k] = (real_t (*)[k]) hmm->transition_probability_tr;
    real_t (* E)[k] = (real_t (*)[k]) hmm->emission_probability_tr;

    real_t * mem;
    uint32_t *memi; 
    posix_memalign((void **) &mem,  8*sizeof(real_t),   2*k*sizeof(real_t));
    posix_memalign((void **) &memi, 8*sizeof(uint32_t), n*k*sizeof(uint32_t));

    real_t (* cost_matrix)[k] = (real_t (*)[k]) mem;
    uint32_t (* policy_matrix)[k] = (uint32_t (*)[k]) memi;

    /** Newly added timer. @date May 10 2016 */
    timer_tic(timer);

    /** Initial probability part */
    const unsigned zN = sequence->emissions[n-1];
    for(int i = 0; i < k; ++i){
        const real_t emission_prob = E[zN][i];
        cost_matrix[(n-1)%2][i] = emission_prob;
    }       

    /** Backwards part */
const __m256 DIFF_J = _mm256_set1_ps(1.);
#ifdef ICC
#pragma nounroll_and_jam
#endif
for(int T = n-2; T >= 0; --T) {
    const unsigned z = sequence->emissions[T];

    for(int i = 0; i < k; i += 32) {     

        const __m256 oc1 = _mm256_set1_ps(cost_matrix[(T+1)%2][0]);
        const __m256 tp1_1 = _mm256_load_ps(&P[0][i + 0]);
        const __m256 tp2_1 = _mm256_load_ps(&P[0][i + 8]);
        const __m256 tp3_1 = _mm256_load_ps(&P[0][i + 16]);
        const __m256 tp4_1 = _mm256_load_ps(&P[0][i + 24]);
        
        __m256 mc1_1 = _mm256_add_ps(oc1, tp1_1);
        __m256 mc2_1 = _mm256_add_ps(oc1, tp2_1);
        __m256 mc3_1 = _mm256_add_ps(oc1, tp3_1);
        __m256 mc4_1 = _mm256_add_ps(oc1, tp4_1);
                
        __m256 mj1_1 = _mm256_set1_ps(0.);
        __m256 mj2_1 = _mm256_set1_ps(0.);
        __m256 mj3_1 = _mm256_set1_ps(0.);
        __m256 mj4_1 = _mm256_set1_ps(0.);
                
        __m256 jj1 = _mm256_set1_ps(1.);
        for(int j = 1; j < k; ++j) {
    
            const __m256 oc1 = _mm256_set1_ps(cost_matrix[(T+1)%2][j+0]);

            const __m256 tp1_1 = _mm256_load_ps(&P[j+0][i + 0]);
            const __m256 tp2_1 = _mm256_load_ps(&P[j+0][i + 8]);
            const __m256 tp3_1 = _mm256_load_ps(&P[j+0][i + 16]);
            const __m256 tp4_1 = _mm256_load_ps(&P[j+0][i + 24]);
                    
            const __m256 nc1_1 = _mm256_add_ps(oc1, tp1_1);
            const __m256 nc2_1 = _mm256_add_ps(oc1, tp2_1);
            const __m256 nc3_1 = _mm256_add_ps(oc1, tp3_1);
            const __m256 nc4_1 = _mm256_add_ps(oc1, tp4_1);
                    
            const __m256 cmp1_1 = _mm256_cmp_ps(nc1_1, mc1_1, 0x01);
            const __m256 cmp2_1 = _mm256_cmp_ps(nc2_1, mc2_1, 0x01);
            const __m256 cmp3_1 = _mm256_cmp_ps(nc3_1, mc3_1, 0x01);
            const __m256 cmp4_1 = _mm256_cmp_ps(nc4_1, mc4_1, 0x01);
                    
            mc1_1 = _mm256_blendv_ps(mc1_1, nc1_1, cmp1_1);
            mc2_1 = _mm256_blendv_ps(mc2_1, nc2_1, cmp2_1);
            mc3_1 = _mm256_blendv_ps(mc3_1, nc3_1, cmp3_1);
            mc4_1 = _mm256_blendv_ps(mc4_1, nc4_1, cmp4_1);
                    
            mj1_1 = _mm256_blendv_ps(mj1_1, jj1, cmp1_1);
            mj2_1 = _mm256_blendv_ps(mj2_1, jj1, cmp2_1);
            mj3_1 = _mm256_blendv_ps(mj3_1, jj1, cmp3_1);
            mj4_1 = _mm256_blendv_ps(mj4_1, jj1, cmp4_1);
                    
            jj1 = _mm256_add_ps(jj1, DIFF_J);
            
        }
        const __m256 mc__1__1__1 = mc1_1;
        const __m256 mc__1__2__1 = mc2_1;
        const __m256 mc__1__3__1 = mc3_1;
        const __m256 mc__1__4__1 = mc4_1;
        const __m256 mj__1__1__1 = mj1_1;
        const __m256 mj__1__2__1 = mj2_1;
        const __m256 mj__1__3__1 = mj3_1;
        const __m256 mj__1__4__1 = mj4_1;
        

              
        

        const __m256 mc1 = mc__1__1__1;
        const __m256 mc2 = mc__1__2__1;
        const __m256 mc3 = mc__1__3__1;
        const __m256 mc4 = mc__1__4__1;
                
        const __m256 mj1 = mj__1__1__1;
        const __m256 mj2 = mj__1__2__1;
        const __m256 mj3 = mj__1__3__1;
        const __m256 mj4 = mj__1__4__1;
                
        const __m256 ep1 = _mm256_load_ps(&E[z][i + 0]);
        const __m256 ep2 = _mm256_load_ps(&E[z][i + 8]);
        const __m256 ep3 = _mm256_load_ps(&E[z][i + 16]);
        const __m256 ep4 = _mm256_load_ps(&E[z][i + 24]);
                
        __m256 mc1fin = _mm256_add_ps(mc1, ep1);
        __m256 mc2fin = _mm256_add_ps(mc2, ep2);
        __m256 mc3fin = _mm256_add_ps(mc3, ep3);
        __m256 mc4fin = _mm256_add_ps(mc4, ep4);
                
        const __m256i mj1fin = _mm256_cvtps_epi32(mj1);
        const __m256i mj2fin = _mm256_cvtps_epi32(mj2);
        const __m256i mj3fin = _mm256_cvtps_epi32(mj3);
        const __m256i mj4fin = _mm256_cvtps_epi32(mj4);
                
        if(T == 0) {
            const __m256 ip1 = _mm256_load_ps(&hmm->initial_probability[i + 0]);
            const __m256 ip2 = _mm256_load_ps(&hmm->initial_probability[i + 8]);
            const __m256 ip3 = _mm256_load_ps(&hmm->initial_probability[i + 16]);
            const __m256 ip4 = _mm256_load_ps(&hmm->initial_probability[i + 24]);
                    
            mc1fin = _mm256_add_ps(mc1fin, ip1);
            mc2fin = _mm256_add_ps(mc2fin, ip2);
            mc3fin = _mm256_add_ps(mc3fin, ip3);
            mc4fin = _mm256_add_ps(mc4fin, ip4);
                    
        }
        _mm256_store_ps(&cost_matrix[T%2][i + 0], mc1fin);        
        _mm256_store_ps(&cost_matrix[T%2][i + 8], mc2fin);        
        _mm256_store_ps(&cost_matrix[T%2][i + 16], mc3fin);        
        _mm256_store_ps(&cost_matrix[T%2][i + 24], mc4fin);        
                
        _mm256_store_si256((__m256i *) &policy_matrix[T][i + 0], mj1fin);
        _mm256_store_si256((__m256i *) &policy_matrix[T][i + 8], mj2fin);
        _mm256_store_si256((__m256i *) &policy_matrix[T][i + 16], mj3fin);
        _mm256_store_si256((__m256i *) &policy_matrix[T][i + 24], mj4fin);
                
    }
}
    /** Forward part */
    real_t min_cost1 = cost_matrix[0][0];
    real_t min_cost2 = cost_matrix[0][1];
    int min_i1 = 0;
    int min_i2 = 1;

    for(int i = 2; i < k; i += 2) {
        real_t cost1 = cost_matrix[0][i];
        real_t cost2 = cost_matrix[0][i+1];
        _Bool b1 = cost1 < min_cost1;
        _Bool b2 = cost2 < min_cost2;

        min_cost1 = b1 ? cost1 : min_cost1; 
        min_cost2 = b2 ? cost2 : min_cost2; 
        min_i1 = b1 ? i   : min_i1; 
        min_i2 = b2 ? i+1 : min_i2; 
    }
    _Bool b = min_cost1 < min_cost2;
    int min_i = b ? min_i1 : min_i2;

    for(int T = 0; T < n; ++T) {
        sequence->states[T] = min_i;
        min_i = policy_matrix[T][min_i];
    }    
    timer_toc(timer);

    free(mem);
    free(memi);
}


