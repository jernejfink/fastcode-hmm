#include "viterbi.h"
#include "utility.h"
#include <immintrin.h>

/* Unrolled the middle loop. Perfomance increase for small state size. No drop for large state size */
#ifdef GCC
__attribute__((optimize("no-tree-vectorize")))
__attribute__((optimize("no-unroll-loops")))
#endif
void viterbi_log_fink_11(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer) {
    const unsigned n = sequence->size;
    const unsigned k = hmm->num_states;

    real_t   (* cost_matrix)[k] = (real_t (*)[k]) calloc(sizeof(real_t), k*2);
    unsigned (* policy_matrix)[k] = (unsigned (*)[k]) calloc(sizeof(unsigned), k*n);

    real_t (* P)[k] = (real_t (*)[k]) hmm->transition_probability;
    real_t (* E)[k] = (real_t (*)[k]) hmm->emission_probability;
    real_t (* E_tr)[k] = (real_t (*)[k]) hmm->emission_probability_tr;


    /** Newly added timer. @date May 10 2016 */
    timer_tic(timer);

    /** Initial probability part */
    const unsigned zN = sequence->emissions[n-1];
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif  
    for(int i = 0; i < k; ++i){
        const real_t emission_prob = E_tr[zN][i];
        cost_matrix[(n-1)%2][i] = emission_prob;
    }       
    /** Backwards part */
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif  
    for(int T = n-2; T >= 0; --T) {
        const unsigned z = sequence->emissions[T];  
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif
        for(int i = 0; i < k; i+= 2) {
            const real_t c1 = cost_matrix[(T + 1) % 2][0];
            const real_t c2 = cost_matrix[(T + 1) % 2][1];
            const real_t c3 = cost_matrix[(T + 1) % 2][2];
            const real_t c4 = cost_matrix[(T + 1) % 2][3];
                        
            int mj4_1_1 = 0;
            int mj4_2_1 = 0;
            
            int mj4_1_2 = 1;
            int mj4_2_2 = 1;
            
            int mj4_1_3 = 2;
            int mj4_2_3 = 2;
            
            int mj4_1_4 = 3;
            int mj4_2_4 = 3;
            
            
            // Min costs 
            real_t mc4_1_1 = P[i+0][0] + c1;
            real_t mc4_2_1 = P[i+1][0] + c1;
            
            real_t mc4_1_2 = P[i+0][1] + c2;
            real_t mc4_2_2 = P[i+1][1] + c2;
            
            real_t mc4_1_3 = P[i+0][2] + c3;
            real_t mc4_2_3 = P[i+1][2] + c3;
            
            real_t mc4_1_4 = P[i+0][3] + c4;
            real_t mc4_2_4 = P[i+1][3] + c4;
            
                        
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif
            for(int j = 4; j < k; j += 4) {
                const real_t oc1 = cost_matrix[(T+1) % 2][j+0];
                const real_t oc2 = cost_matrix[(T+1) % 2][j+1];
                const real_t oc3 = cost_matrix[(T+1) % 2][j+2];
                const real_t oc4 = cost_matrix[(T+1) % 2][j+3];
                            
                const real_t t1_1 = P[i+0][j+0];
                const real_t t2_1 = P[i+1][j+0];
                
                const real_t t1_2 = P[i+0][j+1];
                const real_t t2_2 = P[i+1][j+1];
                
                const real_t t1_3 = P[i+0][j+2];
                const real_t t2_3 = P[i+1][j+2];
                
                const real_t t1_4 = P[i+0][j+3];
                const real_t t2_4 = P[i+1][j+3];
                
                
                const real_t nc1_1 = oc1 + t1_1;
                const real_t nc2_1 = oc1 + t2_1;
                
                const real_t nc1_2 = oc2 + t1_2;
                const real_t nc2_2 = oc2 + t2_2;
                
                const real_t nc1_3 = oc3 + t1_3;
                const real_t nc2_3 = oc3 + t2_3;
                
                const real_t nc1_4 = oc4 + t1_4;
                const real_t nc2_4 = oc4 + t2_4;
                
                
                const _Bool cmp1_1 = nc1_1 < mc4_1_1;
                const _Bool cmp2_1 = nc2_1 < mc4_2_1;
                
                const _Bool cmp1_2 = nc1_2 < mc4_1_2;
                const _Bool cmp2_2 = nc2_2 < mc4_2_2;
                
                const _Bool cmp1_3 = nc1_3 < mc4_1_3;
                const _Bool cmp2_3 = nc2_3 < mc4_2_3;
                
                const _Bool cmp1_4 = nc1_4 < mc4_1_4;
                const _Bool cmp2_4 = nc2_4 < mc4_2_4;
                
                
                mj4_1_1 = cmp1_1 ? j+0 : mj4_1_1;
                mj4_2_1 = cmp2_1 ? j+0 : mj4_2_1;
                
                mj4_1_2 = cmp1_2 ? j+1 : mj4_1_2;
                mj4_2_2 = cmp2_2 ? j+1 : mj4_2_2;
                
                mj4_1_3 = cmp1_3 ? j+2 : mj4_1_3;
                mj4_2_3 = cmp2_3 ? j+2 : mj4_2_3;
                
                mj4_1_4 = cmp1_4 ? j+3 : mj4_1_4;
                mj4_2_4 = cmp2_4 ? j+3 : mj4_2_4;
                
                
                mc4_1_1 = cmp1_1 ? nc1_1 : mc4_1_1;
                mc4_2_1 = cmp2_1 ? nc2_1 : mc4_2_1;
                
                mc4_1_2 = cmp1_2 ? nc1_2 : mc4_1_2;
                mc4_2_2 = cmp2_2 ? nc2_2 : mc4_2_2;
                
                mc4_1_3 = cmp1_3 ? nc1_3 : mc4_1_3;
                mc4_2_3 = cmp2_3 ? nc2_3 : mc4_2_3;
                
                mc4_1_4 = cmp1_4 ? nc1_4 : mc4_1_4;
                mc4_2_4 = cmp2_4 ? nc2_4 : mc4_2_4;
                
                
            }
            const _Bool cmp2_1_1 = mc4_1_1 < mc4_1_2;
                const real_t mc2_1_1 = cmp2_1_1 ? mc4_1_1 : mc4_1_2;
                const int mj2_1_1 = cmp2_1_1 ? mj4_1_1 : mj4_1_2;                
                
            const _Bool cmp2_2_1 = mc4_2_1 < mc4_2_2;
                const real_t mc2_2_1 = cmp2_2_1 ? mc4_2_1 : mc4_2_2;
                const int mj2_2_1 = cmp2_2_1 ? mj4_2_1 : mj4_2_2;                
                
            
            const _Bool cmp2_1_2 = mc4_1_3 < mc4_1_4;
                const real_t mc2_1_2 = cmp2_1_2 ? mc4_1_3 : mc4_1_4;
                const int mj2_1_2 = cmp2_1_2 ? mj4_1_3 : mj4_1_4;                
                
            const _Bool cmp2_2_2 = mc4_2_3 < mc4_2_4;
                const real_t mc2_2_2 = cmp2_2_2 ? mc4_2_3 : mc4_2_4;
                const int mj2_2_2 = cmp2_2_2 ? mj4_2_3 : mj4_2_4;                
                
            
            
            const _Bool cmp1 = mc2_1_1 < mc2_1_2;
                const real_t mc1 = cmp1 ? mc2_1_1 : mc2_1_2;
                const int mj1 = cmp1 ? mj2_1_1 : mj2_1_2;
                
            const _Bool cmp2 = mc2_2_1 < mc2_2_2;
                const real_t mc2 = cmp2 ? mc2_2_1 : mc2_2_2;
                const int mj2 = cmp2 ? mj2_2_1 : mj2_2_2;
                
            
            
            
            // Emission probs 
            const real_t e1 = E[i+0][z];
            const real_t e2 = E[i+1][z];
            
            real_t fc1   = mc1 + e1;
            real_t fc2   = mc2 + e2;
            
            if(T == 0){
                fc1  += hmm->initial_probability[i + 0];
                fc2  += hmm->initial_probability[i + 1];
                            
            }
            cost_matrix[T % 2][i + 0]   = fc1;
            cost_matrix[T % 2][i + 1]   = fc2;
            
            policy_matrix[T][i+0]   = mj1;
            policy_matrix[T][i+1]   = mj2;
            
        }
    }

    /** Forward part */

    real_t min_cost = cost_matrix[0][0];
    int min_i = 0;
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif  
    for(int i = 1; i < k; ++i) {
        real_t cost = cost_matrix[0][i];
        if(cost < min_cost) {
            min_i = i;
            min_cost = cost;
        }
    }
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif
    for(int T = 0; T < n; ++T) {
        sequence->states[T] = min_i;
        min_i = policy_matrix[T][min_i];
    }
    timer_toc(timer);

    free(cost_matrix);
    free(policy_matrix);

}


#ifdef GCC
__attribute__((optimize("no-tree-vectorize")))
#endif
void viterbi_log_fink_11_opt(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer) {
    const unsigned n = sequence->size;
    const unsigned k = hmm->num_states;

    real_t   (* cost_matrix)[k] = (real_t (*)[k]) calloc(sizeof(real_t), k*2);
    unsigned (* policy_matrix)[k] = (unsigned (*)[k]) calloc(sizeof(unsigned), k*n);

    real_t (* P)[k] = (real_t (*)[k]) hmm->transition_probability;
    real_t (* E)[k] = (real_t (*)[k]) hmm->emission_probability;
    real_t (* E_tr)[k] = (real_t (*)[k]) hmm->emission_probability_tr;


    /** Newly added timer. @date May 10 2016 */
    timer_tic(timer);

    /** Initial probability part */
    const unsigned zN = sequence->emissions[n-1];
#ifdef ICC
#pragma novector
#pragma unroll_and_jam
#endif  
    for(int i = 0; i < k; ++i){
        const real_t emission_prob = E_tr[zN][i];
        cost_matrix[(n-1)%2][i] = emission_prob;
    }       
    /** Backwards part */
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif  
    for(int T = n-2; T >= 0; --T) {
        const unsigned z = sequence->emissions[T];  
#ifdef ICC
#pragma novector
#pragma unroll_and_jam
#endif
        for(int i = 0; i < k; i+= 2) {
            const real_t c1 = cost_matrix[(T + 1) % 2][0];
            const real_t c2 = cost_matrix[(T + 1) % 2][1];
            const real_t c3 = cost_matrix[(T + 1) % 2][2];
            const real_t c4 = cost_matrix[(T + 1) % 2][3];
                        
            int mj4_1_1 = 0;
            int mj4_2_1 = 0;
            
            int mj4_1_2 = 1;
            int mj4_2_2 = 1;
            
            int mj4_1_3 = 2;
            int mj4_2_3 = 2;
            
            int mj4_1_4 = 3;
            int mj4_2_4 = 3;
            
            
            // Min costs 
            real_t mc4_1_1 = P[i+0][0] + c1;
            real_t mc4_2_1 = P[i+1][0] + c1;
            
            real_t mc4_1_2 = P[i+0][1] + c2;
            real_t mc4_2_2 = P[i+1][1] + c2;
            
            real_t mc4_1_3 = P[i+0][2] + c3;
            real_t mc4_2_3 = P[i+1][2] + c3;
            
            real_t mc4_1_4 = P[i+0][3] + c4;
            real_t mc4_2_4 = P[i+1][3] + c4;
            
                        
#ifdef ICC
#pragma novector
#pragma unroll_and_jam            
#endif
            for(int j = 4; j < k; j += 4) {
                const real_t oc1 = cost_matrix[(T+1) % 2][j+0];
                const real_t oc2 = cost_matrix[(T+1) % 2][j+1];
                const real_t oc3 = cost_matrix[(T+1) % 2][j+2];
                const real_t oc4 = cost_matrix[(T+1) % 2][j+3];
                            
                const real_t t1_1 = P[i+0][j+0];
                const real_t t2_1 = P[i+1][j+0];
                
                const real_t t1_2 = P[i+0][j+1];
                const real_t t2_2 = P[i+1][j+1];
                
                const real_t t1_3 = P[i+0][j+2];
                const real_t t2_3 = P[i+1][j+2];
                
                const real_t t1_4 = P[i+0][j+3];
                const real_t t2_4 = P[i+1][j+3];
                
                
                const real_t nc1_1 = oc1 + t1_1;
                const real_t nc2_1 = oc1 + t2_1;
                
                const real_t nc1_2 = oc2 + t1_2;
                const real_t nc2_2 = oc2 + t2_2;
                
                const real_t nc1_3 = oc3 + t1_3;
                const real_t nc2_3 = oc3 + t2_3;
                
                const real_t nc1_4 = oc4 + t1_4;
                const real_t nc2_4 = oc4 + t2_4;
                
                
                const _Bool cmp1_1 = nc1_1 < mc4_1_1;
                const _Bool cmp2_1 = nc2_1 < mc4_2_1;
                
                const _Bool cmp1_2 = nc1_2 < mc4_1_2;
                const _Bool cmp2_2 = nc2_2 < mc4_2_2;
                
                const _Bool cmp1_3 = nc1_3 < mc4_1_3;
                const _Bool cmp2_3 = nc2_3 < mc4_2_3;
                
                const _Bool cmp1_4 = nc1_4 < mc4_1_4;
                const _Bool cmp2_4 = nc2_4 < mc4_2_4;
                
                
                mj4_1_1 = cmp1_1 ? j+0 : mj4_1_1;
                mj4_2_1 = cmp2_1 ? j+0 : mj4_2_1;
                
                mj4_1_2 = cmp1_2 ? j+1 : mj4_1_2;
                mj4_2_2 = cmp2_2 ? j+1 : mj4_2_2;
                
                mj4_1_3 = cmp1_3 ? j+2 : mj4_1_3;
                mj4_2_3 = cmp2_3 ? j+2 : mj4_2_3;
                
                mj4_1_4 = cmp1_4 ? j+3 : mj4_1_4;
                mj4_2_4 = cmp2_4 ? j+3 : mj4_2_4;
                
                
                mc4_1_1 = cmp1_1 ? nc1_1 : mc4_1_1;
                mc4_2_1 = cmp2_1 ? nc2_1 : mc4_2_1;
                
                mc4_1_2 = cmp1_2 ? nc1_2 : mc4_1_2;
                mc4_2_2 = cmp2_2 ? nc2_2 : mc4_2_2;
                
                mc4_1_3 = cmp1_3 ? nc1_3 : mc4_1_3;
                mc4_2_3 = cmp2_3 ? nc2_3 : mc4_2_3;
                
                mc4_1_4 = cmp1_4 ? nc1_4 : mc4_1_4;
                mc4_2_4 = cmp2_4 ? nc2_4 : mc4_2_4;
                
                
            }
            const _Bool cmp2_1_1 = mc4_1_1 < mc4_1_2;
                const real_t mc2_1_1 = cmp2_1_1 ? mc4_1_1 : mc4_1_2;
                const int mj2_1_1 = cmp2_1_1 ? mj4_1_1 : mj4_1_2;                
                
            const _Bool cmp2_2_1 = mc4_2_1 < mc4_2_2;
                const real_t mc2_2_1 = cmp2_2_1 ? mc4_2_1 : mc4_2_2;
                const int mj2_2_1 = cmp2_2_1 ? mj4_2_1 : mj4_2_2;                
                
            
            const _Bool cmp2_1_2 = mc4_1_3 < mc4_1_4;
                const real_t mc2_1_2 = cmp2_1_2 ? mc4_1_3 : mc4_1_4;
                const int mj2_1_2 = cmp2_1_2 ? mj4_1_3 : mj4_1_4;                
                
            const _Bool cmp2_2_2 = mc4_2_3 < mc4_2_4;
                const real_t mc2_2_2 = cmp2_2_2 ? mc4_2_3 : mc4_2_4;
                const int mj2_2_2 = cmp2_2_2 ? mj4_2_3 : mj4_2_4;                
                
            
            
            const _Bool cmp1 = mc2_1_1 < mc2_1_2;
                const real_t mc1 = cmp1 ? mc2_1_1 : mc2_1_2;
                const int mj1 = cmp1 ? mj2_1_1 : mj2_1_2;
                
            const _Bool cmp2 = mc2_2_1 < mc2_2_2;
                const real_t mc2 = cmp2 ? mc2_2_1 : mc2_2_2;
                const int mj2 = cmp2 ? mj2_2_1 : mj2_2_2;
                
            
            
            
            // Emission probs 
            const real_t e1 = E[i+0][z];
            const real_t e2 = E[i+1][z];
            
            real_t fc1   = mc1 + e1;
            real_t fc2   = mc2 + e2;
            
            if(T == 0){
                fc1  += hmm->initial_probability[i + 0];
                fc2  += hmm->initial_probability[i + 1];
                            
            }
            cost_matrix[T % 2][i + 0]   = fc1;
            cost_matrix[T % 2][i + 1]   = fc2;
            
            policy_matrix[T][i+0]   = mj1;
            policy_matrix[T][i+1]   = mj2;
            
        }
    }

    /** Forward part */

    real_t min_cost = cost_matrix[0][0];
    int min_i = 0;
#ifdef ICC
#pragma novector
#pragma unroll_and_jam
#endif  
    for(int i = 1; i < k; ++i) {
        real_t cost = cost_matrix[0][i];
        if(cost < min_cost) {
            min_i = i;
            min_cost = cost;
        }
    }
#ifdef ICC
#pragma novector
#pragma unroll_and_jam    
#endif
    for(int T = 0; T < n; ++T) {
        sequence->states[T] = min_i;
        min_i = policy_matrix[T][min_i];
    }
    timer_toc(timer);

    free(cost_matrix);
    free(policy_matrix);

}