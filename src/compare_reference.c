#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "utility.h"
#include "train.h"
#include "profile_handler.h"
#include "wrappers.h"
#include "viterbi.h"
#include "hmm.h"

#ifdef GHMM

#include <ghmm/matrix.h>
#include <ghmm/rng.h>
#include <ghmm/sequence.h>
#include <ghmm/model.h>
#include <ghmm/viterbi.h>
#include <ghmm/foba.h>
#include <ghmm/obsolete.h>

#endif

unsigned *matlab_estimates;

int compare_fink_9(hmm_sequence_t * const sequence, profile_timer_t * const timer);
int compare_reference(hmm_sequence_t * const sequence, profile_timer_t * const timer);
void cache_scrambler();

int main(int argc, char * argv[]) {

    const int size[]  = {1000,10000, 100000, 1000000};

    posix_memalign((void **) &matlab_estimates, 32, sizeof(unsigned)*27);

    const int repeats = 10;
    const int test_size =  sizeof(size) / sizeof(int);
    const int space = 32;
    const int valid = 27;

    printf("%s %s %s\n", "n", "reference", "our");
    for(int i = 0; i < test_size; ++i) {

        const int n = size[i];

        hmm_sequence_t * sequence_ref = hmm_sequence_construct(n);
        hmm_sequence_t * sequence_fink = hmm_sequence_construct(n);

        for(int i = 0; i < n; ++i) {
            unsigned long long t = (unsigned long long) rand();
            sequence_ref->emissions[i] = (unsigned) (t % valid);
            sequence_fink->emissions[i] = (unsigned) (t % valid);
        }

        profile_timer_t timer_fink, timer_ref;
        double tsc_cycles_ref = 0., tsc_cycles_fink = 0.;

        for(int j = 0; j < repeats; ++j) {
            timer_reset(&timer_ref);
            timer_reset(&timer_fink);

            cache_scrambler();
            compare_reference(sequence_ref, &timer_ref);
            tsc_cycles_ref += (double) timer_tsc_diff(&timer_ref);

            cache_scrambler();
            compare_fink_9(sequence_fink, &timer_fink);
            tsc_cycles_fink += (double) timer_tsc_diff(&timer_fink);

            if(j == 0)
            for(int l = 0; l < n; ++l) {
                if(sequence_ref->states[l] != sequence_ref->states[l]) printf("neq\n");
            }

            memset(sequence_ref->states,  0, sizeof(unsigned)*n);        
            memset(sequence_fink->states, 0, sizeof(unsigned)*n);
        }

        const double runtime_ref  = tsc_cycles_ref / repeats;
        const double runtime_fink = tsc_cycles_fink / repeats;
       
        printf("%d %f %f\n", size[i], runtime_ref, runtime_fink);

        hmm_sequence_destruct(sequence_ref);
        hmm_sequence_destruct(sequence_fink);

    }

    free(matlab_estimates);

}


void cache_scrambler() 
{
    srand(time(NULL));
    float * pointers[10];
    for(int i = 0; i < 10; ++i) 
    {
        pointers[i] = calloc(1000000, sizeof(float));
    }
    for(int j = 0; j < 1000000; ++j) {
        for(int i = 0; i < 10; ++i) 
        {
            pointers[i][j] = (float) rand() / (float) RAND_MAX;
            pointers[i][j] *= pointers[i][j];
        }
    }
    for(int i = 0; i < 10; ++i) {
        free(pointers[i]);
    }
}

#ifdef GHMM
int compare_reference(hmm_sequence_t * const sequence, profile_timer_t * const timer) {
    timer_tic(timer);
    const int N = 32;
    hmm_t * hmm = hmm_construct(N, N);
   
    read_real_from_file("./data/fink_tran_aligned.txt", hmm->transition_probability, N*N);
    read_real_from_file("./data/fink_emis_aligned.txt", hmm->emission_probability, N*N);
    read_real_from_file("./data/fink_init_aligned.txt", hmm->initial_probability, N);   

    timer_toc(timer);
    viterbi_ghmm(hmm, sequence, timer);
    
   

    return 0;

}
#else 
int compare_reference(hmm_sequence_t * const sequence, profile_timer_t * const timer) {
    return 0;
}
#endif


int compare_fink_9(hmm_sequence_t * const sequence, profile_timer_t * const timer)
{

    timer_tic(timer);
    const int N = 32;
    hmm_t * hmm = hmm_construct(N, N);

    read_real_from_file("./data/fink_tran_aligned.txt", hmm->transition_probability, N*N);
    read_real_from_file("./data/fink_emis_aligned.txt", hmm->emission_probability, N*N);
    read_real_from_file("./data/fink_init_aligned.txt", hmm->initial_probability, N);   

   
    profile_timer_t ttimer;
    hmm_normalize_log(hmm);

    viterbi_log_fink_9(hmm, sequence, &ttimer);
    
    timer_toc(timer);    

    return 0;
}
