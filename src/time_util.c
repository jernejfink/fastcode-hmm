#include "time_util.h"

void timer_tic(profile_timer_t * const timer)
{
	timer->start = clock();
	timer->start_tsc = start_tsc();
}

void timer_toc(profile_timer_t * const timer) {
	timer->end = clock();
	timer->end_tsc = start_tsc();
	timer->diff += timer->end - timer->start;
	timer->diff_tsc += timer->end_tsc - timer->start_tsc;	
}

myInt64 timer_tsc_diff(profile_timer_t * const timer) {
	return timer->end_tsc - timer->start_tsc;
}

double timer_clock_diff(profile_timer_t * const timer) {
	return (double) (timer->end - timer->start);
}

void timer_reset(profile_timer_t * const timer) {
	timer->diff = 0;
	timer->diff_tsc = 0;
}
