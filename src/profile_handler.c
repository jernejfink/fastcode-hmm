#include "profile_handler.h"


profile_handler_t * profile_handler_construct(int (*function)(const unsigned n, profile_timer_t * const), 
	const unsigned repeats, const unsigned * const sizes, const unsigned num_sizes, unsigned long long (*opcount)(const unsigned long long n) )
{
	profile_handler_t * profile_handler = calloc(sizeof(profile_handler_t), 1);
	profile_handler->repeats = repeats;
	profile_handler->function = function;
	profile_handler->opcount = opcount;
	profile_handler->sizes = calloc(sizeof(unsigned), num_sizes);
	profile_handler->num_sizes = num_sizes;
	for(int i = 0; i < num_sizes; ++i) profile_handler->sizes[i] = sizes[i];

	return profile_handler;
}


void profile_handler_destruct(profile_handler_t * const profile_handler) {
	free(profile_handler->sizes);
	free(profile_handler);
}

void profile_handler_run(profile_handler_t * const profile_handler, const char fname[]) {
	FILE *f = fopen(fname, "w");
	if (f == NULL)
    {
        printf("Error opening file! (%s) \n", fname);
        exit(-1);
    }

	printf("%20s %20s %20s %20s\n", "n", "cycles", "FLOP/cycle", "# ops");
	for(int i = 0; i < profile_handler->num_sizes; ++i) {
		double tsc_cycles_sum = 0.;
		double clock_cycles_sum = 0.;
		profile_timer_t timer;
		for(int j = 0; j < profile_handler->repeats; ++j) {
			cache_scrambler();
			timer_reset(&timer);
			profile_handler->function(profile_handler->sizes[i], &timer);
			tsc_cycles_sum   += (double) timer_tsc_diff(&timer);
			clock_cycles_sum += (double) timer_clock_diff(&timer);
		}
		const unsigned long long num_ops = profile_handler->opcount(profile_handler->sizes[i]);
		const double perf = ((double) num_ops) / (tsc_cycles_sum / profile_handler->repeats);
		printf("%20u %20.3f %20.3f %20llu\n", 
				profile_handler->sizes[i], 
				tsc_cycles_sum / profile_handler->repeats, 
				perf,
				num_ops);
		fprintf(f, "%u %lf %lf\n", profile_handler->sizes[i], tsc_cycles_sum / profile_handler->repeats, perf);
	}
	fclose(f);
}

void cache_scrambler() 
{
	srand(time(NULL));
	float * pointers[10];
	for(int i = 0; i < 10; ++i) 
	{
		pointers[i] = calloc(1000000, sizeof(float));
	}
	for(int j = 0; j < 1000000; ++j) {
		for(int i = 0; i < 10; ++i) 
		{
			pointers[i][j] = (float) rand() / (float) RAND_MAX;
			pointers[i][j] *= pointers[i][j];
		}
	}
	for(int i = 0; i < 10; ++i) {
		free(pointers[i]);
	}

}