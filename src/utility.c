#include "utility.h"

int read_real_from_file(const char fname[], real_t * const buffer, const int N) {
	FILE *f = fopen(fname, "r");
	if (f == NULL)
    {
        printf("Error opening file!\n");
        return 1;
    }
    double var;
    for(int i = 0; i < N; ++i) {
    	fscanf(f, "%lf", &var);	
    	buffer[i] = (real_t) var;
    }
    
    fclose(f);    
    return 0;
}

int read_double_from_file(const char fname[], double * const buffer, const int N) {
    FILE *f = fopen(fname, "r");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        return 1;
    }
    double var;
    for(int i = 0; i < N; ++i) {
        fscanf(f, "%lf", &var); 
        buffer[i] = (double) var;
    }
    
    fclose(f);    
    return 0;
}

int read_signed_from_file(const char fname[], int * const buffer, const int N) {
    FILE *f = fopen(fname, "r");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        return 1;
    }
    double var;
    for(int i = 0; i < N; ++i) {
        fscanf(f, "%lf", &var); 
        buffer[i] = (int) var;
    }

    fclose(f);
    return 0;    
}

int read_unsigned_from_file(const char fname[], unsigned * const buffer, const int N) {
	FILE *f = fopen(fname, "r");
	if (f == NULL)
    {
        printf("Error opening file!\n");
        return 1;
    }
    double var;
    for(int i = 0; i < N; ++i) {
    	fscanf(f, "%lf", &var);	
    	buffer[i] = (unsigned) var;
    }

    fclose(f);
    return 0;
}

unsigned longest_word(const char fname[]) {
    FILE *f = fopen(fname, "r");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        return 0;
    }

    size_t line_size = 0;
    size_t num_bytes = 0;
    char *line = NULL;

    size_t longest = 0;
    size_t word = 0;

    /* Determine the longest word */

    while( (num_bytes = getline(&line, &line_size, f) ) != -1 ) {
        word = !is_alpha(line[0]) ? 0 : word + 1;
        longest = word > longest ? word : longest;
    }
    free(line);
    fclose(f);  

    return longest;
}


unsigned test_set_size(const char fname[]) {
    FILE *f = fopen(fname, "r");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        return 0;
    }

    size_t line_size = 0;
    size_t num_bytes = 0;
    char *line = NULL;

    /* Scroll down to the test set (delimited by '.') */
    while( (num_bytes = getline(&line, &line_size, f) ) != -1 )       
        if(line[0] == '.') break; 

    size_t len = 0;
    while( (num_bytes = getline(&line, &line_size, f) ) != -1 ) 
        len += line_size;

    free(line);
    fclose(f);  

    return len;    
}

void read_test_data(const char fname[], hmm_sequence_t * const sequence) {
    char *line = NULL;    
    size_t line_size = 0;
    size_t num_bytes = 0;    

    FILE *f = fopen(fname, "r");
    if (f == NULL)
    {
        printf("Error opening file! %s \n", fname);
        exit(-1);
    }   
    /** Get to the training part */
    while( (num_bytes = getline(&line, &line_size, f) ) != -1 ) 
        if(line[0] == '.') break;

    int k = 0;
    while( (num_bytes = getline(&line, &line_size, f) ) != -1 ) {
        if (line[0] == '_' ) {
            sequence->emissions[k]  = 26;
        } else {
            sequence->emissions[k] = (unsigned) (line[2] - 'a');
        }
        k++;         
    }
    fclose(f);
}

void convert_syllables_to_hmm_emissions(char * const buf, hmm_sequence_t * const sequence) {
    /*  states: a* aa ab ... az, b*, ba, bb, ... bz, .. z*, za, ... zz */
    /*  together: 27^2 states */
    const int N = strlen(buf);
    assert(N <= sequence->size); 
    if(N == 1) {
        sequence->emissions[0] = 27*(buf[0] - 'a');
        sequence->size = 1;
    }
    else {
        for(int i = 0; i < N - 1; ++i) {
            sequence->emissions[i] = 27*(buf[i] - 'a') + (buf[i+1] - 'a' + 1);            
        }
        sequence->size = N - 1;
    }
}

void convert_syllables_to_hmm_states(char * const buf, hmm_sequence_t * const sequence) {
    /*  states: a* aa ab ... az, b*, ba, bb, ... bz, .. z*, za, ... zz */
    /*  together: 27^2 states */
    const int N = strlen(buf);
    assert(N <= sequence->size); 
    if(N == 1) {
        sequence->states[0] = 27*(buf[0] - 'a');
        sequence->size = 1;
    }
    else {
        for(int i = 0; i < N - 1; ++i) {
            sequence->states[i] = 27*(buf[i] - 'a') + (buf[i+1] - 'a' + 1);            
        }
        sequence->size = N - 1;
    }

}

void convert_hmm_states_to_syllables(hmm_sequence_t * const sequence, char * const buf) {

    if(sequence->size == 1) {
        buf[0] = (sequence->states[0] / 27) + 'a';
        if(sequence->states[0] % 27 == 0)
            buf[1] = 0;
        else {
            buf[1] = (sequence->states[0] % 27) - 1 + 'a';
            buf[2] = 0;
        }
    }
    else {
        char letter = sequence->states[0]/27 + 'a';
        buf[0] = letter;
        for(int i = 0; i < sequence->size; ++i) {
            letter = sequence->states[i] % 27 - 1 + 'a';
            buf[i+1] = letter;
        }
        buf[sequence->size + 1] = 0;
    }
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
