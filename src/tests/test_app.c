#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "utility.h"
#include "train.h"
#include "test_handler.h"
#include "viterbi.h"
#include "hmm.h"

typedef void (* viterbi_function_t)(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);

int app_wrapper_single_sequence(viterbi_function_t fun, const char fname[], _Bool debug);
int app_single_sequence();

int app_wrapper(viterbi_function_t fun, const char fname[], _Bool debug);
int app();


int main(int argc, char *argv[]) {

	test_handler_t * test_handler = test_handler_construct(2, 10);

	test_handler_add_function(test_handler, app, "App");
	test_handler_add_function(test_handler, app_single_sequence, "App");

	test_handler_run(test_handler);

	test_handler_destruct(test_handler);

	return 0;
}

int app() {
    return app_wrapper(viterbi_backward_forward_search, "./data/typos20.data", 0);
}

int app_single_sequence() {
	return app_wrapper_single_sequence(viterbi_backward_forward_search, "./data/typos20.data", 0);
}


int app_wrapper_single_sequence(viterbi_function_t fun, const char fname[], _Bool debug) {
	hmm_t * hmm = hmm_construct(27, 27);

    train_single_sequence_from_file(hmm, fname);

    FILE *f = fopen(fname, "r");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        return 1;
    }   

    size_t line_size = 0;
    size_t num_bytes = 0;
    char *line = NULL;

    /** Get to testing sequence */
    while( (num_bytes = getline(&line, &line_size, f) ) != -1 ) 
        if(line[0] == '.') break;

    /** Count length of testing sequence **/
    int seq_len = 0;
    while( (num_bytes = getline(&line, &line_size, f) ) != -1 ) 
        seq_len++;

    assert(seq_len > 0);

    /** Reopen file and move to test sequence **/
    f = fopen(fname, "r");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        return 1;
    }

    while( (num_bytes = getline(&line, &line_size, f) ) != -1 )
        if(line[0] == '.') break;

    hmm_sequence_t * sequence = hmm_sequence_construct(seq_len);
    char *true_sequence = malloc(seq_len);

    int k = 0;
    while( (num_bytes = getline(&line, &line_size, f) ) != -1 ) {
    	if (line[0] == '_' ) {
    		true_sequence[k] 		= 26;
    		sequence->emissions[k] 	= 26;
    	} else {
    		true_sequence[k]       = (unsigned) (line[0] - 'a');
    		sequence->emissions[k] = (unsigned) (line[2] - 'a');
    	}
        k++;         
    }

    assert (k == seq_len);

    profile_timer_t timer;
    fun(hmm, sequence, &timer);

    /* these hold the total number of characters where the emission does not
     * match the true state and where the state obtained by the HMM does not match
     * the true state respectively */
    unsigned total_emission_diffs = 0;
    unsigned total_estimate_diffs = 0;

    // Loop over the results

    char buffer_true[100], buffer_estimate[100], buffer_emission[100];
    unsigned estimate_diff_word = 0, emission_diff_word = 0;

    k = 0;
    for (int i = 0; i < seq_len; ++i) {

        const _Bool same_emission = true_sequence[i] == sequence->emissions[i];
        const _Bool same_estimate = true_sequence[i] == sequence->states[i];

        if(!same_emission) {
            total_emission_diffs += 1;
            emission_diff_word   += 1;
        }

        if(!same_estimate) {
            total_estimate_diffs += 1;
            estimate_diff_word   += 1;
        } 

        if(true_sequence[i] == 26 && debug) {
            buffer_true[k] = 0;
            buffer_estimate[k] = 0;
            buffer_emission[k] = 0;
            printf("%12s: %s\n", "true", buffer_true);
            printf("%12s: %s %3d\n", "emission", buffer_emission, emission_diff_word);            
            printf("%12s: %s %3d\n", "estimate", buffer_estimate, estimate_diff_word);
            printf("\n");
            estimate_diff_word = 0;
            emission_diff_word = 0;            
            k = 0;
        }
        else if(debug) {
            buffer_true[k] = true_sequence[i] + 'a';
            buffer_emission[k] = sequence->emissions[i] + 'a';
            buffer_estimate[k] = sequence->states[i] + 'a';
            ++k;
        }
    }

    printf("Sequence length: %8u\n", seq_len);
    printf("Total emission errors: %8u, rate: %5.3lf\n", total_emission_diffs, (real_t) total_emission_diffs / seq_len);
    printf("Total estimate errors: %8u, rate: %5.3lf\n", total_estimate_diffs, (real_t) total_estimate_diffs / seq_len);


    free(line);
    free(true_sequence);
    hmm_sequence_destruct(sequence);
    hmm_destruct(hmm);

    return 0;
}

int app_wrapper(viterbi_function_t fun, const char fname[], _Bool debug) {
	const int max_len = longest_word(fname);

	hmm_t * hmm = hmm_construct(26, 26);
	hmm_sequence_t * sequence = hmm_sequence_construct(max_len);
    char *line = NULL;    

    size_t word = 0;
    size_t line_size = 0;
    size_t num_bytes = 0;
    size_t test_size = 1000 * max_len;

    unsigned total_emission_diff = 0u;
    unsigned total_estimate_diff = 0u;
    unsigned *all_emissions = calloc(sizeof(unsigned), test_size);
    unsigned *all_states = calloc(sizeof(unsigned), test_size);
    unsigned *true_sequence = calloc(sizeof(unsigned), max_len);

    train_from_file(hmm, fname);
    FILE *f = fopen(fname, "r");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        exit(-1);
    }
    while( (num_bytes = getline(&line, &line_size, f) ) != -1 )
        if(line[0] == '.') break;


    /** Load all the data into buffer */
    int k = 0;
	for(int i = 0; (num_bytes = getline(&line, &line_size, f) ) != -1; ++i) {      

		if(i >= test_size) {
			test_size = 2*test_size;
			all_states = realloc(all_states, test_size*sizeof(unsigned));
			all_emissions = realloc(all_emissions, test_size*sizeof(unsigned));
		}

        if(!is_alpha(line[0])) {
        	all_states[i] = -1;
			all_emissions[i] = -1; 
		}       	
        else {
	        all_states[i]       = (unsigned) (line[0] - 'a');
	        all_emissions[i]    = (unsigned) (line[2] - 'a');   
	    }            
	    k = i;
	}
	test_size = k;
	all_states = realloc(all_states, test_size*sizeof(unsigned));
	all_emissions = realloc(all_emissions, test_size*sizeof(unsigned));	


    for(int i = 0; i < test_size; ++i) { 
    	const unsigned t = all_emissions[i];
    	if(t == -1) {
    		sequence->size = word;

			if(debug) {
				printf("%15s", "true: ");
				for(int i = 0; i < word; ++i) printf("%c", true_sequence[i] + 'a');
				printf("\n");
				printf("%15s", "emission: ");
			}
			unsigned emission_diff = 0;
			for(int i = 0; i < word; ++i) {
				int diff = true_sequence[i] - sequence->emissions[i];
				emission_diff = diff == 0 ? emission_diff : emission_diff+1;
				if(debug) printf("%c", sequence->emissions[i] + 'a');
			}
			if(debug) {
				printf("(error: %4u)", emission_diff);
				printf("\n");
			}

            profile_timer_t timer;
			fun(hmm, sequence, &timer);
		
			if(debug) printf("%15s", "estimated: ");
			unsigned estimate_diff = 0;
			for(int i = 0; i < word; ++i) {
				int diff = true_sequence[i] - sequence->states[i];
				estimate_diff = diff == 0 ? estimate_diff : estimate_diff+1;
				if(debug) printf("%c", sequence->states[i] + 'a');
			}
			if(debug){
				printf("(error: %4u)", estimate_diff);
				printf("\n\n");
			}

			total_emission_diff += emission_diff;
			total_estimate_diff += estimate_diff;

            word = 0;

    	}

	    else {
	    	sequence->emissions[word] = all_emissions[i];
	    	true_sequence[word] = all_states[i];
	    	word++;
	    }

	}

    printf("Sequence length: %8lu\n", test_size);
    printf("Total emission errors: %8u, rate: %5.3lf\n", total_emission_diff, (real_t) total_emission_diff / test_size);
    printf("Total estimate errors: %8u, rate: %5.3lf\n", total_estimate_diff, (real_t) total_estimate_diff / test_size);


    free(line);
    free(true_sequence);
    free(all_states);
    free(all_emissions);
    hmm_sequence_destruct(sequence);
    hmm_destruct(hmm);

    return 0;
}
