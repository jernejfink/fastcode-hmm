/*
    Test for two-stage HMM sequence conversion to syllables
    Future use
    @author Jernej Fink
    @date May 9 2016
*/

#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "utility.h"
#include "train.h"
#include "test_handler.h"
#include "viterbi.h"
#include "hmm.h"


int conversion();

int main(int argc, char *argv[]) {

	test_handler_t * test_handler = test_handler_construct(2, 10);

	test_handler_add_function(test_handler, conversion, "App");

	test_handler_run(test_handler);

	test_handler_destruct(test_handler);

	return 0;
}

int conversion(){
    const char test[] = "zac";
    char result[sizeof(test)];
    hmm_sequence_t * sequence = hmm_sequence_construct(sizeof(test)-1);

    convert_syllables_to_hmm_emissions((char * const) test, sequence);
    for(int i = 0; i < sequence->size; ++i) 
        sequence->states[i] = sequence->emissions[i];

    convert_hmm_states_to_syllables(sequence, (char * const) result);

    printf("%s\n", test);
    printf("%s\n", result);

    return 0;
}
