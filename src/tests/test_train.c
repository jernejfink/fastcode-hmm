
#include "train.h"
#include "hmm.h"
#include "test_handler.h"
#include "utility.h"


int trainfun();
int trainfun_log();
int trainfun_single_sequence();
int trainfun_single_sequence_log();
int trainfun_syllable();

int main(int argc, char *argv[]) {

	test_handler_t * test_handler = test_handler_construct(5, 50);
	test_handler_add_function(test_handler, trainfun, "train function");
	test_handler_add_function(test_handler, trainfun_single_sequence, "single sequence/long text HMM");
	test_handler_add_function(test_handler, trainfun_syllable, "2 stage HMM train");

	test_handler_add_function(test_handler, trainfun_log, "train function");
	test_handler_add_function(test_handler, trainfun_single_sequence_log, "single sequence/long text HMM");

	test_handler_run(test_handler);

	test_handler_destruct(test_handler);

	return 0;
}

int trainfun_syllable() {
	const int N = 27*26;

	hmm_t * hmm = hmm_construct(N, N);

	train_syllable_from_file(hmm, "./data/typos20.data");

	real_t (* P)[N] = (real_t (*)[N]) hmm->transition_probability;
	real_t (* E)[N] = (real_t (*)[N]) hmm->emission_probability;


	printf("Initial probability\n");
	printf("  ");
	for(int j = 0; j < N; ++j)
		printf("%4c ", j + 'a');
	printf("\n");	
	printf("--");
	for(int j = 0; j < N; ++j)
		printf("%4s-", "----");
	printf("\n");
	printf("  ");
	for(int i = 0; i < N; ++i) 
			if(hmm->initial_probability[i] == 0) printf("%4.2f ", hmm->initial_probability[i]);
			else printf(ANSI_COLOR_RED "%4.2f " ANSI_COLOR_RESET, hmm->initial_probability[i]);
	printf("\n");
	printf("\n");


	printf("Transition probability(x_k = down, x_k+1 = right) \n");
	for(int j = 0; j < N; ++j)
		printf("%4c ", j + 'a');
	printf("\n");	
	printf("--");
	for(int j = 0; j < N; ++j)
		printf("%4s-", "----");
	printf("\n");
	for(int i = 0; i < N; ++i) {
		printf("%c ", i + 'a');		
		for(int j = 0; j < N; ++j)
			if(P[i][j] == 0) printf("%4.2f ", P[i][j]);
			else printf(ANSI_COLOR_RED "%4.2f " ANSI_COLOR_RESET, P[i][j]);
		printf("\n");
		printf("--");
		for(int j = 0; j < 26; ++j)
			printf("%4s-", "----");
		printf("\n");
	}
	printf("\n");
	printf("\n");
	printf("Emission probability (x_k = down, z_k = right)\n");

	for(int j = 0; j < N; ++j)
		printf("%4c ", j + 'a');
	printf("\n");	
	printf("--");
		for(int j = 0; j < N; ++j)
			printf("%4s-", "----");
	printf("\n");

	for(int i = 0; i < N; ++i) {
		printf("%c ", i + 'a');		
		for(int j = 0; j < N; ++j)
			if(E[i][j] == 0) printf("%4.2f ", E[i][j]);
			else printf(ANSI_COLOR_RED "%4.2f " ANSI_COLOR_RESET, E[i][j]);
		printf("\n");
		printf("--");
		for(int j = 0; j < N; ++j)
			printf("%4s-", "----");
		printf("\n");
	}
	printf("\n");

	return 0;	
}

int trainfun_single_sequence() {
	const int N = 27;

	hmm_t * hmm = hmm_construct(N, N);

	train_single_sequence_from_file(hmm, "./data/typos20.data");
	real_t (* P)[N] = (real_t (*)[N]) hmm->transition_probability;
	real_t (* E)[N] = (real_t (*)[N]) hmm->emission_probability;


	printf("Initial probability\n");
	printf("  ");
	for(int j = 0; j < N; ++j)
		printf("%4c ", j + 'a');
	printf("\n");	
	printf("--");
	for(int j = 0; j < N; ++j)
		printf("%4s-", "----");
	printf("\n");
	printf("  ");
	for(int i = 0; i < N; ++i) 
			if(hmm->initial_probability[i] == 0) printf("%4.2f ", hmm->initial_probability[i]);
			else printf(ANSI_COLOR_RED "%4.2f " ANSI_COLOR_RESET, hmm->initial_probability[i]);
	printf("\n");
	printf("\n");


	printf("Transition probability(x_k = down, x_k+1 = right) \n");
	for(int j = 0; j < N; ++j)
		printf("%4c ", j + 'a');
	printf("\n");	
	printf("--");
	for(int j = 0; j < N; ++j)
		printf("%4s-", "----");
	printf("\n");
	for(int i = 0; i < N; ++i) {
		printf("%c ", i + 'a');		
		for(int j = 0; j < N; ++j)
			if(P[i][j] == 0) printf("%4.2f ", P[i][j]);
			else printf(ANSI_COLOR_RED "%4.2f " ANSI_COLOR_RESET, P[i][j]);
		printf("\n");
		printf("--");
		for(int j = 0; j < 26; ++j)
			printf("%4s-", "----");
		printf("\n");
	}
	printf("\n");
	printf("\n");
	printf("Emission probability (x_k = down, z_k = right)\n");

	for(int j = 0; j < N; ++j)
		printf("%4c ", j + 'a');
	printf("\n");	
	printf("--");
		for(int j = 0; j < N; ++j)
			printf("%4s-", "----");
	printf("\n");

	for(int i = 0; i < N; ++i) {
		printf("%c ", i + 'a');		
		for(int j = 0; j < N; ++j)
			if(E[i][j] == 0) printf("%4.2f ", E[i][j]);
			else printf(ANSI_COLOR_RED "%4.2f " ANSI_COLOR_RESET, E[i][j]);
		printf("\n");
		printf("--");
		for(int j = 0; j < N; ++j)
			printf("%4s-", "----");
		printf("\n");
	}
	printf("\n");

	unsigned count_E = 0;
	unsigned count_P = 0;
	for(int i = 0; i < N; ++i)
		for(int j = 0; j < N; ++j) {
			count_P = (P[i][j] > 1e-6) ? count_P + 1 : count_P;
			count_E = (E[i][j] > 1e-6) ? count_E + 1 : count_E;
		}

	printf("Num non-zero P: %3d out of %3d\n", count_P, N*N);
	printf("Num non-zero E: %3d out of %3d\n", count_E, N*N);


	return 0;
}


int trainfun() {
	hmm_t * hmm = hmm_construct(26, 26);

	train_from_file(hmm, "./data/typos20.data");
	real_t (* P)[26] = (real_t (*)[26]) hmm->transition_probability;
	real_t (* E)[26] = (real_t (*)[26]) hmm->emission_probability;


	printf("Initial probability\n");
	printf("  ");
	for(int j = 0; j < 26; ++j)
		printf("%4c ", j + 'a');
	printf("\n");	
	printf("--");
	for(int j = 0; j < 26; ++j)
		printf("%4s-", "----");
	printf("\n");
	printf("  ");
	for(int i = 0; i < 26; ++i) 
			if(hmm->initial_probability[i] == 0) printf("%4.2f ", hmm->initial_probability[i]);
			else printf(ANSI_COLOR_RED "%4.2f " ANSI_COLOR_RESET, hmm->initial_probability[i]);
	printf("\n");
	printf("\n");


	printf("Transition probability(x_k = down, x_k+1 = right) \n");
	for(int j = 0; j < 26; ++j)
		printf("%4c ", j + 'a');
	printf("\n");	
	printf("--");
	for(int j = 0; j < 26; ++j)
		printf("%4s-", "----");
	printf("\n");
	for(int i = 0; i < 26; ++i) {
		printf("%c ", i + 'a');		
		for(int j = 0; j < 26; ++j)
			if(P[i][j] == 0) printf("%4.2f ", P[i][j]);
			else printf(ANSI_COLOR_RED "%4.2f " ANSI_COLOR_RESET, P[i][j]);
		printf("\n");
		printf("--");
		for(int j = 0; j < 26; ++j)
			printf("%4s-", "----");
		printf("\n");
	}
	printf("\n");
	printf("\n");
	printf("Emission probability (x_k = down, z_k = right)\n");

	for(int j = 0; j < 26; ++j)
		printf("%4c ", j + 'a');
	printf("\n");	
	printf("--");
		for(int j = 0; j < 26; ++j)
			printf("%4s-", "----");
	printf("\n");

	for(int i = 0; i < 26; ++i) {
		printf("%c ", i + 'a');		
		for(int j = 0; j < 26; ++j)
			if(E[i][j] == 0) printf("%4.2f ", E[i][j]);
			else printf(ANSI_COLOR_RED "%4.2f " ANSI_COLOR_RESET, E[i][j]);
		printf("\n");
		printf("--");
		for(int j = 0; j < 26; ++j)
			printf("%4s-", "----");
		printf("\n");
	}
	printf("\n");

	return 0;
}

int trainfun_single_sequence_log() {
	const int N = 27;

	hmm_t * hmm = hmm_construct(N, N);

	const int kp = 32;

	train_single_sequence_from_file(hmm, "./data/typos20.data");
	//real_t (* P)[N] = (real_t (*)[N]) hmm->transition_probability;
	real_t (* E)[N] = (real_t (*)[N]) hmm->emission_probability;

	real_t (* P) = hmm->transition_probability;


	printf("Initial probability\n");
	printf("  ");
	for(int j = 0; j < N; ++j)
		printf("%4c ", j + 'a');
	printf("\n");	
	printf("--");
	for(int j = 0; j < N; ++j)
		printf("%4s-", "----");
	printf("\n");
	printf("  ");
	for(int i = 0; i < N; ++i) 
			if(hmm->initial_probability[i] == INFINITY) printf("%4.2f ", 0.0);
			else printf(ANSI_COLOR_RED "%4.2f " ANSI_COLOR_RESET, exp(-hmm->initial_probability[i]));
	printf("\n");
	printf("\n");


	printf("Transition probability(x_k = down, x_k+1 = right) \n");
	for(int j = 0; j < N; ++j)
		printf("%4c ", j + 'a');
	printf("\n");	
	printf("--");
	for(int j = 0; j < N; ++j)
		printf("%4s-", "----");
	printf("\n");
	for(int i = 0; i < N; ++i) {
		printf("%c ", i + 'a');		
		for(int j = 0; j < N; ++j)
			if(P[i*kp + j] == INFINITY) printf("%4.2f ", 0.0);
			else printf(ANSI_COLOR_RED "%4.2f " ANSI_COLOR_RESET, exp(-P[i*kp + j]));
		printf("\n");
		printf("--");
		for(int j = 0; j < 26; ++j)
			printf("%4s-", "----");
		printf("\n");
	}
	printf("\n");
	printf("\n");
	printf("Emission probability (x_k = down, z_k = right)\n");

	for(int j = 0; j < N; ++j)
		printf("%4c ", j + 'a');
	printf("\n");	
	printf("--");
		for(int j = 0; j < N; ++j)
			printf("%4s-", "----");
	printf("\n");

	for(int i = 0; i < N; ++i) {
		printf("%c ", i + 'a');		
		for(int j = 0; j < N; ++j)
			if(E[i][j] == INFINITY) printf("%4.2f ", 0.0);
			else printf(ANSI_COLOR_RED "%4.2f " ANSI_COLOR_RESET, exp(-E[i][j]));
		printf("\n");
		printf("--");
		for(int j = 0; j < N; ++j)
			printf("%4s-", "----");
		printf("\n");
	}
	printf("\n");

	unsigned count_E = 0;
	unsigned count_P = 0;
	for(int i = 0; i < N; ++i)
		for(int j = 0; j < N; ++j) {
			count_P = (P[i*kp + j] > -log(1e-6)) ? count_P + 1 : count_P;
			count_E = (E[i][j] > -log(1e-6)) ? count_E + 1 : count_E;
		}

	printf("Num non-zero P: %3d out of %3d\n", count_P, N*N);
	printf("Num non-zero E: %3d out of %3d\n", count_E, N*N);


	return 0;
}


int trainfun_log() {
	hmm_t * hmm = hmm_construct(26, 26);

	const int kp = 32;
	train_from_file(hmm, "./data/typos20.data");
	//real_t (* P)[26] = (real_t (*)[26]) hmm->transition_probability;
	real_t* P = hmm->transition_probability;
	real_t (* E)[26] = (real_t (*)[26]) hmm->emission_probability;


	printf("Initial probability\n");
	printf("  ");
	for(int j = 0; j < 26; ++j)
		printf("%4c ", j + 'a');
	printf("\n");	
	printf("--");
	for(int j = 0; j < 26; ++j)
		printf("%4s-", "----");
	printf("\n");
	printf("  ");
	for(int i = 0; i < 26; ++i) 
			if(hmm->initial_probability[i] == INFINITY) printf("%4.2f ", 0.0);
			else printf(ANSI_COLOR_RED "%4.2f " ANSI_COLOR_RESET, exp(-hmm->initial_probability[i]));
	printf("\n");
	printf("\n");


	printf("Transition probability(x_k = down, x_k+1 = right) \n");
	for(int j = 0; j < 26; ++j)
		printf("%4c ", j + 'a');
	printf("\n");	
	printf("--");
	for(int j = 0; j < 26; ++j)
		printf("%4s-", "----");
	printf("\n");
	for(int i = 0; i < 26; ++i) {
		printf("%c ", i + 'a');		
		for(int j = 0; j < 26; ++j)
			if(P[i*kp + j] == INFINITY) printf("%4.2f ", 0.0);
			else printf(ANSI_COLOR_RED "%4.2f " ANSI_COLOR_RESET, exp(-P[i*kp + j]));
		printf("\n");
		printf("--");
		for(int j = 0; j < 26; ++j)
			printf("%4s-", "----");
		printf("\n");
	}
	printf("\n");
	printf("\n");
	printf("Emission probability (x_k = down, z_k = right)\n");

	for(int j = 0; j < 26; ++j)
		printf("%4c ", j + 'a');
	printf("\n");	
	printf("--");
		for(int j = 0; j < 26; ++j)
			printf("%4s-", "----");
	printf("\n");

	for(int i = 0; i < 26; ++i) {
		printf("%c ", i + 'a');		
		for(int j = 0; j < 26; ++j)
			if(E[i][j] == INFINITY) printf("%4.2f ", 0.0);
			else printf(ANSI_COLOR_RED "%4.2f " ANSI_COLOR_RESET, exp(-E[i][j]));
		printf("\n");
		printf("--");
		for(int j = 0; j < 26; ++j)
			printf("%4s-", "----");
		printf("\n");
	}
	printf("\n");

	return 0;
}
