#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "utility.h"
#include "train.h"
#include "test_handler.h"
#include "viterbi.h"
#include "hmm.h"

typedef void (* viterbi_function_t)(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);


int hmm_alignment();
int hmm_transpose();

int main(int argc, char *argv[]) {

	test_handler_t * test_handler = test_handler_construct(2, 20);

	test_handler_add_function(test_handler, hmm_alignment, "hmm_alignment");
    test_handler_add_function(test_handler, hmm_transpose, "hmm_transpose");

	test_handler_run(test_handler);

	test_handler_destruct(test_handler);

	return 0;
}

int hmm_transpose() {

    const int N = 27;

    hmm_t * hmm = hmm_construct(N, N);
    read_real_from_file("./data/fink_tran_aligned.txt", hmm->transition_probability, N*N);
    read_real_from_file("./data/fink_emis_aligned.txt", hmm->emission_probability, N*N);
    read_real_from_file("./data/fink_init_aligned.txt", hmm->initial_probability, N);   

    hmm_normalize_log(hmm);

    const unsigned k = hmm->num_states;
    const unsigned l = hmm->num_emissions;

    real_t (* P)[k] = (real_t (*)[k]) hmm->transition_probability;
    real_t (* E)[l] = (real_t (*)[l]) hmm->emission_probability;  

    real_t (* P_tr)[k] = (real_t (*)[k]) hmm->transition_probability_tr;
    real_t (* E_tr)[k] = (real_t (*)[k]) hmm->emission_probability_tr;  

    for(int i = 0; i < k; ++i)
        for(int j = 0; j < k; ++j)
            assert(P[i][j] == P_tr[j][i]);

    for(int i = 0; i < k; ++i)
        for(int j = 0; j < l; ++j)
            assert(E[i][j] == E_tr[j][i]);            

    return 0;
}

int hmm_alignment() {
    const int N = 24;
    hmm_t * hmm = hmm_construct(N, N);

    real_t (* P)[N] = (real_t (*)[N]) hmm->transition_probability;
    real_t (* E)[N] = (real_t (*)[N]) hmm->emission_probability;

    printf("initial vector\n");    
    printf("%8s %12s\n", "pointer", "PASS");
    printf("%8lu", (unsigned long) hmm->initial_probability);    
    PRINTF_T_F(((unsigned long) &hmm->initial_probability % 8*sizeof(real_t)) == 0);
    printf("\n");

    printf("emission matrix\n");
    printf("%8s %12s\n", "pointer", "PASS");
    for(int i = 0; i < N; ++i) {
        printf("%8lu", (unsigned long) &E[i]);
        PRINTF_T_F(((unsigned long) &E[i] % 8*sizeof(real_t)) == 0);
        printf("\n");
    }    

    printf("transition matrix\n");
    printf("%8s %12s\n", "pointer", "PASS");
    for(int i = 0; i < N; ++i) {
        printf("%8lu", (unsigned long) &P[i]);
        PRINTF_T_F(((unsigned long) &P[i] % 8*sizeof(real_t)) == 0);
        printf("\n");
    }

    return 0;
}