/*
	Test for any viterbi optimization that you make

	Change when adding a new optimization 

	@author Jernej
	@date May 9

*/

#include "viterbi.h"
#include "test_handler.h"
#include "utility.h"
#include <stdio.h>
#include <stdlib.h>

typedef void (* viterbi_function_t)(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);

int case_log_fink_1();
int case_log_fink_11();
int case_log_sam_aligned();
int case_log_mojmir_1();
int case_log_mojmir_3();
int case_log_fink_9();

int case_ghmm();

int wrapper(viterbi_function_t fun);
int wrapper_log(viterbi_function_t fun);
int wrapper_log_aligned(viterbi_function_t fun);


int main(int argc, char *argv[]) {
	test_handler_t * test_handler = test_handler_construct(6, 50);
	
	test_handler_add_function(test_handler, case_log_fink_1, "log_fink_1");
	test_handler_add_function(test_handler, case_log_fink_11, "log_fink_11");
	test_handler_add_function(test_handler, case_log_sam_aligned, "log_sam_aligned");
	test_handler_add_function(test_handler, case_log_mojmir_1, "log_mojmir_1");
	test_handler_add_function(test_handler, case_log_mojmir_3, "log_mojmir_3");
	test_handler_add_function(test_handler, case_log_fink_9, "log_fink_9");

	test_handler_run(test_handler);
	test_handler_destruct(test_handler);
}

int case_log_fink_1() {
	return wrapper_log_aligned(viterbi_log_fink_1);
}
int case_log_fink_11() {
	return wrapper_log_aligned(viterbi_log_fink_11);
}
int case_log_sam_aligned() {
	return wrapper_log_aligned(viterbi_log_sam_aligned);
}
int case_log_mojmir_1() {
	return wrapper_log_aligned(viterbi_log_mojmir_1);
}
int case_log_mojmir_3() {
	return wrapper_log_aligned(viterbi_log_mojmir_3);
}
int case_log_fink_9() {
	return wrapper_log_aligned(viterbi_log_fink_9);
}

int wrapper_log_aligned(viterbi_function_t fun) {
	const int N = 32;
	hmm_t * hmm = hmm_construct(N, N);
	hmm_sequence_t * sequence = hmm_sequence_construct(27);
   
   	unsigned *matlab_estimates = calloc(sizeof(unsigned), 27);

	read_real_from_file("./data/fink_tran_aligned.txt", hmm->transition_probability, N*N);
	read_real_from_file("./data/fink_emis_aligned.txt", hmm->emission_probability, N*N);
	read_real_from_file("./data/fink_init_aligned.txt", hmm->initial_probability, N);	
	read_unsigned_from_file("./data/fink_emissions_aligned.txt", sequence->emissions, 27);
	read_unsigned_from_file("./data/fink_estimates_aligned.txt", matlab_estimates, 27);

	hmm_normalize_log(hmm);

	profile_timer_t timer;
	fun(hmm, sequence, &timer);
	
	printf("%8s %8s %8s %12s\n", "emission", "matlab", "our", "PASS");
	for(int i = 0; i < 27; ++i) {
		printf("%8d %8d %8d", sequence->emissions[i], matlab_estimates[i], sequence->states[i]);
		PRINTF_T_F(matlab_estimates[i] == sequence->states[i]);
		printf("\n");
	}

	return 0;
}
