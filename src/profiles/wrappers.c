#include "wrappers.h"

int profile_dense_aligned_sequence_wrapper_log(viterbi_function_t fun, unsigned n, profile_timer_t * const timer) 
{
	const int N = 32;	
	hmm_t * hmm = hmm_construct(N, N);
	read_real_from_file("./data/fink_tran_aligned.txt", hmm->transition_probability, N*N);
	read_real_from_file("./data/fink_emis_aligned.txt", hmm->emission_probability, N*N);
	read_real_from_file("./data/fink_init_aligned.txt", hmm->initial_probability, N);	

	hmm_normalize_log(hmm);


	hmm_sequence_t * sequence = hmm_sequence_construct(n);
    srand(1245);
	for(int i = 0; i < n; ++i) sequence->emissions[i] = rand() % N;	

	fun(hmm, sequence, timer);

    hmm_sequence_destruct(sequence);
    hmm_destruct(hmm);

    return 0;
}


int profile_huge_state_wrapper_log(viterbi_function_t fun, unsigned n, profile_timer_t * const timer) 
{
	const int N = 640;	
    srand(1245);

	hmm_t * hmm = hmm_construct(N, N);

	real_t (* P)[N] = (real_t (*)[N]) hmm->transition_probability;
	real_t (* E)[N] = (real_t (*)[N]) hmm->emission_probability;

	for(int i = 0; i < N; ++i) {
		for(int j = 0; j < N; ++j) {
			E[i][j] = ((real_t) rand()/(real_t) RAND_MAX);
			P[i][j] = ((real_t) rand()/(real_t) RAND_MAX);
		}
		hmm->initial_probability[i] = ((real_t) rand()/(real_t) RAND_MAX);
	}

	hmm_normalize_log(hmm);

	hmm_sequence_t * sequence = hmm_sequence_construct(n);
	for(int i = 0; i < n; ++i) sequence->emissions[i] = rand() % N;	
	fun(hmm, sequence, timer);
    hmm_sequence_destruct(sequence);
    hmm_destruct(hmm);

    return 0;
}
