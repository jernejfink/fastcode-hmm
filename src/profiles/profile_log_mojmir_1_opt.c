#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "utility.h"
#include "train.h"
#include "profile_handler.h"
#include "wrappers.h"
#include "viterbi.h"
#include "hmm.h"

int profile_dense_aligned_sequence(unsigned n, profile_timer_t * const timer) {
	return profile_dense_aligned_sequence_wrapper_log(viterbi_log_mojmir_1_opt, n, timer);
}

unsigned long long profile_dense_aligned_sequence_opcount(const unsigned long long n) {
	/** 
	Flops
	=======
	4n * ( 65*8 adds/comp , 62*8 max/mins )

	
	Loads
	=============
	4n * ( 1 + 8*8 + 1) * 8 float loads
	4n * (66) * 8 float loads
	
	Stores
	=============
	4*n*2 stores 

	stores + loads together floats
	4n*(68) floats

	Opt Int
	============
	(65*2 + 62*2) / (68 ) = 3.73 bytes/cycle

	**/


	/*
	// emission + seq + tp + cost + path + initial 
	4* (k^2 + n + k^2 + 2*k + n*k + k)  = 
	*/

	// Conservative Case
	//return 4*n*(65*8);

	// Mins/Maxs as adds

	// new 
	unsigned long long k = 32; 

	return 4*(n-1ull)*(65ull*8ull + 62ull*8ull) + 2ull*k + 2ull*k*(k-1ull);
	
}

unsigned long long profile_huge_state_opcount(const unsigned long long n) {
	//return 640ull*640ull*( n-1ull + n-1ull );
	return 0; 
}

int main(int argc, char *argv[]) {

	const int dense_sizes[]  = {1000, 2000, 3000, 4000, 5000, 5000, 6000, 7000, 8000, 9000, 10000, 11000, 12000, 13000, 14000, 15000, 16000, 17000, 18000, 19000, 20000, 30000, 50000, 70000, 100000, 300000, 500000, 700000, 1000000};

	const unsigned dense_num_sizes =  sizeof(dense_sizes) / sizeof(unsigned);

	const unsigned repeats = 3;

	profile_handler_t * profile_handler_dense_aligned  = profile_handler_construct(profile_dense_aligned_sequence,  repeats, (const unsigned * const) dense_sizes,  dense_num_sizes,  profile_dense_aligned_sequence_opcount);

	char dense_name[100];

	strcpy(dense_name, argv[1]);
	strcpy(dense_name + strlen(dense_name), "_dense_aligned.txt");

	profile_handler_run(profile_handler_dense_aligned,  dense_name);

	profile_handler_destruct(profile_handler_dense_aligned);

	return 0;
}