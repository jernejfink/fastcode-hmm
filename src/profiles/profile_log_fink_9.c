#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "utility.h"
#include "train.h"
#include "profile_handler.h"
#include "wrappers.h"
#include "viterbi.h"
#include "hmm.h"

int profile_dense_aligned_sequence(unsigned n, profile_timer_t * const timer) {
	return profile_dense_aligned_sequence_wrapper_log(viterbi_log_fink_9, n, timer);
}


unsigned long long profile_dense_aligned_sequence_opcount(const unsigned long long n) {

	/*
	unsigned long long cmp = 32ull*32ull*(n-1);
	unsigned long long tr_prob = 32ull*32ull*(n-1);
	//unsigned long long flt_idx = 32ull*(n-1);
	unsigned long long em_prob = 32ull*(n-1);
	unsigned long long final_cmp = 32ull;
	*/
	unsigned long long k = 32;
	unsigned long long l = (unsigned long long) ((double)k /32.0);
	return (n-1ull)*l*(64ull + (k-1ull)*72ull ) + k;
}

int main(int argc, char *argv[]) {

	const int dense_sizes[]  = {1000, 2000, 3000, 4000, 5000, 5000, 6000, 7000, 8000, 9000, 10000, 11000, 12000, 13000, 14000, 15000, 16000, 17000, 18000, 19000, 20000, 30000, 50000, 70000, 100000, 300000, 500000, 700000, 1000000};

	const unsigned dense_num_sizes =  sizeof(dense_sizes) / sizeof(unsigned);

	const unsigned repeats = 3;

	profile_handler_t * profile_handler_dense_aligned  = profile_handler_construct(profile_dense_aligned_sequence,  repeats, (const unsigned * const) dense_sizes,  dense_num_sizes,  profile_dense_aligned_sequence_opcount);

	char dense_name[100];

	strcpy(dense_name, argv[1]);
	strcpy(dense_name + strlen(dense_name), "_dense_aligned.txt");

	profile_handler_run(profile_handler_dense_aligned,  dense_name);

	profile_handler_destruct(profile_handler_dense_aligned);

	return 0;
}