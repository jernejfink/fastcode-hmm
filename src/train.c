#include "train.h"

_Bool is_alpha(const char ch);


void train_add_single_sample(const hmm_t * unnormalized_hmm, const int state, const int state_next, const int emission) {
    assert(unnormalized_hmm != NULL);
    // we can have state_next = -1 representing last state
    assert(state_next >= -1 && state_next < (int) unnormalized_hmm->num_states);
    assert(state >=0 && state < unnormalized_hmm->num_states);
    assert(emission >= 0 && emission < unnormalized_hmm->num_emissions);

    const int n = unnormalized_hmm->num_states;

    real_t (* P)[n] = (real_t (*)[n]) unnormalized_hmm->transition_probability;
    real_t (* E)[n] = (real_t (*)[n]) unnormalized_hmm->emission_probability;

    if (state_next != -1)
        P[state][state_next]++;
    E[state][emission]++;
}

void train_single_sequence_from_file(const hmm_t * unnormalized_hmm, const char file[]) {

    size_t line_size = 0;
    size_t num_bytes = 0;
    char *line = NULL;

    unsigned  state;
    unsigned  state_next;  
    unsigned  emission;
    unsigned  emission_next;    

    FILE *f = fopen(file, "r");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        return;
    }    

    // Get first state and emission
    num_bytes = getline(&line, &line_size, f);
    assert (num_bytes > 0);

    state       = (unsigned) (line[0] - 'a');
    emission    = (unsigned) (line[2] - 'a');

    // add the first letter to the initial prob distribution
    unnormalized_hmm->initial_probability[state]++;

    while( (num_bytes = getline(&line, &line_size, f) ) != -1 ) {

        if (line[0] == '.')
            break;

        if (line[0] == '_') {
            // spaces are represented by underscores and are always transcribed correctly
            // space should always be the last state
            state_next       = 26;
            emission_next    = 26;

        } else {
            state_next       = (unsigned) (line[0] - 'a');
            emission_next    = (unsigned) (line[2] - 'a');
        }  

        train_add_single_sample(unnormalized_hmm, state, state_next, emission);
        state       = state_next;
        emission    = emission_next;
    }

    // add last emission
    train_add_single_sample(unnormalized_hmm, state, -1, emission);

    // initial probability
    const int n = unnormalized_hmm->num_states;
    real_t (* P)[n] = (real_t (*)[n]) unnormalized_hmm->transition_probability;
    
    for (int i = 0; i < n; i++) {
        unnormalized_hmm->initial_probability[i] = P[26][i];
    }

    hmm_normalize(unnormalized_hmm);

    free(line);
}

void train_add_sample(const hmm_t * unnormalized_hmm, const hmm_sequence_t * const training_sequence) {
    assert(unnormalized_hmm != NULL);
    assert(training_sequence != NULL);
    assert(training_sequence->size > 0);

    const int n = unnormalized_hmm->num_states;

    real_t (* P)[n] = (real_t (*)[n]) unnormalized_hmm->transition_probability;
    real_t (* E)[n] = (real_t (*)[n]) unnormalized_hmm->emission_probability;

    unnormalized_hmm->initial_probability[training_sequence->states[0]]++;
    for(int k = 0; k < training_sequence->size - 1; ++k) {
        const unsigned i = training_sequence->states[k];
        const unsigned j = training_sequence->states[k+1];
        const unsigned z = training_sequence->emissions[k];
        P[i][j]++;
        E[i][z]++;
    }
    const unsigned z = training_sequence->emissions[training_sequence->size - 1];
    const unsigned i = training_sequence->states[training_sequence->size - 1];
    E[i][z]++;
}

void train_syllable_from_file(const hmm_t * unnormalized_hmm, const char file[]) {
    FILE *f = fopen(file, "r");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        return;
    }

    size_t line_size = 0;
    size_t num_bytes = 0;
    char *line = NULL;

    size_t longest_word = 0;
    size_t word = 0;

    /* Determine the longest word */

    while( (num_bytes = getline(&line, &line_size, f) ) != -1 ) {
        word = !is_alpha(line[0]) ? 0 : word + 1;
        longest_word = word > longest_word ? word : longest_word;
    }
    free(line);
    fclose(f);

    f = fopen(file, "r");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        return;
    }    

    /* Read file again, this time do the actual training */
    line = NULL;
    line_size = 0;
    hmm_sequence_t * sequence = hmm_sequence_construct(longest_word - 1);
    char * correct = calloc(sizeof(char), longest_word);
    char * corrupt = calloc(sizeof(char), longest_word);

    word = 0;

    while( (num_bytes = getline(&line, &line_size, f) ) != -1 ) {       
        if(!is_alpha(line[0])) {
            corrupt[word] = 0;
            correct[word] = 0;
            convert_syllables_to_hmm_states(correct, sequence);
            convert_syllables_to_hmm_emissions(corrupt, sequence);
            train_add_sample(unnormalized_hmm, sequence);
            word = 0;
        }
        else {
            correct[word] = line[0];
            corrupt[word] = line[2];   
            word++;     
        }
    }

    hmm_normalize(unnormalized_hmm);

    free(line);
    free(correct);
    free(corrupt);
    hmm_sequence_destruct(sequence);
}

void train_from_file(const hmm_t * unnormalized_hmm, const char file[]) {
    FILE *f = fopen(file, "r");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        return;
    }

    size_t line_size = 0;
    size_t num_bytes = 0;
    char *line = NULL;

    size_t longest_word = 0;
    size_t word = 0;

    /* Determine the longest word */

    while( (num_bytes = getline(&line, &line_size, f) ) != -1 ) {
        word = !is_alpha(line[0]) ? 0 : word + 1;
        longest_word = word > longest_word ? word : longest_word;
    }
    free(line);
    fclose(f);

    f = fopen(file, "r");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        return;
    }    

    /* Read file again, this time do the actual training */
    line = NULL;
    line_size = 0;
    hmm_sequence_t * sequence = hmm_sequence_construct(longest_word);
    word = 0;

    while( (num_bytes = getline(&line, &line_size, f) ) != -1 ) {       
        if(!is_alpha(line[0])) {
            sequence->size = word;
            train_add_sample(unnormalized_hmm, sequence);
            word = 0;
            continue;
        }
        sequence->states[word]    = (unsigned) (line[0] - 'a');
        sequence->emissions[word] = (unsigned) (line[2] - 'a');   
        word++;     
    }

    hmm_normalize(unnormalized_hmm);

    free(line);
    hmm_sequence_destruct(sequence);
}

void train_single_sequence_from_file_log(const hmm_t * unnormalized_hmm, const char file[]) {

    size_t line_size = 0;
    size_t num_bytes = 0;
    char *line = NULL;

    unsigned  state;
    unsigned  state_next;  
    unsigned  emission;
    unsigned  emission_next;    

    FILE *f = fopen(file, "r");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        return;
    }    

    // Get first state and emission
    num_bytes = getline(&line, &line_size, f);
    assert (num_bytes > 0);

    state       = (unsigned) (line[0] - 'a');
    emission    = (unsigned) (line[2] - 'a');

    // add the first letter to the initial prob distribution
    unnormalized_hmm->initial_probability[state]++;

    while( (num_bytes = getline(&line, &line_size, f) ) != -1 ) {

        if (line[0] == '.')
            break;

        if (line[0] == '_') {
            // spaces are represented by underscores and are always transcribed correctly
            // space should always be the last state
            state_next       = 26;
            emission_next    = 26;

        } else {
            state_next       = (unsigned) (line[0] - 'a');
            emission_next    = (unsigned) (line[2] - 'a');
        }  

        train_add_single_sample(unnormalized_hmm, state, state_next, emission);
        state       = state_next;
        emission    = emission_next;
    }

    // add last emission
    train_add_single_sample(unnormalized_hmm, state, -1, emission);

    // initial probability
    const int n = unnormalized_hmm->num_states;
    real_t (* P)[n] = (real_t (*)[n]) unnormalized_hmm->transition_probability;
    
    for (int i = 0; i < n; i++) {
        unnormalized_hmm->initial_probability[i] = P[26][i];
    }

    hmm_normalize(unnormalized_hmm);

    free(line);
}

void train_syllable_from_file_log(const hmm_t * unnormalized_hmm, const char file[]) {
    FILE *f = fopen(file, "r");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        return;
    }

    size_t line_size = 0;
    size_t num_bytes = 0;
    char *line = NULL;

    size_t longest_word = 0;
    size_t word = 0;

    /* Determine the longest word */

    while( (num_bytes = getline(&line, &line_size, f) ) != -1 ) {
        word = !is_alpha(line[0]) ? 0 : word + 1;
        longest_word = word > longest_word ? word : longest_word;
    }
    free(line);
    fclose(f);

    f = fopen(file, "r");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        return;
    }    

    /* Read file again, this time do the actual training */
    line = NULL;
    line_size = 0;
    hmm_sequence_t * sequence = hmm_sequence_construct(longest_word - 1);
    char * correct = calloc(sizeof(char), longest_word);
    char * corrupt = calloc(sizeof(char), longest_word);

    word = 0;

    while( (num_bytes = getline(&line, &line_size, f) ) != -1 ) {       
        if(!is_alpha(line[0])) {
            corrupt[word] = 0;
            correct[word] = 0;
            convert_syllables_to_hmm_states(correct, sequence);
            convert_syllables_to_hmm_emissions(corrupt, sequence);
            train_add_sample(unnormalized_hmm, sequence);
            word = 0;
        }
        else {
            correct[word] = line[0];
            corrupt[word] = line[2];   
            word++;     
        }
    }

    hmm_normalize_log(unnormalized_hmm);

    free(line);
    free(correct);
    free(corrupt);
    hmm_sequence_destruct(sequence);
}

void train_from_file_log(const hmm_t * unnormalized_hmm, const char file[]) {
    FILE *f = fopen(file, "r");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        return;
    }

    size_t line_size = 0;
    size_t num_bytes = 0;
    char *line = NULL;

    size_t longest_word = 0;
    size_t word = 0;

    /* Determine the longest word */

    while( (num_bytes = getline(&line, &line_size, f) ) != -1 ) {
        word = !is_alpha(line[0]) ? 0 : word + 1;
        longest_word = word > longest_word ? word : longest_word;
    }
    free(line);
    fclose(f);

    f = fopen(file, "r");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        return;
    }    

    /* Read file again, this time do the actual training */
    line = NULL;
    line_size = 0;
    hmm_sequence_t * sequence = hmm_sequence_construct(longest_word);
    word = 0;

    while( (num_bytes = getline(&line, &line_size, f) ) != -1 ) {       
        if(!is_alpha(line[0])) {
            sequence->size = word;
            train_add_sample(unnormalized_hmm, sequence);
            word = 0;
            continue;
        }
        sequence->states[word]    = (unsigned) (line[0] - 'a');
        sequence->emissions[word] = (unsigned) (line[2] - 'a');   
        word++;     
    }

    hmm_normalize_log(unnormalized_hmm);

    free(line);
    hmm_sequence_destruct(sequence);
}