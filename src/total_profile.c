#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "utility.h"
#include "train.h"
#include "profile_handler.h"
#include "wrappers.h"
#include "viterbi.h"
#include "hmm.h"

typedef void (* viterbi_function_t)(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);
typedef int (* fprof)(const unsigned n, profile_timer_t * const);
typedef unsigned long long (* fopc)(const unsigned n, const unsigned k);

int profile(viterbi_function_t fun, unsigned n, profile_timer_t * const timer);
void group_profile(const fprof * const function,
                   const fopc * const opcount,
                   const char *fnames[],
                   const int num_funs,
                   const int * const sizes, 
                   const int num_sizes, 
                   const int repeats,
                   const char fname[]);
int group_test(const viterbi_function_t * const function,
               const char *fnames[],
               const int num_funs);

void cache_scrambler();

int fink_1(unsigned n, profile_timer_t * const timer);
int fink_1_opt(unsigned n, profile_timer_t * const timer);
int fink_11(unsigned n, profile_timer_t * const timer);
int fink_11_opt(unsigned n, profile_timer_t * const timer);
int sam_aligned(unsigned n, profile_timer_t * const timer);
int sam_aligned_opt(unsigned n, profile_timer_t * const timer);
int mojmir_1(unsigned n, profile_timer_t * const timer);
int mojmir_1_opt(unsigned n, profile_timer_t * const timer);
int mojmir_3(unsigned n, profile_timer_t * const timer);
int mojmir_3_opt(unsigned n, profile_timer_t * const timer);
int fink_9(unsigned n, profile_timer_t * const timer);
int fink_9_opt(unsigned n, profile_timer_t * const timer);

unsigned long long op_fink_1(const unsigned n, const unsigned  k);
unsigned long long op_fink_11(const unsigned  n, const unsigned  k);
unsigned long long op_sam_aligned(const unsigned  n, const unsigned  k);
unsigned long long op_mojmir_1(const unsigned  n, const unsigned  k);
unsigned long long op_mojmir_3(const unsigned  n, const unsigned  k);
unsigned long long op_fink_9(const unsigned  n, const unsigned  k);

int main(int argc, char *argv[]) {

    if(argc != 2) {
        printf("Usage total_profile.o profile_output.txt\n");
        exit(-1);
    }

    const int size[]  = {1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000,
                         10000, 20000, 30000, 40000, 50000, 60000, 70000, 80000, 90000, 100000,
                         100000, 200000, 300000, 400000, 500000, 600000, 700000, 800000, 900000, 1000000};

    const int dense_num_sizes =  sizeof(size) / sizeof(int);
    const int repeats = 10;
    const viterbi_function_t viterbi_functions[] = {
        viterbi_log_fink_1,
        viterbi_log_fink_11,
        viterbi_log_sam_aligned,        
        viterbi_log_mojmir_1, 
        viterbi_log_mojmir_3,        
        viterbi_log_fink_9,
        viterbi_log_fink_1_opt,
        viterbi_log_fink_11_opt,
        viterbi_log_sam_aligned_opt,
        viterbi_log_mojmir_1_opt,
        viterbi_log_mojmir_3_opt,
        viterbi_log_fink_9_opt        
    };
    const fprof function[] = {
        fink_1, 
        fink_11,
        sam_aligned,
        mojmir_1,
        mojmir_3,
        fink_9,
        fink_1_opt,
        fink_11_opt,
        sam_aligned_opt,
        mojmir_1_opt,
        mojmir_3_opt,
        fink_9_opt        
    };

    const fopc opcount[] = {
        op_fink_1, 
        op_fink_11,
        op_sam_aligned,
        op_mojmir_1,
        op_mojmir_3,
        op_fink_9,
        op_fink_1, 
        op_fink_11,
        op_sam_aligned,
        op_mojmir_1,
        op_mojmir_3,
        op_fink_9        
    };        

    const char *fnames[] = {
        "fink_1", 
        "fink_11", 
        "sam_aligned", 
        "mojmir_1", 
        "mojmir_3", 
        "fink_9",
        "fink_1_opt",
        "fink_11_opt",
        "sam_aligned_opt",
        "mojmir_1_opt",
        "mojmir_3_opt",
        "fink_9_opt"        
    };

    const int n = 12;

    group_test(viterbi_functions, fnames, n);
    group_profile(function, opcount, fnames, n, size, dense_num_sizes, repeats, argv[1]);    
    
    return 0;
}


int fink_1(unsigned n, profile_timer_t * const timer) {
    return profile(viterbi_log_fink_1, n, timer);
}

int fink_1_opt(unsigned n, profile_timer_t * const timer) {
    return profile(viterbi_log_fink_1_opt, n, timer);
}

int fink_11(unsigned n, profile_timer_t * const timer) {
    return profile(viterbi_log_fink_11, n, timer);
}

int fink_11_opt(unsigned n, profile_timer_t * const timer) {
    return profile(viterbi_log_fink_11_opt, n, timer);
}

int sam_aligned(unsigned n, profile_timer_t * const timer) {
    return profile(viterbi_log_sam_aligned, n, timer);
}

int sam_aligned_opt(unsigned n, profile_timer_t * const timer) {
    return profile(viterbi_log_sam_aligned_opt, n, timer);
}

int mojmir_1(unsigned n, profile_timer_t * const timer) {
    return profile(viterbi_log_mojmir_1, n, timer);
}

int mojmir_1_opt(unsigned n, profile_timer_t * const timer) {
    return profile(viterbi_log_mojmir_1_opt, n, timer);
}

int mojmir_3(unsigned n, profile_timer_t * const timer) {
    return profile(viterbi_log_mojmir_3, n, timer);
}

int mojmir_3_opt(unsigned n, profile_timer_t * const timer) {
    return profile(viterbi_log_mojmir_3_opt, n, timer);
}

int fink_9(unsigned n, profile_timer_t * const timer) {
    return profile(viterbi_log_fink_9, n, timer);
}

int fink_9_opt(unsigned n, profile_timer_t * const timer) {
    return profile(viterbi_log_fink_9_opt, n, timer);
}

unsigned long long op_fink_1(const unsigned n, const unsigned k) {
    return 2ull*n*k*k - 2ull*k*k + 6ull*k;
}

unsigned long long op_fink_11(const unsigned n, const unsigned k) {
    return 2ull*n*k*k - 2ull*k*k + 6ull*k;
}

unsigned long long op_sam_aligned(const unsigned n, const unsigned k) {
    return  82ull*n*k - 82ull*k + 3ull*k;
}

unsigned long long op_mojmir_1(const unsigned n, const unsigned k) {
    return 4ull*(n-1ull)*(65ull*8ull + 62ull*8ull) + 2ull*k + 2ull*k*(k-1ull);
}

unsigned long long op_mojmir_3(const unsigned n, const unsigned k) {
    return  4ull*(n-1ull)*816ull + 2ull*k + 2ull*k*(k-1ull);
}

unsigned long long op_fink_9(const unsigned n, const unsigned k) {
    unsigned long long l = (unsigned long long) k / 32ull;    
    return (n-1ull)*l*(64ull + (k-1ull)*72ull ) + k;
}

void cache_scrambler() 
{
    srand(time(NULL));
    float * pointers[10];
    for(int i = 0; i < 10; ++i) 
    {
        pointers[i] = calloc(1000000, sizeof(float));
    }
    for(int j = 0; j < 1000000; ++j) {
        for(int i = 0; i < 10; ++i) 
        {
            pointers[i][j] = (float) rand() / (float) RAND_MAX;
            pointers[i][j] *= pointers[i][j];
        }
    }
    for(int i = 0; i < 10; ++i) {
        free(pointers[i]);
    }
}

int profile(viterbi_function_t fun, unsigned n, profile_timer_t * const timer) 
{
    srand(1245);    
    const int N = 32;   
    hmm_t * hmm = hmm_construct(N, N);
    read_real_from_file("./data/fink_tran_aligned.txt", hmm->transition_probability, N*N);
    read_real_from_file("./data/fink_emis_aligned.txt", hmm->emission_probability, N*N);
    read_real_from_file("./data/fink_init_aligned.txt", hmm->initial_probability, N);   
    hmm_normalize_log(hmm);
    hmm_sequence_t * sequence = hmm_sequence_construct(n);
    for(int i = 0; i < n; ++i) sequence->emissions[i] = rand() % 27; 

    fun(hmm, sequence, timer);

    hmm_sequence_destruct(sequence);
    hmm_destruct(hmm);

    return 0;
}


int group_test(const viterbi_function_t * const function,
               const char *fnames[],
               const int num_funs)
{
    const int N = 32;
    hmm_t * hmm = hmm_construct(N, N);
    const int shorter = 27;
    const int longer = 27;

    hmm_sequence_t * sequence = hmm_sequence_construct(shorter);
    hmm_sequence_t * sequence_longer = hmm_sequence_construct(longer);

   
    unsigned *matlab_estimates = calloc(sizeof(unsigned), shorter);
    unsigned *matlab_estimates_longer = calloc(sizeof(unsigned), longer);

    read_real_from_file("./data/fink_tran_aligned.txt", hmm->transition_probability, N*N);
    read_real_from_file("./data/fink_emis_aligned.txt", hmm->emission_probability, N*N);
    read_real_from_file("./data/fink_init_aligned.txt", hmm->initial_probability, N);   

    read_unsigned_from_file("./data/fink_emissions_aligned.txt", sequence->emissions, shorter);
    read_unsigned_from_file("./data/fink_estimates_aligned.txt", matlab_estimates, shorter);
    
    read_unsigned_from_file("./data/fink_emissions_aligned_longer.txt", sequence_longer->emissions, longer);
    read_unsigned_from_file("./data/fink_estimates_aligned_longer.txt", matlab_estimates_longer, longer);

    hmm_normalize_log(hmm);
    printf("%12s %12s %12s\n", "function", "passed-short", "passed-long");
    for(int k = 0; k < num_funs; ++k) {

        profile_timer_t timer;
        _Bool test1 = 1, test2 = 1;
        memset(sequence->states, 0, shorter*sizeof(unsigned));
        memset(sequence_longer->states, 0, longer*sizeof(unsigned));

        printf("%12s", fnames[k]);

        function[k](hmm, sequence, &timer);
        for(int i = 0; i < shorter; ++i) {
            test1 &= matlab_estimates[i] == sequence->states[i];
        }
        PRINTF_T_F(test1 == 1);

        function[k](hmm, sequence_longer, &timer);        
        for(int i = 0; i < longer; ++i) {
            test2 &= matlab_estimates_longer[i] == sequence_longer->states[i];
        }
        PRINTF_T_F(test2 == 1);
        printf("\n");
    }

    hmm_sequence_destruct(sequence);
    hmm_destruct(hmm);   

    free(matlab_estimates);
    free(matlab_estimates_longer);

    return 0;
}

void group_profile(const fprof * const function,
                   const fopc * const opcount,
                   const char *fnames[],
                   const int num_funs,
                   const int * const sizes, 
                   const int num_sizes, 
                   const int repeats,
                   const char fname[])
{
    const unsigned k = 32;

    FILE *f = fopen(fname, "w");
    if (f == NULL)
    {
        printf("Error opening file! (%s) \n", fname);
        exit(-1);
    }

    double (* runtime)[num_funs] = (double (*)[num_funs]) calloc(num_sizes*num_funs, sizeof(double));
    double (* perf)[num_funs]    = (double (*)[num_funs]) calloc(num_sizes*num_funs, sizeof(double));

    for(int i = 0; i < num_funs; ++i) 
    {   
        printf("%20s\n", fnames[i]);
        for(int j = 0; j < num_sizes; ++j) {
            
            double tsc_cycles = 0.;

            for(int k = 0; k < repeats; ++k) {
                profile_timer_t timer;
                timer_reset(&timer); 
                cache_scrambler();
                function[i](sizes[j], &timer);
                tsc_cycles += (double) timer_tsc_diff(&timer);
            }

            tsc_cycles /= repeats;
            double curr_perf = (double) opcount[i](sizes[j], k) / tsc_cycles;
            runtime[j][i] = tsc_cycles;
            perf[j][i] = curr_perf;
        }
    }
    
    fprintf(f, "N ");
    for(int i = 0; i < num_funs; ++i)
        fprintf(f, "%s_rt %s_perf ", fnames[i], fnames[i]);
    fprintf(f, "\n");

    for(int j = 0; j < num_sizes; ++j) {
        fprintf(f, "%d ", sizes[j]);
        for(int i = 0; i < num_funs; ++i) {
            fprintf(f, "%.2lf %.2lf ", runtime[j][i], perf[j][i]);
        }
        fprintf(f, "\n");
    }

    fclose(f);
    free(runtime);
    free(perf);
}
