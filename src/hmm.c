#include <math.h>
#include <immintrin.h>
#include "hmm.h"
#include "utility.h"

hmm_sequence_t * hmm_sequence_construct(const unsigned n) {
	hmm_sequence_t * hmm_sequence;
	hmm_sequence = calloc(1u, sizeof(hmm_sequence_t));
	hmm_sequence->states    = calloc((n/4096 + 1), 4096*sizeof(unsigned));
	hmm_sequence->emissions = calloc((n/4096 + 1), 4096*sizeof(unsigned));
	hmm_sequence->size = n;
	return hmm_sequence;
}

void hmm_sequence_destruct(hmm_sequence_t * hmm_sequence) {
	free(hmm_sequence->states);
	free(hmm_sequence->emissions);
	free(hmm_sequence);
}

hmm_t * hmm_construct(const unsigned k, const unsigned l) {
	hmm_t * hmm;
	hmm = calloc(1u, sizeof(hmm_t));

    posix_memalign((void **) &hmm->initial_probability,    8*sizeof(real_t), k*sizeof(real_t));
    posix_memalign((void **) &hmm->transition_probability, 8*sizeof(real_t), k*k*sizeof(real_t));
    posix_memalign((void **) &hmm->emission_probability,   8*sizeof(real_t), k*l*sizeof(real_t));
    posix_memalign((void **) &hmm->transition_probability_tr, 8*sizeof(real_t), k*k*sizeof(real_t));
    posix_memalign((void **) &hmm->emission_probability_tr,   8*sizeof(real_t), k*l*sizeof(real_t));

	hmm->num_states = k;
	hmm->num_emissions = l;
	return hmm;
}

void hmm_destruct(hmm_t * hmm) {
	free(hmm->initial_probability);
    free(hmm->transition_probability);
	free(hmm->emission_probability);
    free(hmm->transition_probability_tr);
    free(hmm->emission_probability_tr);
	free(hmm);
}

void hmm_normalize(const hmm_t * unnormalized_hmm) {
    assert(unnormalized_hmm != NULL);
    assert(unnormalized_hmm->num_states > 0);
    assert(unnormalized_hmm->num_emissions > 0);

    const unsigned k = unnormalized_hmm->num_states;
    const unsigned l = unnormalized_hmm->num_emissions;
    /** Normalize initial probability vector */
    {
        real_t sum = 0.;
        for(int i = 0; i < k; ++i) sum += unnormalized_hmm->initial_probability[i];
        if(sum != 0.) for(int i = 0; i < k; ++i) unnormalized_hmm->initial_probability[i] /= sum;
    }

    /** Normalize transition probability matrix */
    for(int i = 0; i < k; ++i) {
        real_t rowsum = 0.;
        real_t *tran_prob_row = unnormalized_hmm->transition_probability + i*k;
        for(int j = 0; j < k; ++j) rowsum += tran_prob_row[j];
        if(rowsum == 0.) continue;
        for(int j = 0; j < k; ++j) tran_prob_row[j] /= rowsum;            
    }

    /** Normalize emission probability matrix */
    for(int i = 0; i < l; ++i) {
        real_t rowsum = 0.;
        real_t *emis_prob_row = unnormalized_hmm->emission_probability + i*k;
        for(int j = 0; j < l; ++j) rowsum += emis_prob_row[j];
        if(rowsum == 0.) continue;          
        for(int j = 0; j < l; ++j) emis_prob_row[j] /= rowsum;            
    }
    
    for(int i = 0; i < k; ++i) {
        for(int j = 0; j < k; ++j) {
            unnormalized_hmm->transition_probability_tr[i*k + j] = unnormalized_hmm->transition_probability[j*k + i];            
        }
    }

    for(int i = 0; i < l; ++i) {
        for(int j = 0; j < k; ++j) {
            unnormalized_hmm->emission_probability_tr[i*k + j] = unnormalized_hmm->emission_probability[j*l + i];            
        }
    }

    
}

void hmm_normalize_log(const hmm_t * unnormalized_hmm) {
    assert(unnormalized_hmm != NULL);
    assert(unnormalized_hmm->num_states > 0);
    assert(unnormalized_hmm->num_emissions > 0);

    const unsigned k = unnormalized_hmm->num_states;
    const unsigned l = unnormalized_hmm->num_emissions;
    /** Normalize initial probability vector 
        Also store in negative log format */
    {
        real_t sum = 0.;
        for(int i = 0; i < k; ++i) {
            sum += unnormalized_hmm->initial_probability[i];
        }
        if(sum != 0.) {
            for(int i = 0; i < k; ++i) {
                if (unnormalized_hmm->initial_probability[i] == 0)
                    unnormalized_hmm->initial_probability[i] = INFINITY;
                else
                    unnormalized_hmm->initial_probability[i] = -log(unnormalized_hmm->initial_probability[i]/sum);
            }
        }
    }

    /** Normalize transition probability matrix 
        Also store in negative log format */
    for(int i = 0; i < k; ++i) {
        real_t rowsum = 0.;
        real_t *tran_prob_row = unnormalized_hmm->transition_probability + i*k;
        for(int j = 0; j < k; ++j) rowsum += tran_prob_row[j];
        if(rowsum == 0.) continue;
        for(int j = 0; j < k; ++j) {
            if (tran_prob_row[j] == 0)
                tran_prob_row[j] = INFINITY;
            else
                tran_prob_row[j] = -log(tran_prob_row[j]/rowsum);
        }          
    }

    /** Normalize emission probability matrix 
        Also store in negative log format */
    for(int i = 0; i < l; ++i) {
        real_t rowsum = 0.;
        real_t *emis_prob_row = unnormalized_hmm->emission_probability + i*k;
        for(int j = 0; j < l; ++j) rowsum += emis_prob_row[j];
        if(rowsum == 0.) continue;        	
        for(int j = 0; j < l; ++j) {
            if(emis_prob_row[j] == 0)
                emis_prob_row[j] = INFINITY;
            else
                emis_prob_row[j] = -log(emis_prob_row[j]/rowsum); 
        }            
    }

    for(int i = 0; i < k; ++i) {
        for(int j = 0; j < k; ++j) {
            unnormalized_hmm->transition_probability_tr[i*k + j] = unnormalized_hmm->transition_probability[j*k + i];            
        }
    }
    
    for(int i = 0; i < l; ++i) {
        for(int j = 0; j < k; ++j) {
            unnormalized_hmm->emission_probability_tr[i*k + j] = unnormalized_hmm->emission_probability[j*l + i];            
        }
    }    
}
