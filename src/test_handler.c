#include "test_handler.h"


test_handler_t * test_handler_construct(const int range, const int name_range) {
	test_handler_t * test_handler = calloc(sizeof(test_handler_t), 1);

	/** Allocate unit test names memory */
	test_handler->functions = calloc(sizeof(int *), range);
	test_handler->test_name = calloc(sizeof(char *), range);
	for(int i = 0; i < range; ++i) {
		test_handler->test_name[i] = calloc(sizeof(char), name_range + 1);
	}

	test_handler->range = range;
	test_handler->name_range = name_range;
	test_handler->size = 0u;

	return test_handler;

}


void test_handler_destruct(test_handler_t * const test_handler) {
	
	/** Clean up unit test names memory */
	for(int i = 0; i < test_handler->size; ++i) {
		free(test_handler->test_name[i]);
	}
	free(test_handler->test_name);
	free(test_handler->functions);
	free(test_handler);
}


void test_handler_add_function(test_handler_t * const test_handler, int (*function)(), const char name[])
{
	assert(test_handler->size < test_handler->range);
	assert(strlen(name) < test_handler->name_range);

	test_handler->functions[test_handler->size] = function;

	strcpy(test_handler->test_name[test_handler->size], name);

	++test_handler->size;
}

void test_handler_run(test_handler_t * const test_handler) {
	for(int i = 0; i < test_handler->size; ++i) {
		printf("* Running test %02d: %s\n", i+1, test_handler->test_name[i]);
		int result = test_handler->functions[i]();
		printf(result ? "* Test %02d failed\n" : "* Test %02d passed\n", i+1);
	}

}
