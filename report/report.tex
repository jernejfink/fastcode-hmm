% IEEE standard conference template; to be used with:
%   spconf.sty  - LaTeX style file, and
%   IEEEbib.bst - IEEE bibliography style file.
% --------------------------------------------------------------------------

\documentclass[letterpaper]{article}
\usepackage{spconf,amsmath,amssymb,graphicx}
\usepackage{algorithm}%
\usepackage{algpseudocode}
\usepackage{listings}
%\usepackage[ttdefault=true]{AnonymousPro}
\usepackage[T1]{fontenc}
\usepackage{color}
\usepackage{xcolor}

%\usepackage{fullpage}

% Example definitions.
% --------------------
% nice symbols for real and complex numbers
\newcommand{\R}[0]{\mathbb{R}}
\newcommand{\C}[0]{\mathbb{C}}
\newcommand{\bA}[0]{\mathbf{A}}
\newcommand{\bE}[0]{\mathbf{E}}


\newcommand\mynote[1]{\textcolor{red}{#1}}
\newcommand\mymark[1]{\textcolor{blue}{#1}}
\definecolor{light-gray}{rgb}{0.94, 0.94, 0.94}
\definecolor{blue}{rgb}{0.12,0.47,0.71}
\definecolor{red}{rgb}{0.84,0.15,0.16}
\algrenewcommand\algorithmicindent{1.0em}%

\renewcommand*\ttdefault{AnonymousPro}
\renewcommand{\algorithmicrequire}{\textbf{Parameters:}}
\renewcommand{\algorithmicensure}{\textbf{Initialization:}}

\lstset{
	language=C,
    basicstyle=\footnotesize\ttfamily,
    frame=top,frame=bottom,
    stepnumber=1,
    numbersep=1pt,
    tabsize=2,
    numbers=left,
    breaklines=true,
    captionpos=t,
    morekeywords={real_t, real, vec, blend, verMin, horMin, set1, cmp},
    backgroundcolor = \color{light-gray}
}



% bold paragraph titles
\newcommand{\mypar}[1]{{\bf #1.}}


% Title.
% ------
\title{Implemntation of optimized Viterbi algorithm for performing spelling correction using Hidden Markov Models}
%
% 
% Single address.
% ---------------
\name{Samuel Charreyron, Jernej Fink, Ivaylo Toskov, Mojmir Mutny} 
\address{ETH Z\"urich\\Z\"urich, Switzerland}

% For example:
% ------------
%\address{School\\
%		 Department\\
%		 Address}
%
% Two addresses (uncomment and modify for two-address case).
% ----------------------------------------------------------
%\twoauthors
%  {A. Author-one, B. Author-two\sthanks{Thanks to XYZ agency for funding.}}
%		 {School A-B\\
%		 Department A-B\\
%		 Address A-B}
%  {C. Author-three, D. Author-four\sthanks{The fourth author performed the work
%		 while at ...}}
%		 {School C-D\\
%		 Department C-D\\
%		 Address C-D}
%

\begin{document}
%\ninept
%
\maketitle
%

\begin{abstract}
We implement a highly optimized Viterbi algorithm for hidden Markov model used in spelling correction. We focus on improving the performance and runtime of the algorithm. Using techniques such as unrolling, code generation and AVX instructions, we were able to improve the base implementation to make it 14 times faster and reach more than $62\%$ of the peak performance. Our implementation outperforms the well-known GHMM library by approximately the factor of 20 in terms of runtime. 
\end{abstract}

\section{Introduction}\label{sec:intro}
Human-typed text, whether it be performed on a computer or a smartphone is prone to typographical errors produced by the user hitting an incorrect key. In order to process or assist the user, automatic spelling correction are commonly available as software packages. Automatic spelling correction has been performed as early as 1964 by comparing strings with an existing dictionary using the Damerau-Levenshtein distance \cite{Damerau:1964}. More recently, developments using statistical methods \cite{Kernighan1990} have been introduced which do not require a dictionary of the words in a language. 

Methods without a dictionary build a statistical model for human-typed text and the typographical errors that may occur. Therefore, the model can be adjusted to a particular user. 

In the following work, we implement highly optimized statistical dictionary-free automatic spelling corrector using hidden Markov models which are often part of a pipeline of modern spelling correction software \cite{Li:2012}. We put emphasis on the performance and runtime execution of the implementation. We disregard the accuracy of the actual spelling correction model.

\mypar{Our Model} 
We implemented one stage transition HMM, where states are represented by letters, and emissions represent the corrupted user-typed information. In theory, a syllable or any $p$-gram could be used as a single state of our algorithm, however, this increases the state space exponentially. All our experiments were performed assuming one letter transitions, thus the number of states was $26$ with additional state space for white space. 

Our implementation allows for per word, sentence or even any $n$-gram correction, but this has to be specified before training of the HMM. In order to find the optimal correction for the letter sequence, we use Viterbi algorithm, which can be characterized as highly optimized breadth-first search within a specific graph structure.

\mypar{Related work} To our knowledge, there are no publicly available and/or standalone spelling correctors software relying solely on HMM. Usually, the models combine aspects of dictionary correction, and of a probabilistic model such as HMM, e.g. in \cite{Hladek2013} and \cite{Li:2012}. A more complicated software implementing another line of probabilitic models similar to our can be found in \cite{Duan2011}, \cite{Kernighan1990} and \cite{Brill2000}.

Our project focused only on the HMM part of the algorithms, thus, for our reference, we relied on two implementations of the Viterbi algorithm, which work for any discrete HMM. As a main baseline for comparison we used general purpose GHMM library \cite{Schliep2004}, and for the test of the algorithm the MATLAB implementation was used. 

\section{Background}\label{sec:background}
\mypar{Hidden Markov Model} A hidden Markov model consists of three parts:

\begin{itemize}
    \item a Markov chain with a finite number of unobserved discrete variables or states $x_k \in \mathcal{S}$, $m \stackrel{\text{def}} =  \mathcal{|S|}$ and transition probabilities between a previous state and next state $p(x_k | x_{k-1})$
    \item a discrete distribution over the initial states $p(x_0)$
    \item a measurement model over discrete observed variables or emissions $y_k \in \mathcal{Y}$,$|\mathcal{Y}|=m$  and the conditional probabilities of such emissions given the state space $p(y_k | x_k)$
\end{itemize}

The objective of HMM is to reconstruct the unobserved sequence $(x_0,\dots,x_N)$ from the observed emissions $(y_0,\dots,y_N)$. Due to probabilistic nature of the model this corresponds to maximization of the joint probability of the unobserved states and the observed emissions $$p_T \stackrel{\text{def}} = p(x_0, x_1, \dots, x_N, y_1, y_2, \dots, y_N).$$

Due to Markovian property, $x_k$ depends only on $x_{k-1}$ and $z_k$, so the joint probability can be decomposed as 
\begin{align}
    p_T = p(x_0) \prod_{k = 1}^{N} p(x_k| x_{k-1}) p(y_k|x_k).
    \label{eq:joint_pdf}
\end{align}
The optimal sequence for combination of hidden states $\pi \stackrel{\text{def}} = (x_0, x_1, \cdots, x_N)$ maximizes \eqref{eq:joint_pdf} as
\begin{align}
    \pi^* = \arg\max_{\pi} \left( p(x_0) \prod_{k = 1}^{N} p(x_k| x_{k-1}) p(y_k|x_k) \right).
    \label{eq:max_joint_pdf}
\end{align}
By taking the negative logarithm of all the probabilities, \eqref{eq:max_joint_pdf} can be rewritten in the more convenient form,
\begin{equation*}
\pi^* = \arg\min_{\pi} \left ( P(x_0) + \sum_{k=1}^N A(x_k|x_{k-1}) + E(y_k | x_k) \right ),
\end{equation*}
where $P(x_0) = -\log(p(x_0))$, $A(x_i|x_{i-1}) = -\log(p(x_i|x_{i-1})$ and $ E(y_i | x_i) = -\log(p(y_i | x_i))$.

This type of a problem can be represented graphically using a trellis graph, which portrays the transitions between states in the state space over the sequence as edges of a directed acyclic graph. An example is shown in Fig.  \ref{fig:trellis}. As edges correspond to the probabilities, the problem can be recognized as a classic shortest path problem from $x_0$ to $x_N$ states. Specifically, the edge weights correspond to the sum of the transition and emission probabilities $e(x_k, x_{k-1}) = A(x_k|x_{k-1}) + E(y_k | x_k)$. A shortest path algorithm for the specific structure of a graph is called Viterbi algorithm \cite{Viterbi:67}.

%% This needs to be there
\begin{figure}
  \centering
    \includegraphics[scale = 0.33]{trellis.png}
    
  \caption{An example of a trellis diagram for a sequence of a length 4. The emissions are shown in purple, and the states in orange. Edges between states are given the value $A(x_k|x_{k-1}) + E(y_k | x_k)$. The optimal path between the start and end states is shown in red.}
  \label{fig:trellis}
\end{figure}

\begin{algorithm}
\caption{Backwards-forwards Viterbi algorithm}
\begin{algorithmic}[1]
    \Require transition matrix $\mathbf{A}$; Emission matrix $\mathbf{E}$; initial probability $P(x_0)$; Number of states $N$; State space $\mathcal{S} = \{1,\dots,m\}$
	\Ensure Pre-compute logarithms of $\bA$ and $\bE$
    \Procedure{Initialization}{}
    \For{$i \in \mathcal{S} $}
         \State $J_{N-1} \gets \bE\left(y_{N-1}, x_i\right)$
    \EndFor
    \EndProcedure
        
    \Procedure{Backwards Iteration}{}
    \State $k \gets N-2$
    \While{$k > 0$}
    \For{$i \in \mathcal{S} $}
         \State $J_k(i) \gets {\bE(y_k, i) + \min_{j \in \mathcal{S}} \left(\bA(i,j) + J_{k+1}(j) \right)}$
         \State $\pi_k(i) \gets \arg\min_{j \in \mathcal{S}} \left(\bA(i,j) + J_{k+1}(j) \right)$
    \EndFor
    \State $k \gets k-1$
    \EndWhile
        
    \For{$i \in \mathcal{S} $}
         \State $J_0(i) \gets {P(i)+\bE(y_0, i)+\min_{j \in \mathcal{S}} \left(\bA(i,j) + J_{1}(j) \right)}$
         \State $\pi_0(i) \gets \arg\min_{j \in \mathcal{S}} \left(\bA(i,j) + J_{1}(j) \right)$
    \EndFor
    \EndProcedure
	
    \Procedure{Forward Pass}{}
    \State $i\gets \arg\min_{j \in \mathcal{S}} J_0(j)$
    \State $\pi \gets \{i\}$
    \State $ k \gets 0$
    \While{$k < N$}
    \State $i \gets \pi_k(i)$
    \State $\pi = \{\pi, \pi_k(i)\}$
    \State $k \gets k+1$
    \EndWhile
    \EndProcedure
\end{algorithmic}
\end{algorithm}

\mypar{Viterbi algorithm}
The \emph{principle of optimality} states that the shortest path from the current state to the end state is equal to the sum of shortest path from the current state to the next state and the shortest path from the next state to the end. 

From this principle, the shortest path can be computed by working backwards from the end of the sequence to the start. Once the optimal paths for each starting state $x_0$ have been computed, the shortest path from the initial to the end state can be computed by moving forward through the computed paths. 

The minimum cost for the state $x_k$ is denoted as $J_k(x_k)$, whose interpretation is the negative log of the probability that the sequence contain $x_k$ and the following states $x_l$ with $l>k$. The Algorithm 1 specifies the execution in great detail.


\mypar{AVX instructions}
Advanced Vector Extensions (AVX) are type of SIMD instructions introduced by Intel in 2008 \cite{Firasta2008}. They can operate on 256 bit registers and can perform basic numerical or shift operation on 8 floating point numbers in one instruction.

\mypar{Cost Analysis}
As we are interested in an optimized implementation that has low runtime and good utilization of computational power, we define the metric \emph{performance} which simply corresponds to the number of floating point add operations performed in one clock-cycle of a processor. This metric, in addition to runtime, serves as an indicator whether we utilize the peak performance of our processor.

Our algorithm does not use any multiplication or more complicated instructions as an extensive precomputation of logarithms is done before the algorithm start. Thus, we consider the peak performance of the processor to be the maximum number of add operations performed in one cycle. We distinguish the \emph{vector} and the \emph{scalar} peak, where the former assumes SIMD instructions presence, and the latter does not. Another useful measure when considering SIMD instructions is \emph{vectrorized efficiency}, which is calculated as a ratio of FLOP vector instructions over all vector instructions. To complete the analysis, our algorithm belongs to asymptotic complexity class of $O(Nm^2)$ but this is not subject to change in this work. 
\section{Implementation details and methodology}
\mypar{Data containers}
We encapsulated the HMM and sequence data in two user defined structures $\texttt{hmm\_t}$ and\\ $\texttt{hmm\_sequence\_t}$.
%\begin{lstlisting}[caption=User defined structs for HMMs and sequences]
%struct hmm_t {
%    real *initial_probability; 
%    real *transition_probability; 
%    real *emission_probability;
%    real *transition_probability_tr;
%    real *emission_probability_tr;
%    unsigned num_states, num_emissions;
%}
%struct hmm_sequence_t {
%    unsigned *states, *emissions;
%    unsigned size;
%}
%\end{lstlisting}
Since the state space is discrete, we enumerate $\mathcal{S}$ and create a one-to-one mapping such that $\{ \texttt{'a'} \rightarrow 0, \ldots, \texttt{'z'} \rightarrow 25, \texttt{' '} \rightarrow 26 \}$. This ordering is very natural, and can be easily obtained by simply subtracting the ASCII value of 'a' from the every letter. The space character has to be handled separately. This state enumeration is then used for accessing the transition and emission probabilities, which are represented in the code as $\texttt{p}$ and $\texttt{e}$, e.g. $\texttt{p[0][3]} = p(x_{k+1} = \texttt{'d'} \ | \ x_{k} = \texttt{'a'})$. We also store initial probability as  $\texttt{initial\_probability[i]} = p(x_0 = i)$. For \texttt{hmm\_sequence\_t}, $\texttt{states[k]} = x_k$ is the state estimate as determined by the Viterbi algorithm and \\ $\texttt{emissions[k]} = y_k$ is provided by user as HMM emission. Viterbi algorithm needs to track cost function $J_k(x_k)$ and the policy $\pi_k(x_k)$ as well. These we store as follows:
\begin{itemize}
\item \texttt{policies[N][m]} where \texttt{N} is sequence length and \texttt{m} number of states. 
\item \texttt{costs[2][m]}
\end{itemize}
As we are interested only in policies (path taken), we need to keep track of successive cost ($J_k$ only depends on $J_{k+1}$) in a given instant only. Hence, in contrast to the pseudocode we simply access the cost matrix like \texttt{costs[k \% 2][.]}.
\section{Base implementation and scalar improvements}
\mypar{Base implementation}
The \emph{base} implementation is a straightforward translation of pseudocode to a C code, the only change being the reduced size of the \texttt{costs}. In the code below we show the backwards iteration which contains the most important part of the work.
\begin{lstlisting}[caption=Base implementation of backwards iteration]
for(int k = N-2; k >= 0; --k) {
	for(int i = 0; i < m; ++i) {
		real min_cost = costs[(k+1)%2][0] + p[i][0];
		int min_j = 0;
		for(int j = 1; j < m; ++j) {
			real old_cost = cost[(k+1)%2][j];
			real new_cost = old_cost + p[i][j];
			if(new_cost < min_cost) {
			    min_cost = new_cost;
			    min_j = j;
			}
		}
	costs[k%2][i] = min_cost  + e[i][y[k]] + (k == 0 ? p0[i] : 0.);
	policies[k][i] = min_j;
	}
}
\end{lstlisting}

Before elaborating the details of improvements, we have to justify, why we only focus on the backwards iteration. A close inspection of the pseudo-code reveals that the backwards iteration is the most computationally intensive part; indeed for every element of our sequence, we perform $2m^2$ operation with $m$ being the size of our state space. Therefore, due to this part of the algorithm the algorithm belongs to $O(Nm^2)$. In the initialization step, we only copy the memory contents from one location to the other, whereas the forwards iteration step is an unpredictable random access across the memory without computation. The backwards iteration is thus the part of algorithm on which we focused.

Despite it being a computationally intensive part, it is also remarkably simple. Aggressive compiler optimizations took advantage of it. For the base implementation we had to diligently specify additional \emph{local compiler pragmas} such that the compiler did not perform any loop unrolling. Only then, we could reason what caused the speed-up. Although the initial implementation was straightforward, it was not written poorly on purpose. Our intention was to have a code that corresponds to the algorithm description, but would have reasonable performance from the beginning. Consequently, some basic optimizations like code motion, strength reduction, and removing suboptimal procedure calls were exhausted already during the initial coding phase.

\mypar{Scalar improvements}
The two main optimizations that we could implement here were blocking for registers and exploitation of instruction level parallelism (ILP) to avoid latency slowdowns. To implement these ideas, we performed loop unrolling. It pays off to unroll both the inner-most and the middle loop. By unrolling the middle loop, we can reuse the scalar variable \texttt{old\_cost} (scalar replacement). By unrolling the inner loop, we can use multiple accumulators, which in this case contain multiple minimum candidates, among which we at end of the $j$-th iteration find the minimum; this way we ensure efficient order of execution.

To achieve the best results, we relied on generating the code automatically, using Python and the templating engine Jinja. Despite its primary usage in Web applications, Jinja can be reconfigured for various purposes, including code generation. Our search space of unrolling parameters was \emph{\{i+=1, 2, 4, 8\}} $\times$ \emph{\{j+=1, 2, 4, 8\}}. Contrary to our expectations, it turned out that the best performing codelet was with parameters \emph{\{i+=2, j+=2\}}. We later reasoned that with larger $j$, the overhead of finding the minimum among multiple accumulators outweighs the time saved by reducing latencies due to the use of the multiple accumulators. Further, with additional unrolling for $i$, we possibly run out of registers and have to start moving data on the stack, causing memory latencies.

\section{AVX instruction improvements}
In order to further improve the code, we use AVX vector instructions reviewed in Section \ref{sec:background} with single precision floating points (8 floats in a vector). We generated three generations of code and incrementally improve the implementation. Here we outline the path we took in optimizing the code. 

As AVX instructions operate on 8 floats at once all instructions are executed in groups of 8. Given parameters $m$ and $N$ it might be impossible to group all instructions. In such cases, when the parameter $m$ is not divisible by 8, we add an extra padding to probability transition and emission matrices to make their dimensions divisible by 8. For example, for the case of spelling correction $m = 27$, but we padded the matrices to have $m = 32$ while not changing the result.  

\mypar{First generation: Naive vectorization}
Our first approach involved vectorizing the minimization in the innermost loop. The process consisted of stacking edge costs into vectors of length 8 and finding pairwise minimums using the \texttt{\_mm256\_cmp\_ps} instruction by extracting the mask. Since the argument of the minimum was also required, an identical number of vectors containing indices were processed identically to the edge values. Two vectors were then blended together using \texttt{\_mm256\_blendv\_ps} to reduce the number of vectors by two. We call this operation, which works on two sets of vectors a \emph{vertical min}. This was repeated until a single vector of values and a single vector of indices remained. Then the minimum of the values contained in the single vector was found in what we call a \emph{horizontal min}, consisting of three permutations and compares. Finally, the minimum cost and index were written back to the cost and policies matrices respectively. We demonstrate the algorithm idea with $m = 32$ in the C pseudo code with nonessential parts skipped.

\iffalse
	\begin{lstlisting}[caption=AVX naive vectorization with horizontal min]
	for(int k = N-2; k >= 0; --k) {
		for(int i = 0; i < m; i+=8) {
			vec j_a = [0:7];
			vec cost_a = 8*[cost_mat[(k+1)%2][0:7]] + P[i][j_a];
			vec j_b = [8:15];
			vec cost_b = 8*[cost_mat[(k+1)%2][8:15]] + P[i][j_b];
			vec j_c = [16:23];
			vec cost_c = 8*[cost_mat[(k+1)%2][16:23]] + P[i][j_c];
			vec j_d = [24:31];
			vec cost_d = 8*[cost_mat[(k+1)%2][24:31]] + P[i][j_d];
			
			vec min_cost_ab, vec min_j_ab = vertMin(cost_a, cost_b, j_a, j_b);
			vec min_cost_cd, vec min_j_cd = vertMin(cost_a, cost_b, j_a, j_b);
			vec min_cost_abcd, vec min_j_abcd = verMin(cost_ab, cost_cd, j_ab, j_cd);
		 float min_cost, int min_j = horMin(min_cost_abcd, min_j_abcd);
			
			cost_mat[k%2][i] = min_cost + E[y[k]][i] + (k==0 ? P0[i] : 0);
			policy_mat[k][i] = min_j;
		}
	}
	\end{lstlisting}
\fi

\begin{lstlisting}[caption=AVX naive vectorization with horizontal min]
for(int k = N-2; k >= 0; --k) {
	for(int i = 0; i < 32; i+=1) {
		// completely unrolled inner loop
		j_a = 0..7, j_b = 8..15 , ..., j_d = 24..31
		vec cost_a,cost_b,cost_c,cost_d;
		// loading data 
		...
		vec mask = cmp(cost_a,cost_b);
		vec min_ab = blend(cost_a,cost_b,mask);
		vec min_j_ab = blend(j_a,j_b,mask);
		... // similarly other pairwise
		(min_cost, int min_j) = horizontal_min(min_cost_abcd, min_j_abcd);
		// save the new policy and cost
		...
	}
}
\end{lstlisting}

\mypar{Second generation: Transposition}
The second optimization unrolled also the middle loop in contrast to previous naive implementation to seek simultaneously a minimum of 8 states. This left us with 8 vectors where horizontal minimum had to be performed. We chose to transpose the vectors instead, and perform vertical minimums. The indices are backtracked at the end when the minimum for all 8 next states is found. Using \texttt{\_mm256\_set1\_ps} instruction, minimum for each state is placed inside its own vector. Subsequently, it is compared to all initials cost vectors, and similarly as in the previous case masks are created. These masks are blended with vectors containing indices and added. This leaves us with an AVX vector with only one non-zero element. Extracting this number would be difficult so we employ a similar strategy as for finding the minimum. Eight vectors, each containing one non-zero element equal to index for respective next states, are transposed and added in vertical fashion. This leaves us with the vector of 8 policies for the 8 states we unrolled the middle loop on. 

Unfortunately, the backtracking of the index introduces extra floating point operations that are not present in the original base case implementation. The approximate number of floating point operations has doubled in contrast to the previous implementations.

\mypar{Third optimization: Memory layout transposition}
In previous generations the log-probability $p(x_{k+1} = j | x_k = i)$ was accessed as \texttt{p[i][j]}. However, the issue that we faced when vectorizing for \texttt{j} was that in the end, we are faced with two vectors of $8$ elements, one vector of j-indices, which are candidate states $x_{n+1}$ from our state $x_n = i$, and a vector of costs to those candidate states. Among them, we have to find a minimum, which we called the horizontal minimum. This is a costly operation that we want to avoid. In contrast to previous implementation, we avoid this by transposing the transition probability matrix before the algorithm run (the emission probability matrix has already been transposed in the previous optimization). In the subsequent code \texttt{p[j][i]} now refers to $(x_{k+1} = j | x_k = i)$
\iffalse
\begin{lstlisting}[caption=AVX vectorization using transposed memory layout]
for(int k = n-2; k >= 0; --k) {
	for(int i = 0; i < m; i+=8) {
		vec min_cost = set1([costs[(k+1)%2][0]] + p[0][i:i+8]);
		vec min_j = 8*[0];
		for(vec j=set1(1); j < set1(m); j+=8*[1]) {
			vec old_cost = set1([costs[(k+1)%2][j]]);
			vec new_cost = old_cost + P[j][i:i+8];
			vec mask = cmp(new_cost,min_cost)
			min_cost = blend(min_cost, new_cost, mask);
			min_j    = blend(min_j, j, mask);
		}
		costs[k%2][i:i+8] = min_cost + E[y[k]][i:i+8] + (k==0 ? P0[i:i+8] : 8*[0]);
		policies[k][i:i+8] = min_j;
	}
}
\end{lstlisting}
\fi

\begin{lstlisting}[caption=AVX vectorization using transposed memory layout]
for(int k = n-2; k >= 0; --k) {
	for(int i = 0; i < m; i+=8) {
		vec min_cost = set1([costs[(k+1)%2][0]] + p[0][i:i+8]);
		vec min_j = set1(0);
		for(vec j=set1(1);j<set1(m);j+=set1(1)){
			vec old_cost = set1([costs[(k+1)%2][j]])
			vec new_cost = old_cost + P[j][i:i+8];
			vec mask = cmp(new_cost,min_cost);
			min_cost = blend(min_cost, new_cost, mask);
			min_j = blend(min_j, j, mask);
		}
		// save the new policy and cost
		...
	}
}
\end{lstlisting}

As further optimization we unrolled for \texttt{i} by multiple factors of 8. This way we benefit from scalar replacement, as \texttt{old\_cost} can be reused for multiple vectors \texttt{min\_cost} in consequent iterations. Since \texttt{j} is now also a floating point vector, incrementing it is a necessary and unavoidable overhead that comes with vectorization when relying only on AVX. However, to our benefit, the number of flops changes only marginally. Unrolling for \texttt{i} has the benefit that the same loop counter vector can be used for multiple \texttt{i}. In scalar case, this is not a big advantage, since the loop counter lies in dedicated register. In this case, however, it is beneficial. 

We tested multiple unrolling combinations (also unrolling for j), and the experimental results show that the best unrolling is: \emph{i += 32, j += 1}. In other words, unrolling for \texttt{j} does not bring any benefit, whereas unrolling for \texttt{i} seems to confirm our reasoning.

%Finally, we note that this vectorization is optimal in the sense of operation count, given our constraint of AVX-1. Since keeping track of the index, where the minimum occurs is the crux of our algorithm, the overhead introduced by placing these indices in a floating point vector is unavoidable and will have to be present in any vectorization. In this version however we perform the absolute minimum of such index manipulations and thus predict that the resulting runtime will also be the shortest among all vectorizations.

\section{Experimental Results \& Discussion}

\begin{figure}
    \centering
	\includegraphics[scale=0.27]{scalar-perf}
	%\includegraphics[scale=0.33]{g}
	\caption{Performance plot of the \emph{base} and unrolled scalar implementations.}\label{scalarperf}
\end{figure}

%\begin{figure}
%  \includegraphics[scale=0.33]{scalar-rt}
%  \caption{Speedup of the unrolled implementation with respect to base implementation}\label{scalarrt}
%\end{figure}

\begin{figure}
    \centering
    \includegraphics[scale=0.27]{vector-perf}
	%\includegraphics[scale=0.42]{graph2.eps}
	\caption{Performance of the three generations of vectorized AVX implementations.}\label{vectorperf}
\end{figure}

\begin{figure}
    \centering
	\includegraphics[scale=0.27]{vector-rt}
	%\includegraphics[scale=0.42]{graph3.eps}
	\caption{Speedup of all implementation with respect to the \emph{base} implementation. The numbers correspond to the order of presentation of implementations. The \#0 corresponds to unrolling scalar implementation and further implementations correspond to generations of the vector code.}
	\label{rt}
\end{figure}

To run the tests and profiles, we compiled our code with \emph{icc 16.0.3} compiler with optimization flag \emph{-O3}, and ran it on Macbook Pro with Intel Core i7 2.0 GHz Sandy Bridge, which has peak performance of $1$ add/cycle in the \emph{scalar} regime and $8$ add/cycle in the \emph{vector} regime. Our timing architecture used the TSC counter for cycle measurements, and we tried to measure only the core algorithm execution, skipping memory allocation and function call overheads. We counted the number of FLOPS and total cycle count to deduce the \emph{performance}. We simulated our code in the spelling correction scenario where $m = 32$ with varying sizes of $N$. 

The performance results can be reviewed in Fig. \ref{scalarperf} for scalar case and in Fig. \ref{vectorperf} for vector case. We see that unrolling and scalar replacement in scalar case improved the code by approximately $10\%$ of the peak performance. The improvement was observed only when specific \emph{local compiler pragma} were used in order to avoid unrolling by compiler. 

In the vector case, the results of the first generation code show increased performance over the scalar baseline as expected, however, it it is still far from the vector peak performance, most likely, due to inefficient horizontal minimum. The second generation improved on the performance of the code, however the FLOP count is differs from other versions (cca. twice as much), and thus the results are not directly comparable. However, despite the increased FLOP count we observe a speedup as in Fig. \ref{rt}. The third generation of our code shares the same FLOP count as our \emph{base} implementation and achieves very similar performance as our second generation, but as predicted it significantly decreases the runtime, and is nearly $14$ times faster than the \emph{base} implementation. 

We note that we have achieved only approximately $62\%$ of the peak performance even with our latest generation of the vector code most likely due to auxiliary AVX instructions such as shuffles and set instructions. When we calculated the \emph{vectorized efficiency}, it was slightly above 4 for the last generation of our code. Thus getting approximately $50\%$ of the peak performance using 8-way vectors is an expected behavior. 

%We believe that this is an artefact of the timing architecture we used. Our optimization effort focused only on backward iteration of Viterbi algorithm but our timing architecture timed the whole algorithm. Other parts of the algorithm, especially the forwards pass, take a significant time with no floating point computation. Hence, within this timing setup, we would not be able to hit the peak performance per se. \mymark{We hypothesize that our backward iteration reaches the peak performance but the forward pass takes significant time to finish and skews the results.}

We compared our implementation to GHMM \cite{Schliep2004} library which implements Viterbi algorithm for general hidden Markov models. We run their implementation on the same input, and timed the corresponding parts of the computation, and came to conclusion that our implementation is approximately 20 times faster than the GHMM implementation. 

\section{Conclusions}
An highly optimized and efficient implementation of Viterbi algorithm for dictionary-free spelling correction using hidden Markov model was presented. Our optimization effort focused on backward iteration of Viterbi algorithm. Our implementation has a significant precomputation part to allow for efficient running once the model is trained. Our first approach was to optimize the scalar code for ILP using unrolling and scalar replacement with code generation improving $10\%$ of peak performance. Further approaches utilized AVX instructions, and through iteration of 3 code generations, we finished with 14 times faster implementation than our basis implementation, and reached more than $62.5\%$ of peak performance. A comparison with an existing well known implementation of Viterbi algorithm GHMM showed that our implementation is approximately 20 times faster. 

%\begin{figure}\centering
%  \includegraphics[scale=0.33]{dft-performance}
%  \caption{Performance of four single precision implementations of the
%  discrete Fourier transform. The operations count is roughly the
%  same. {\em The labels in this plot are too small.}\label{fftperf}}
%\end{figure}



% References should be produced using the bibtex program from suitable
% BiBTeX files (here: bibl_conf). The IEEEbib.bst bibliography
% style file from IEEE produces unsorted bibliography list.
% -------------------------------------------------------------------------
\bibliographystyle{IEEEbib}
\bibliography{bibl_conf}

\end{document}

