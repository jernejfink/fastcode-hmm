# Viterbi algorithm for spelling correction

## Project structure

The project has the following folders:

  + src
  + include
  + other

Upon compilation, the following folders will have been created:
  
  + bin
  + bin/tests


## First run

To compile the project run
```shell
make all
```
## Debugging
To compile with debug flags run
```shell
make DEBUG=true
```

## Running reference implementation (GHMM)
```shell
make GHMM=true
```
Without this option, all the functions using GHMM will default to void.
For this, you need to have GHMM installed, headers in /usr/local/include/ghmm, libraries in /usr/local/lib
For more info refer to ghmm.org. OS X users, run configure with the flag --with-rng=bsd.

### Testing

To run the unit test, run
```shell
make test_UNIT
```
where UNIT is the name of your test as defined by the test filename in src/tests/test_UNIT.c
An appropriate target has to be added in the Makefile (copy the existing ones if necessary)

### Profiling

To run the profile for a function, run
```shell
make profile_UNIT
```
where a profiling function is defined in src/profiles/profile_UNIT.c
Generally a new target DOES NOT need to be created (as opposed to the unit tests). It would have to be created in the special case where one would profile functions defined outside of viterbi.c
After a profile run, the result will be saved to profiles/profile_UNIT_HOSTNAME.txt. The result will be the (roughly) the same as in stdout.

All profile functions passed to the profile_handler_t (that is, every profile function in its own file profile_stuff.c) need to have a prototype
```c
int profile_function(unsigned n, profile_timer_t * const timer)
```
where n is the data size and timer is the pointer to the timer that will be used somewhere inside.

In that function you can do whatever you want, but I recommend to do as little as possible, best thing is to just called a wrapped function that takes another function as a parameter that you want to profile further, i.e. This way you can reuse common wrappers for different optimization version of a function.
```c
int profile_function(unsigned n, profile_timer_t * const timer) {
    return profile_single_sequence_wrapper(viterbi_backward_forward_search, n, timer);
}
```
In the wrapper you then do things like training, memory allocation, etc. Two wrappers are ALREADY provided in the file wrappers.c, and they include two common cases: random HMM matrices from MATLAB (long_sequence) and matrices that we get from training (single_sequence).

The viterbi functions now take the timer as a parameter directly:
```c
void viterbi_backward_forward_search(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer)
```
and the wrappers to not modify the timer directly but merely passed onto the viterbi function as a parameter. It should then be used inside the viterbi function, in the section that you want to profile!

In addition, this does not have to be one timer but multiple of them, so you can use things like
```c
timer_tic(timer + 1);
// stuff
timer_toc(timer + 1);

timer_tic(timer + 2);
// more stuff
timer_toc(timer + 2);
```
but the profile_handler is not yet adapted to make use of multiple timers, so you would have to access this data differently.


### Plotting
Upon profiling, one can run 
```shell
make plots
```
and plots will be created for all profile data currently in the profiles folder.

## Documentation

All function declarations should be equipped with Javadoc documentation
which includes preconditions, postconditions and a brief description. For example, like this
```c
/**
 *  Node "copy constructor"
 *	
 *	@param i should be smaller than N
 *	@param best_dist should be greater than 0.f
 *	@return node_t with members set as function parameters
 */
node_t node(const index_t i, const dist_t best_dist);
```

## Code purism

  + All global variables should have prefix __ 
  + If you use global variables for more than one setting / parameter, shared across
	multiple functions in the unit, lump them together in a (const) struct, so 
	the namespace is not polluted with generic names (i.e. N).
  + All custom types have to end with _t
  + Make sure your declarations are const correct
  + Maintain a balance between brevity and descriptiveness for names
  + Use inline functions whereever possible
  + Use assertions at the beginning of the function calls to check for precondition correctness.
  + Clean up after yourself (malloc / free)