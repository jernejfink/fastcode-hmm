#ifndef WRAPPERS_H
#define WRAPPERS_H

#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "utility.h"
#include "train.h"
#include "profile_handler.h"
#include "viterbi.h"
#include "hmm.h"
#include "time_util.h"

typedef void (* viterbi_function_t)(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);
int profile_long_sequence_wrapper(viterbi_function_t fun, unsigned n, profile_timer_t * const timer);
int profile_long_sequence_wrapper_log(viterbi_function_t fun, unsigned n, profile_timer_t * const timer);

int profile_single_sequence_wrapper(viterbi_function_t fun, unsigned n, profile_timer_t * const timer);
int profile_single_sequence_wrapper_log(viterbi_function_t fun, unsigned n, profile_timer_t * const timer);

int profile_dense_sequence_wrapper(viterbi_function_t fun, unsigned n, profile_timer_t * const timer);
int profile_dense_sequence_wrapper_log(viterbi_function_t fun, unsigned n, profile_timer_t * const timer);
int profile_dense_aligned_sequence_wrapper_log(viterbi_function_t fun, unsigned n, profile_timer_t * const timer); 

int profile_huge_state_wrapper_log(viterbi_function_t fun, unsigned n, profile_timer_t * const timer);

#endif