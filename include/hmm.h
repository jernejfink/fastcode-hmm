#ifndef HMM_H
#define HMM_H

#include <stdlib.h>
#include <assert.h>

typedef float real_t;

typedef struct hmm_t {
	real_t * initial_probability;

	/* transition_probability[i][j] = P(x_next = j | x_curr = i). \
	 * Stored in neg log format (see @hmm_normalize). */
	real_t * transition_probability;

	/* emission_probability[i][j] = P(z = j | x_curr = i).
	 * Stored in neg log format (see @hmm_normalize) */
	real_t * emission_probability;
	real_t * transition_probability_tr;
	real_t * emission_probability_tr;	
	unsigned num_states;
	unsigned num_emissions;
} hmm_t;

typedef struct hmm_sequence_t {
	unsigned * states;
	unsigned * emissions;
	unsigned size;
} hmm_sequence_t;


hmm_t * hmm_construct(const unsigned k, const unsigned l);


void hmm_destruct(hmm_t * hmm);

/**
 * Allocates memory for the hmm_sequence_t and its states and observations arrays.
 *
 * @param n is the number of states and observations that we want to allocate for.
 * @return the pointer to the allocated hmm_sequence_t struct
 *
 * Preconditions:
 * - n > 0
 * Postconditions:
 * - Allocated 2*sizeof(index_t)*n memory
 */
hmm_sequence_t * hmm_sequence_construct(const unsigned n);

/**
 * Frees the memory allocated for the hmm_sequence_t by the constructor function.
 *
 * @param hmm_sequence is the pointer to the allocated hmm_sequence_t struct
 * 
 * Preconditions:
 * - hmm_sequence should be set with hmm_sequence_construct.
 * Postconditions:
 * - Memory allocated by hmm_sequence_construct is freed.
 */
void hmm_sequence_destruct(hmm_sequence_t * hmm_sequence);

/**
 * Takes an unnormalized hmm (from training) and normalizes, s.t.
 * the sum of the initial probabilities is 1.
 * the sum of each row of transition probability matrix is 1.
 * the sum of each row of emission probability matrix is 1.
 *
 * @param hmm_t is a proper hmm with unnormalized values
 *
 */
void hmm_normalize(const hmm_t * unnormalized_hmm);

/**
 * Takes an unnormalized hmm (from training), normalizes such that
 * the sum of the initial probabilities is 1.
 * the sum of each row of transition probability matrix is 1.
 * the sum of each row of emission probability matrix is 1.
 *
 * Subsequently, the probabilities are stored in the negative log
 * format:
 * -1: if P = 0
 * -log(P) if   0 < P <= 1
 * 
 * @param hmm_t is a proper hmm with unnormalized values
 *
 */
void hmm_normalize_log(const hmm_t * unnormalized_hmm);

#endif // HMM_H
