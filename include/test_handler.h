#ifndef TEST_HANDLER_H
#define TEST_HANDLER_H

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include "time_util.h"
#include "tsc_x86.h"

typedef struct test_handler_t {
	unsigned int name_range;
	unsigned int range;
	unsigned int size;
	char **test_name;
	int (**functions)();
} test_handler_t;

/**
 * Allocates memory for the test_handler_t.
 *
 * @param range: number of functions we intend to add
 * @param name_range: maximum length for the unit test description string.
 *
 * @return pointer to the allocated test_handler_t
 *
 * Preconditions:
 * - range > 0
 * - name_range > 0
 * Postconditions:
 * - memory is allocated for the test_handler container and arrays for function pointers and function descriptions
 */
test_handler_t * test_handler_construct(const int range, const int name_range);


/**
 * Cleans up memory allocated for the test_handler_t.
 *
 * @param test_handler: Pointer to the test_handler that we want to destroy
 * 
 * Preconditions:
 * - test_handler should be created with test_handler_construct
 * Postconditions:
 * - Memory for test_handler and all corresponding containers is freed.
 */
void test_handler_destruct(test_handler_t * const test_handler);


/**
 * Adds a function to the test_handler
 *
 * @param test_handler: Pointer to the test_handler to which we want to add the function
 * @param function: pointer to the function that we want to add
 * @param name: pretty name to be displayed when the function is executed
 * 
 * Preconditions:
 * - test_handler should be created with test_handler_construct
 * - function should return an integer
 * - test_handler->size < test_handler->range
 * Postconditions:
 * - name is copied to the internal memory
 * - function pointer is saved to the internal memory
 */
void test_handler_add_function(test_handler_t * const test_handler, int (*function)(), const char name[]);


/**
 * Executes all functions in the test_handler
 *
 * @param test_handler: Pointer to the test_handler that we want to execute
 * 
 * Preconditions:
 * - test_handler should be created with test_handler_construct
 * Postconditions:
 * - all functions in the test_handler are executed.
 * - potential sideeffects of functions in the test_handler
 */
void test_handler_run(test_handler_t * const test_handler);


#endif //TEST_HANDLER_H