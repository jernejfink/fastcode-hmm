/**
 * @file arch_params.h
 * @author: Ivaylo Toskov
 * @brief: A header file that contains parameters for the underlying architecture.
 * @details: This file could be unique for each individual contributor and is
 *           therefore ignored by git, since different members may use different
 *           architectures.
 * @date: 27.04.2016
 */

// Processor frequency of the current system
#define FREQUENCY 2.3e9
// Parameter needed for some systems in order to get proper results for clock()
#define FREQUENCY_COEFFICIENT 2300
