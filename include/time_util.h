/**
 * @file time_util.h
 * @author: Ivaylo Toskov
 * @brief: A header file that contains helper functions to measure performance.
 * @details: Two functions for cycles measurements based on clock(). 
 * @date: 27.04.2016
 */
#ifndef TIME_UTIL_H
#define TIME_UTIL_H 

#ifndef WIN32
#include <sys/time.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "tsc_x86.h"
#include "arch_params.h"

typedef struct {
	clock_t start, end, diff;
	myInt64 start_tsc, end_tsc, diff_tsc;
} profile_timer_t;

/**
 * Records a starting timepoint with tsc and clock()
 * After measuring the time, run timer_toc to add
 * the time increment 
 *
 * @params timer: timer struct where the time information 
 * will be recorded 
 *
 * Preconditions:
 * - A timer allocated with timer_construct
 * Postconditions:
 * - Modified timer members with _start prefix
 */
void timer_tic(profile_timer_t * const timer);

/**
 * Records the stopping timepoint with tsc and clock()
 * Adds the time increment to the total runtime in diff_
 * members
 *
 * @params timer: timer struct where the time information 
 * will be recorded 
 *
 * Preconditions:
 * - timer_tic is run with the same timer before timer_toc
 *
 * Postconditions: 
 * - Modified end_ members, incremented diff_ members
 */
void timer_toc(profile_timer_t * const timer);

/**
 * Sets the total runtime of the timer to zero.
 *
 * @params timer: timer struct where the time information 
 * will be recorded 
 *
 * Preconditions:
 * - timer is allocated with timer_construct
 * Postconditions:
 * - Modified diff_ members
 */
void timer_reset(profile_timer_t * const timer);


/**
 * Returns the runtime of the timer as
 * measured by the tsc ticks.
 * 
 * @params timer: timer struct where the time information 
 * will be obtained from
 *
 * @return total runtime of the timer in tsc units
 *
 * Preconditions:
 * - timer is allocated with timer_construct
 * - timer_tic and timer_toc are run on the timer at least once 
 * Postconditions:
 * - none
 */
myInt64 timer_tsc_diff(profile_timer_t * const timer);

/**
 * Returns the runtime of the timer as
 * measured by the clock() ticks.
 * 
 * @params timer: timer struct where the time information 
 * will be obtained from
 *
 * @return total runtime of the timer in clock() units
 *
 * Preconditions:
 * - timer is allocated with timer_construct
 * - timer_tic and timer_toc are run on the timer at least once 
 * Postconditions:
 * - none
 */
double timer_clock_diff(profile_timer_t * const timer);

#endif // TIME_UTIL_H
