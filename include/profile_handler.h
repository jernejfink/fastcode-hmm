/**
	@author: Jernej Fink
	@brief: Declaration of the profiling struct and corresponding functions
	@details: The profiling struct, used for time-profiling a single function for multiple
	 		  sizes n is declared here.
	@date 11. 2. 2016

*/

#ifndef PROFILE_HANDLER_H
#define PROFILE_HANDLER_H

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include "time_util.h"
#include "tsc_x86.h"

typedef struct profile_handler_t {
	/* Function to be profiled */
	int (*function)(const unsigned n, profile_timer_t * const);

	/* Helper function that returns the op count of the function for size n */
	unsigned long long (*opcount)(const unsigned long long n);

	/* Timer to be used for profiling */
	profile_timer_t timer;

	/* Number of runs we want to have for each size */
	unsigned repeats;

	/* Pointer to the different n's */
	unsigned *sizes;

	/* Length of 'sizes' */
	unsigned num_sizes;
} profile_handler_t;

/**
 * Constructor for the profile_handler.
 * Initializes the profile_handler members with corresponding arguments
 *	
 *	@param *function Function to be profiled 
 *	@param repeats: Number of runs we want to have for each size 
 *	@param sizes: Pointer to the different n's 
 *  @param num_sizes: Length of 'sizes'
 *	@param opcount: Helper function that returns the op count of the function for size n
 *
 */
profile_handler_t * profile_handler_construct(int (*function)(const unsigned n, profile_timer_t * const), 
	const unsigned repeats, const unsigned * const sizes, const unsigned num_sizes, unsigned long long (*opcount)(const unsigned long long n) );

/**
 * Destructor for the profile_handler.
 * Frees the memory that profile_handler occupied from profile_handler_construct
 */
void profile_handler_destruct(profile_handler_t * const profile_handler);

/**
 * Profiles the function profile_handler_t->repeat times and 
 * records the mean runtime and performance in the filename 
 * in the format: data_size runtime flops/cycle
 * for every size as specified in profile_handler_t->sizes
 * The same data is output to stdout for diagnostics too.
 */
void profile_handler_run(profile_handler_t * const profile_handler, const char fname[]);

void cache_scrambler();

#endif //PROFILE_HANDLER_H