#ifndef TRAIN_H_6ED51906
#define TRAIN_H_6ED51906

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>

#include "hmm.h"
#include "utility.h"

/** 
 * Updates the hmm given a single state transition and emission
 *
 * @param unnormalized_hmm is a hmm constructed by hmm_construct
 * @param state is the number of the state in the current observation
 * @param state_next is the number of the next state. state_next can be -1
 * indicating that state is the last state in the sequence
 * @param emission is the number of the emission at this timestep
 *
 * Postconditions:
 * - initial_probability, transition_matrix and emission_matrix are incremented 
 */
void train_add_single_sample(const hmm_t * unnormalized_hmm, const int state, const int state_next, const int emission);

/** 
 * Runs the training from the file of words
 *
 * @param unnormalized_hmm is a hmm constructed by hmm_construct
 * @param file is the relative path to the file containing the words
 *
 * Preconditions:
 * - file is formatted in the style as dictated in the assignment
 * Postconditions:
 * - initial_probability, transition_matrix and emission_matrix are incremented 
 *	 in accordance with the training sequence
 */
void train_single_sequence_from_file(const hmm_t * unnormalized_hmm, const char file[]);

void train_single_sequence_from_file_log(const hmm_t * unnormalized_hmm, const char file[]);


/** 
 * Updates the hmm given a state sequence and emission sequence pair
 *
 * @param unnormalized_hmm is a hmm constructed by hmm_construct
 * @param training_sequence is constructed by hmm_sequence_construct 
 *	and has values in training_sequence->states and training_sequence->emissions
 *
 * Postconditions:
 * - initial_probability, transition_matrix and emission_matrix are incremented 
 *	 in accordance with the training sequence
 */
void train_add_sample(const hmm_t * unnormalized_hmm, const hmm_sequence_t * const training_sequence);

/** 
 * Runs the training from the file of words
 *
 * @param unnormalized_hmm is a hmm constructed by hmm_construct
 * @param file is the relative path to the file containing the words
 *
 * Preconditions:
 * - file is formatted in the style as dictated in the assignment
 * Postconditions:
 * - initial_probability, transition_matrix and emission_matrix are incremented 
 *	 in accordance with the training sequence
 */
void train_from_file(const hmm_t * unnormalized_hmm, const char file[]);

void train_from_file_log(const hmm_t * unnormalized_hmm, const char file[]);

void train_syllable_from_file(const hmm_t * unnormalized_hmm, const char file[]);

void train_syllable_from_file_log(const hmm_t * unnormalized_hmm, const char file[]);

#endif /* end of include guard: TRAIN_H_6ED51906 */
