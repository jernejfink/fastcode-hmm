#ifndef VITERBI_H
#define VITERBI_H

#include "train.h"
#include "hmm.h"
#include "time_util.h"
#include <stdlib.h>
#include <math.h>
#include <assert.h>

void viterbi_ghmm(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);

void viterbi_backward_forward_search(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);

void viterbi_backward_forward_search_log(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);

void viterbi_log_fink_1(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);

void viterbi_log_fink_1_opt(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);

void viterbi_log_fink_9(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);

void viterbi_log_fink_9_opt(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);

void viterbi_log_sam_aligned(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);

void viterbi_log_sam_aligned_opt(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);

void viterbi_log_mojmir_1(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);

void viterbi_log_mojmir_1_opt(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);

void viterbi_log_mojmir_3(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);

void viterbi_log_mojmir_3_opt(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);

void viterbi_log_fink_11(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);

void viterbi_log_fink_11_opt(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);

#endif // VITERBI_H