#ifndef UTILITY_H
#define UTILITY_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "hmm.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_RESET   "\x1b[0m"

/* print 12 spaces followed by true or false in 
 * in green/red depending on binary switch sw */
#define PRINTF_T_F(sw) printf((sw ? (ANSI_COLOR_GREEN "%12s" ANSI_COLOR_RESET) : (ANSI_COLOR_RED "%12s" ANSI_COLOR_RESET)), sw ? "true" : "false")

/* Convenience to switch -1 to infinity 
* for dealing with probablility 0 in the log space */


static const enum { _val = 'z' + 1 } __UNDERSCORE = _val;

/**
 * Takes a file saved by MATLAB
 * and imports it to the buffer (contiguously) as doubles
 *
 * @param fname is the relative path to the file
 * @param buffer is the memory location where the data will be written to.
 * @param N is the number of numbers to be read
 * @return 0 on success, 1 on file read/open errors
 *
 * Preconditions:
 * - allocated memory of size N
 * - file fname exists and has read permissions
 * - file has numbers saved in the MATLAB scientific format
 * Postconditions:
 * - N numbers written to buffer contiguously
 */
int read_real_from_file(const char fname[], real_t * const buffer, const int N);

/**
 * Takes a file saved by MATLAB
 * and imports it to the buffer (contiguously) as negative log probabilities
 *
 * @param fname is the relative path to the file
 * @param buffer is the memory location where the data will be written to.
 * @param N is the number of numbers to be read
 * @return 0 on success, 1 on file read/open errors
 *
 * Preconditions:
 * - allocated memory of size N
 * - file fname exists and has read permissions
 * - file has numbers saved in the MATLAB scientific format
 * Postconditions:
 * - N numbers written to buffer contiguously in negative log format
 *  -1: if P = 0
 * 	-log(P) if   0 < P <= 1
 */
int read_prob_from_file(const char fname[], real_t * const buffer, const int N);


/**
 * Takes a file saved by MATLAB
 * and imports it to the buffer (contiguously) as unsigned
 *
 * @param fname is the relative path to the file
 * @param buffer is the memory location where the data will be written to.
 * @param N is the number of numbers to be read
 * @return 0 on success, 1 on file read/open errors
 *
 * Preconditions:
 * - allocated memory of size N
 * - file fname exists and has read permissions
 * - file has numbers saved in the MATLAB scientific format
 * Postconditions:
 * - N numbers written to buffer contiguously
 */
int read_unsigned_from_file(const char fname[], unsigned * const buffer, const int N);


/**
 * Takes a file with training and test words
 * and returns the length of the longest word
 *
 * @param fname is the relative path to the file
 * @return length of the longest word
 *
 * Preconditions:
 * - file fname exists and has read permissions
 * - file has words written in the format as specified in the assignment
 */
unsigned longest_word(const char fname[]);


/**
 * Takes a character and returns true
 * if the character is lowercase alphabetic
 *
 * @param ch is the character to be checked
 * @return 1 if it's lowercase alphabetic, 0 otherwise
 *
 */
 static inline _Bool is_alpha(const char ch) {
    return (ch <= 'z' && ch >= 'a');
}

/**
 * Takes a file with training and test words
 * and returns the sum of all lengths of words
 * including the '-' delimiter
 *
 * @param fname is the relative path to the file
 * @return size of the test data set in bytes
 *
 * Preconditions:
 * - file fname exists and has read permissions
 * - file has words written in the format as specified in the assignment
 */
unsigned test_set_size(const char fname[]);

void read_test_data(const char fname[], hmm_sequence_t * const sequence);

int read_signed_from_file(const char fname[], int * const buffer, const int N);
int read_double_from_file(const char fname[], double * const buffer, const int N);

void convert_syllables_to_hmm_emissions(char * const buf, hmm_sequence_t * const sequence);
void convert_syllables_to_hmm_states(char * const buf, hmm_sequence_t * const sequence);
void convert_hmm_states_to_syllables(hmm_sequence_t * const sequence, char * const buf);


#endif

