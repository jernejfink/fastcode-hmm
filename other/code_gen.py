tok1 = "cost_matrix[(T+1)%2][base + {}]"
tok2 = "P[i][base + {}]"
tok3 = "P[i+1][base + {}]"
tok4 = "base + {}."
inf = "INFINITY"
strings = []
txt = ", ".join(8*["{}"])
for i in range(0, 15):
    substring1 = txt.format( *( (8-i-1)*[inf] + [tok1.format(j) for j in range(i, -1, -1)]))
    substring2 = txt.format( *( (8-i-1)*[inf] + [tok2.format(j) for j in range(i, -1, -1)]))
    substring3 = txt.format( *( (8-i-1)*[inf] + [tok3.format(j) for j in range(i, -1, -1)]))
    substring4 = txt.format( *( (8-i-1)*[inf] + [tok4.format(j) for j in range(i, -1, -1)]))

    string1 = "oc  = _mm256_set_ps({});".format(substring1)
    string2 = "tp1 = _mm256_set_ps({});".format(substring2)
    string3 = "tp2 = _mm256_set_ps({});".format(substring3)
    string4 = "jj  = _mm256_set_ps({});".format(substring4)
    print("case {}:".format(i+1))
    print(4*" " + string1)
    print(4*" " + string2)
    print(4*" " + string3)
    print(4*" " + string4)
    print(4*" " + "break;")
    print()