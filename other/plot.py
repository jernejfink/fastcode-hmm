from matplotlib import pyplot as plt
from math import log
import numpy as np
import sys

def plot(fname, x_, y_, title, plot_type="runtime"):
    tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),    
                 (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),    
                 (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),    
                 (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),    
                 (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)] 
    gray = (207/255, 207/255, 207/255)

    decades = [1000000000, 1000000, 1000]
    words = ["('000'000'000)", "('000'000)", "('000)"]
    max_y = max(y_)
    max_x = max(x_)
    word_y = ""

    x = np.array(x_)
    y = np.array(y_)

    for i in range(len(decades)):
        if max_y > decades[i]:
            word_y = words[i]
            y /= decades[i]
            break         


    fs=14

    for i in range(len(tableau20)):    
        r, g, b = tableau20[i]    
        tableau20[i] = (r / 255., g / 255., b / 255.) 


    plt.plot(x, y, "-o", color=tableau20[0], linewidth=2.)        

    ax = plt.gca()

    if(np.max(x)/np.min(y) > 1000):
        ax.semilogx()


    if(plot_type=="performance"):
        ax.set_ylim(0, 2.)

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    plt.tick_params(axis="both", which="both", bottom="on", top="off",    
                labelbottom="on", left="off", right="off", labelleft="on")
    plt.xticks(fontsize=fs, horizontalalignment="left")
    plt.yticks(fontsize=fs, horizontalalignment="left", x=-0.1, transform=ax.get_yaxis_text1_transform(0)[0])
    plt.grid()

    ax.xaxis.grid(False)
    ax.yaxis.grid(linestyle="-", linewidth=1, color=gray)
    ax.set_axisbelow(True)


    x_low, x_up = ax.axes.get_xlim()
    y_low, y_up = ax.axes.get_ylim()

    label = "FLOPs/cycle" if plot_type=="performance" else "# cycles"

    title2 = plt.text(-0.1, 0.12*(y_up - y_low) + y_up, title, fontsize=fs+2, weight="bold", ha='left', transform=ax.get_yaxis_text1_transform(0)[0])
    ylabel = plt.text(-0.1, 0.07*(y_up - y_low) + y_up, label + word_y, fontsize=fs, family="sans-serif", transform=ax.get_yaxis_text1_transform(0)[0])

    xlabel = plt.text(0, -0.12, "Sequence length ", fontsize=fs, transform=ax.transAxes)
    fname_label = plt.text(0, -0.2, fname[:-3], fontsize=fs-4, transform=ax.transAxes)

    plt.tight_layout(pad=4.6)
    plt.savefig(fname[:-3] + "pdf")


