from string import Template
s1 = """
const __m256 cmp${x} = _mm256_cmp_ps(mc${x}_1, mc${x}_2, 0x01);
const __m256 mc${x} = _mm256_blendv_ps(mc${x}_2, mc${x}_1, cmp${x});
const __m256 mj${x} = _mm256_blendv_ps(mj${x}_2, mj${x}_1, cmp${x});
const __m256 ep${x} = _mm256_load_ps(&E[z][i+${x}]);
__m256 mc${x}fin = _mm256_add_ps(mc${x}, ep${x});
const __m256i mj${x}fin = _mm256_cvtps_epi32(mj${x});
"""

s2 = """const __m256 ip${x} = _mm256_load_ps(&hmm->initial_probability[i]);
mc${x}fin = _mm256_add_ps(mc${x}fin, ip${x});"""

s3 = "_mm256_store_ps(&cost_matrix[T%2][i+${s}*${y}],    mc${x}fin);"
s4 = "_mm256_store_si256((__m256i *) &policy_matrix[T][i+${s}*${y}], mj${x}fin);"

templ = Template(s1)

strides = [1, 2, 4, 8]
for j in strides:
      print("STRIDE {}".format(j))
      for k in range(1, j+1):
            res = templ.substitute(x = k, s = j, y = k-1)
            print(res)