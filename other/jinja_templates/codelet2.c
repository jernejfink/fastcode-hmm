const __m256 DIFF_J = _mm256_set1_ps(%%unroll_x%%.);
for(int T = n-2; T >= 0; --T) {
    const unsigned z = sequence->emissions[T];

    for(int i = 0; i < k; i += %% unroll_y * reg_size %%) {     

        <% for f in range(1, unroll_x+1) -%>

        const __m256 oc%%f%% = _mm256_set1_ps(cost_matrix[(T+1)%2][%%f-1%%]);
        <% for h in range(1, unroll_y+1) -%>
        const __m256 tp%%h%%_%%f%% = _mm256_load_ps(&P[%%f-1%%][i + %% reg_size*(h - 1) %%]);
        <% endfor %>
        <% for h in range(1, unroll_y+1) -%>                
        __m256 mc%%h%%_%%f%% = _mm256_add_ps(oc%%f%%, tp%%h%%_%%f%%);
        <% endfor %>        
        <% for h in range(1, unroll_y+1) -%>
        __m256 mj%%h%%_%%f%% = _mm256_set1_ps(%%f - 1%%.);
        <% endfor %>        

        __m256 jj%%f%% = _mm256_set1_ps(%%unroll_x + f - 1%%.);
        <% endfor -%>

        for(int j = %%unroll_x%%; j < k; j += %%unroll_x%%) {
    
            <% for f in range(1, unroll_x+1) %>
            const __m256 oc%%f%% = _mm256_set1_ps(cost_matrix[(T+1)%2][j+%%f-1%%]);

            <% for h in range(1, unroll_y+1) -%>                
            const __m256 tp%%h%%_%%f%% = _mm256_load_ps(&P[j+%%f-1%%][i + %%reg_size * (h - 1)%%]);
            <% endfor %>        
            <% for h in range(1, unroll_y+1) -%>                
            const __m256 nc%%h%%_%%f%% = _mm256_add_ps(oc%%f%%, tp%%h%%_%%f%%);
            <% endfor %>        
            <% for h in range(1, unroll_y+1) -%>                
            const __m256 cmp%%h%%_%%f%% = _mm256_cmp_ps(nc%%h%%_%%f%%, mc%%h%%_%%f%%, 0x01);
            <% endfor %>        
            <% for h in range(1, unroll_y+1) -%>                
            mc%%h%%_%%f%% = _mm256_blendv_ps(mc%%h%%_%%f%%, nc%%h%%_%%f%%, cmp%%h%%_%%f%%);
            <% endfor %>        
            <% for h in range(1, unroll_y+1) -%>                
            mj%%h%%_%%f%% = _mm256_blendv_ps(mj%%h%%_%%f%%, jj%%f%%, cmp%%h%%_%%f%%);
            <% endfor %>        

            jj%%f%% = _mm256_add_ps(jj%%f%%, DIFF_J);
            <% endfor %>
        }
        <% for f in range(1, unroll_x+1) -%>   
        <% for h in range(1, unroll_y+1) -%>     
        const __m256 mc__%%unroll_x%%__%%h%%__%%f%% = mc%%h%%_%%f%%;
        <% endfor -%>
        <% for h in range(1, unroll_y+1) -%>             
        const __m256 mj__%%unroll_x%%__%%h%%__%%f%% = mj%%h%%_%%f%%;
        <% endfor -%>
        <% endfor %>

              
        <% for f in unroll_x_logs %>
        <% for t in range(1, f+1) -%>
        <% for h in range(1, unroll_y+1) -%> 
        const __m256 cmp_%%f%%__%%h%%__%%t%% = _mm256_cmp_ps(mc__%%f*2%%__%%h%%__%%2*t-1%%, mc__%%f*2%%__%%h%%__%%2*t%%, 0x01);
        <% endfor %>
        <% for h in range(1, unroll_y+1) -%>  
        const __m256 mc__%%f%%__%%h%%__%%t%% = _mm256_blendv_ps(mc__%%f*2%%__%%h%%__%%2*t%%, mc__%%f*2%%__%%h%%__%%2*t-1%%, cmp_%%f%%__%%h%%__%%t%%);                
        <% endfor %>
        <% for h in range(1, unroll_y+1) -%>  
        const __m256 mj__%%f%%__%%h%%__%%t%% = _mm256_blendv_ps(mj__%%f*2%%__%%h%%__%%2*t%%, mj__%%f*2%%__%%h%%__%%2*t-1%%, cmp_%%f%%__%%h%%__%%t%%);                
        <% endfor %>
        <% endfor -%>
        <% endfor %>

        <% for h in range(1, unroll_y+1) -%>                
        const __m256 mc%%h%% = mc__1__%%h%%__1;
        <% endfor %>        
        <% for h in range(1, unroll_y+1) -%>                
        const __m256 mj%%h%% = mj__1__%%h%%__1;
        <% endfor %>        
        <% for h in range(1, unroll_y+1) -%>                
        const __m256 ep%%h%% = _mm256_load_ps(&E[z][i + %%reg_size*(h - 1)%%]);
        <% endfor %>        
        <% for h in range(1, unroll_y+1) -%>                
        __m256 mc%%h%%fin = _mm256_add_ps(mc%%h%%, ep%%h%%);
        <% endfor %>        
        <% for h in range(1, unroll_y+1) -%>                
        const __m256i mj%%h%%fin = _mm256_cvtps_epi32(mj%%h%%);
        <% endfor %>        
        if(T == 0) {
            <% for h in range(1, unroll_y+1) -%>                
            const __m256 ip%%h%% = _mm256_load_ps(&hmm->initial_probability[i + %%reg_size*(h - 1)%%]);
            <% endfor %>        
            <% for h in range(1, unroll_y+1) -%>                
            mc%%h%%fin = _mm256_add_ps(mc%%h%%fin, ip%%h%%);
            <% endfor %>        
        }
        <% for h in range(1, unroll_y+1) -%>                
        _mm256_store_ps(&cost_matrix[T%2][i + %%reg_size*(h - 1)%%], mc%%h%%fin);        
        <% endfor %>        
        <% for h in range(1, unroll_y+1) -%>                
        _mm256_store_si256((__m256i *) &policy_matrix[T][i + %%reg_size*(h - 1)%%], mj%%h%%fin);
        <% endfor %>        
    }
}