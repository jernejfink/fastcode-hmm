from jinja2 import Environment, PackageLoader
env = Environment(loader=PackageLoader(__name__, '.'), 
				  block_start_string='<%', 
				  block_end_string='%>',
			      variable_start_string='%%',
				  variable_end_string='%%')

template = env.get_template("codelet2.c")
ux = 2
print(template.render(unroll_y = 4, unroll_x = ux, reg_size = 8, unroll_x_logs=[ux // pow(2, t) for t in range(1, ux) if ux // pow(2, t) > 0]))