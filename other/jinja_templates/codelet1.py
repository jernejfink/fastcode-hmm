from jinja2 import Environment, PackageLoader
env = Environment(loader=PackageLoader(__name__, '.'), 
				  block_start_string='<%', 
				  block_end_string='%>',
			      variable_start_string='%%',
				  variable_end_string='%%')

template = env.get_template("codelet1.c")
print(template.render(unroll_y = 4, reg_size = 8))