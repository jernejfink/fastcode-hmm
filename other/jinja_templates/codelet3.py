from jinja2 import Environment, PackageLoader
env = Environment(loader=PackageLoader(__name__, '.'), 
				  block_start_string='<%', 
				  block_end_string='%>',
			      variable_start_string='%%',
				  variable_end_string='%%')

template = env.get_template("codelet3.c")

ux = 4
uy = 2

stump = "log_fink_11"
optimization_name="viterbi_" + stump
with open("../../src/viterbi/{}.c".format(optimization_name), "w") as f:
	f.write(template.render(optimization_name=optimization_name, unroll_y = uy, unroll_x = ux, unroll_x_logs=[ux // pow(2, t) for t in range(1, ux) if ux // pow(2, t) > 0]))

with open("../../include/viterbi.h", "r+") as f:
	data = f.read()
	prototype = "void {}(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);".format(optimization_name)
	prototype_opt = "void {}_opt(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer);".format(optimization_name)

	lines = data.split("\n")

	if prototype not in data:
		lines.insert(-2, "\n" + prototype)

	if prototype_opt not in data:
		lines.insert(-2, "\n" + prototype_opt)

	f.seek(0)
	f.write("\n".join(lines))		

profile_template = env.get_template("profile_codelet3.c")
with open("../../src/profiles/profile_{}.c".format(stump), "w") as f:
	f.write(profile_template.render(optimization_name=optimization_name))

with open("../../src/profiles/profile_{}_opt.c".format(stump), "w") as f:
	f.write(profile_template.render(optimization_name=optimization_name+"_opt"))



test_template = env.get_template("test_codelet3.c")
with open("../../src/tests/test_{}.c".format(stump), "w") as f:
	f.write(test_template.render(optimization_name=optimization_name))

with open("../../src/tests/test_{}_opt.c".format(stump), "w") as f:
	f.write(test_template.render(optimization_name=optimization_name+"_opt"))
