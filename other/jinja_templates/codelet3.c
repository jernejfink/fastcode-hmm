#include "viterbi.h"
#include "utility.h"
#include <immintrin.h>

/* Unrolled the middle loop. Perfomance increase for small state size. No drop for large state size */
#ifdef GCC
__attribute__((optimize("no-tree-vectorize")))
__attribute__((optimize("no-unroll-loops")))
#endif
void %%optimization_name%%(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer) {
    const unsigned n = sequence->size;
    const unsigned k = hmm->num_states;

    real_t   (* cost_matrix)[k] = (real_t (*)[k]) calloc(sizeof(real_t), k*2);
    unsigned (* policy_matrix)[k] = (unsigned (*)[k]) calloc(sizeof(unsigned), k*n);

    real_t (* P)[k] = (real_t (*)[k]) hmm->transition_probability;
    real_t (* E)[k] = (real_t (*)[k]) hmm->emission_probability;
    real_t (* E_tr)[k] = (real_t (*)[k]) hmm->emission_probability_tr;


    /** Newly added timer. @date May 10 2016 */
    timer_tic(timer);

    /** Initial probability part */
    const unsigned zN = sequence->emissions[n-1];
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif  
    for(int i = 0; i < k; ++i){
        const real_t emission_prob = E_tr[zN][i];
        cost_matrix[(n-1)%2][i] = emission_prob;
    }       
    /** Backwards part */
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif  
    for(int T = n-2; T >= 0; --T) {
        const unsigned z = sequence->emissions[T];  
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif
        for(int i = 0; i < k; i+= %%unroll_y%%) {
            <% for f in range(1, unroll_x + 1) -%>
            const real_t c%%f%% = cost_matrix[(T + 1) % 2][%%f-1%%];
            <% endfor %>            
            <% for f in range(1, unroll_x + 1) -%>
            <% for h in range(1, unroll_y + 1) -%>
            int mj%%unroll_x%%_%%h%%_%%f%% = %%f-1%%;
            <% endfor %>
            <% endfor %>
            // Min costs 
            <% for f in range(1, unroll_x + 1) -%>
            <% for h in range(1, unroll_y + 1) -%>
            real_t mc%%unroll_x%%_%%h%%_%%f%% = P[i+%%h-1%%][%%f-1%%] + c%%f%%;
            <% endfor %>
            <% endfor %>            
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif
            for(int j = %%unroll_x%%; j < k; j += %%unroll_x%%) {
                <% for f in range(1, unroll_x + 1) -%>
                const real_t oc%%f%% = cost_matrix[(T+1) % 2][j+%%f-1%%];
                <% endfor %>            
                <% for f in range(1, unroll_x + 1) -%>
                <% for h in range(1, unroll_y + 1) -%>              
                const real_t t%%h%%_%%f%% = P[i+%%h-1%%][j+%%f-1%%];
                <% endfor %>
                <% endfor %>
                <% for f in range(1, unroll_x + 1) -%>
                <% for h in range(1, unroll_y + 1) -%>              
                const real_t nc%%h%%_%%f%% = oc%%f%% + t%%h%%_%%f%%;
                <% endfor %>
                <% endfor %>
                <% for f in range(1, unroll_x + 1) -%>
                <% for h in range(1, unroll_y + 1) -%>  
                const _Bool cmp%%h%%_%%f%% = nc%%h%%_%%f%% < mc%%unroll_x%%_%%h%%_%%f%%;
                <% endfor %>
                <% endfor %>
                <% for f in range(1, unroll_x + 1) -%>
                <% for h in range(1, unroll_y + 1) -%>  
                    mj%%unroll_x%%_%%h%%_%%f%% = cmp%%h%%_%%f%% ? j+%%f-1%% : mj%%unroll_x%%_%%h%%_%%f%%;
                <% endfor %>
                <% endfor %>
                <% for f in range(1, unroll_x + 1) -%>
                <% for h in range(1, unroll_y + 1) -%>  
                    mc%%unroll_x%%_%%h%%_%%f%% = cmp%%h%%_%%f%% ? nc%%h%%_%%f%% : mc%%unroll_x%%_%%h%%_%%f%%;
                <% endfor %>
                <% endfor %>
            }
            <% for k in unroll_x_logs -%>
            <% for f in range(1, k+1) -%>
            <% for h in range(1, unroll_y+1) -%> 
                <% if k == 1 -%>
                const _Bool cmp%%h%% = mc%%k*2%%_%%h%%_%%2*f-1%% < mc%%k*2%%_%%h%%_%%2*f%%;
                const real_t mc%%h%% = cmp%%h%% ? mc%%k*2%%_%%h%%_%%2*f-1%% : mc%%k*2%%_%%h%%_%%2*f%%;
                const int mj%%h%% = cmp%%h%% ? mj%%k*2%%_%%h%%_%%2*f-1%% : mj%%k*2%%_%%h%%_%%2*f%%;
                <% else -%>
                const _Bool cmp%%k%%_%%h%%_%%f%% = mc%%k*2%%_%%h%%_%%2*f-1%% < mc%%k*2%%_%%h%%_%%2*f%%;
                const real_t mc%%k%%_%%h%%_%%f%% = cmp%%k%%_%%h%%_%%f%% ? mc%%k*2%%_%%h%%_%%2*f-1%% : mc%%k*2%%_%%h%%_%%2*f%%;
                const int mj%%k%%_%%h%%_%%f%% = cmp%%k%%_%%h%%_%%f%% ? mj%%k*2%%_%%h%%_%%2*f-1%% : mj%%k*2%%_%%h%%_%%2*f%%;                
                <% endif %>
            <% endfor %>
            <% endfor %>
            <% endfor %>
            // Emission probs 
            <% for h in range(1, unroll_y + 1) -%>
            const real_t e%%h%% = E[i+%%h-1%%][z];
            <% endfor %>
            <% for h in range(1, unroll_y + 1) -%>
            real_t fc%%h%%   = mc%%h%% + e%%h%%;
            <% endfor %>
            if(T == 0){
                <% for h in range(1, unroll_y + 1) -%>
                fc%%h%%  += hmm->initial_probability[i + %%h-1%%];
                <% endfor %>            
            }
            <% for h in range(1, unroll_y + 1) -%>
            cost_matrix[T % 2][i + %%h-1%%]   = fc%%h%%;
            <% endfor %>
            <% for h in range(1, unroll_y + 1) -%>
            policy_matrix[T][i+%%h-1%%]   = mj%%h%%;
            <% endfor %>
        }
    }

    /** Forward part */

    real_t min_cost = cost_matrix[0][0];
    int min_i = 0;
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif  
    for(int i = 1; i < k; ++i) {
        real_t cost = cost_matrix[0][i];
        if(cost < min_cost) {
            min_i = i;
            min_cost = cost;
        }
    }
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif
    for(int T = 0; T < n; ++T) {
        sequence->states[T] = min_i;
        min_i = policy_matrix[T][min_i];
    }
    timer_toc(timer);

    free(cost_matrix);
    free(policy_matrix);

}


#ifdef GCC
__attribute__((optimize("no-tree-vectorize")))
#endif
void %%optimization_name%%_opt(const hmm_t * const hmm, hmm_sequence_t * const sequence, profile_timer_t * const timer) {
    const unsigned n = sequence->size;
    const unsigned k = hmm->num_states;

    real_t   (* cost_matrix)[k] = (real_t (*)[k]) calloc(sizeof(real_t), k*2);
    unsigned (* policy_matrix)[k] = (unsigned (*)[k]) calloc(sizeof(unsigned), k*n);

    real_t (* P)[k] = (real_t (*)[k]) hmm->transition_probability;
    real_t (* E)[k] = (real_t (*)[k]) hmm->emission_probability;
    real_t (* E_tr)[k] = (real_t (*)[k]) hmm->emission_probability_tr;


    /** Newly added timer. @date May 10 2016 */
    timer_tic(timer);

    /** Initial probability part */
    const unsigned zN = sequence->emissions[n-1];
#ifdef ICC
#pragma novector
#pragma unroll_and_jam
#endif  
    for(int i = 0; i < k; ++i){
        const real_t emission_prob = E_tr[zN][i];
        cost_matrix[(n-1)%2][i] = emission_prob;
    }       
    /** Backwards part */
#ifdef ICC
#pragma novector
#pragma nounroll_and_jam
#endif  
    for(int T = n-2; T >= 0; --T) {
        const unsigned z = sequence->emissions[T];  
#ifdef ICC
#pragma novector
#pragma unroll_and_jam
#endif
        for(int i = 0; i < k; i+= %%unroll_y%%) {
            <% for f in range(1, unroll_x + 1) -%>
            const real_t c%%f%% = cost_matrix[(T + 1) % 2][%%f-1%%];
            <% endfor %>            
            <% for f in range(1, unroll_x + 1) -%>
            <% for h in range(1, unroll_y + 1) -%>
            int mj%%unroll_x%%_%%h%%_%%f%% = %%f-1%%;
            <% endfor %>
            <% endfor %>
            // Min costs 
            <% for f in range(1, unroll_x + 1) -%>
            <% for h in range(1, unroll_y + 1) -%>
            real_t mc%%unroll_x%%_%%h%%_%%f%% = P[i+%%h-1%%][%%f-1%%] + c%%f%%;
            <% endfor %>
            <% endfor %>            
#ifdef ICC
#pragma novector
#pragma unroll_and_jam            
#endif
            for(int j = %%unroll_x%%; j < k; j += %%unroll_x%%) {
                <% for f in range(1, unroll_x + 1) -%>
                const real_t oc%%f%% = cost_matrix[(T+1) % 2][j+%%f-1%%];
                <% endfor %>            
                <% for f in range(1, unroll_x + 1) -%>
                <% for h in range(1, unroll_y + 1) -%>              
                const real_t t%%h%%_%%f%% = P[i+%%h-1%%][j+%%f-1%%];
                <% endfor %>
                <% endfor %>
                <% for f in range(1, unroll_x + 1) -%>
                <% for h in range(1, unroll_y + 1) -%>              
                const real_t nc%%h%%_%%f%% = oc%%f%% + t%%h%%_%%f%%;
                <% endfor %>
                <% endfor %>
                <% for f in range(1, unroll_x + 1) -%>
                <% for h in range(1, unroll_y + 1) -%>  
                const _Bool cmp%%h%%_%%f%% = nc%%h%%_%%f%% < mc%%unroll_x%%_%%h%%_%%f%%;
                <% endfor %>
                <% endfor %>
                <% for f in range(1, unroll_x + 1) -%>
                <% for h in range(1, unroll_y + 1) -%>  
                    mj%%unroll_x%%_%%h%%_%%f%% = cmp%%h%%_%%f%% ? j+%%f-1%% : mj%%unroll_x%%_%%h%%_%%f%%;
                <% endfor %>
                <% endfor %>
                <% for f in range(1, unroll_x + 1) -%>
                <% for h in range(1, unroll_y + 1) -%>  
                    mc%%unroll_x%%_%%h%%_%%f%% = cmp%%h%%_%%f%% ? nc%%h%%_%%f%% : mc%%unroll_x%%_%%h%%_%%f%%;
                <% endfor %>
                <% endfor %>
            }
            <% for k in unroll_x_logs -%>
            <% for f in range(1, k+1) -%>
            <% for h in range(1, unroll_y+1) -%> 
                <% if k == 1 -%>
                const _Bool cmp%%h%% = mc%%k*2%%_%%h%%_%%2*f-1%% < mc%%k*2%%_%%h%%_%%2*f%%;
                const real_t mc%%h%% = cmp%%h%% ? mc%%k*2%%_%%h%%_%%2*f-1%% : mc%%k*2%%_%%h%%_%%2*f%%;
                const int mj%%h%% = cmp%%h%% ? mj%%k*2%%_%%h%%_%%2*f-1%% : mj%%k*2%%_%%h%%_%%2*f%%;
                <% else -%>
                const _Bool cmp%%k%%_%%h%%_%%f%% = mc%%k*2%%_%%h%%_%%2*f-1%% < mc%%k*2%%_%%h%%_%%2*f%%;
                const real_t mc%%k%%_%%h%%_%%f%% = cmp%%k%%_%%h%%_%%f%% ? mc%%k*2%%_%%h%%_%%2*f-1%% : mc%%k*2%%_%%h%%_%%2*f%%;
                const int mj%%k%%_%%h%%_%%f%% = cmp%%k%%_%%h%%_%%f%% ? mj%%k*2%%_%%h%%_%%2*f-1%% : mj%%k*2%%_%%h%%_%%2*f%%;                
                <% endif %>
            <% endfor %>
            <% endfor %>
            <% endfor %>
            // Emission probs 
            <% for h in range(1, unroll_y + 1) -%>
            const real_t e%%h%% = E[i+%%h-1%%][z];
            <% endfor %>
            <% for h in range(1, unroll_y + 1) -%>
            real_t fc%%h%%   = mc%%h%% + e%%h%%;
            <% endfor %>
            if(T == 0){
                <% for h in range(1, unroll_y + 1) -%>
                fc%%h%%  += hmm->initial_probability[i + %%h-1%%];
                <% endfor %>            
            }
            <% for h in range(1, unroll_y + 1) -%>
            cost_matrix[T % 2][i + %%h-1%%]   = fc%%h%%;
            <% endfor %>
            <% for h in range(1, unroll_y + 1) -%>
            policy_matrix[T][i+%%h-1%%]   = mj%%h%%;
            <% endfor %>
        }
    }

    /** Forward part */

    real_t min_cost = cost_matrix[0][0];
    int min_i = 0;
#ifdef ICC
#pragma novector
#pragma unroll_and_jam
#endif  
    for(int i = 1; i < k; ++i) {
        real_t cost = cost_matrix[0][i];
        if(cost < min_cost) {
            min_i = i;
            min_cost = cost;
        }
    }
#ifdef ICC
#pragma novector
#pragma unroll_and_jam    
#endif
    for(int T = 0; T < n; ++T) {
        sequence->states[T] = min_i;
        min_i = policy_matrix[T][min_i];
    }
    timer_toc(timer);

    free(cost_matrix);
    free(policy_matrix);

}