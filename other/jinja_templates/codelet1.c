for(int T = n-2; T >= 0; --T) {
    const unsigned z = sequence->emissions[T];

    for(int i = 0; i < k; i += %% unroll_y * reg_size %%) {     
        const __m256 oc1 = _mm256_set1_ps(cost_matrix[(T+1)%2][0]);
        <% for h in range(1, unroll_y+1) -%>
        const __m256 tp%%h%%_1 = _mm256_load_ps(&P[0][i + %% reg_size*(h - 1) %%]);
        <% endfor %>
        <% for h in range(1, unroll_y+1) -%>                
        __m256 mc%%h%%_1 = _mm256_add_ps(oc1, tp%%h%%_1);
        <% endfor %>        
        <% for h in range(1, unroll_y+1) -%>
        __m256 mj%%h%%_1 = _mm256_set1_ps(0.);
        <% endfor %>        

        __m256 jj1 = _mm256_set1_ps(1.);

        for(int j = 1; j < k; ++j) {
    
            const __m256 oc1 = _mm256_set1_ps(cost_matrix[(T+1)%2][j]);

            <% for h in range(1, unroll_y+1) -%>                
            const __m256 tp%%h%%_1 = _mm256_load_ps(&P[j][i + %%reg_size * (h - 1)%%]);
            <% endfor %>        
            <% for h in range(1, unroll_y+1) -%>                
            const __m256 nc%%h%%_1 = _mm256_add_ps(oc1, tp%%h%%_1);
            <% endfor %>        
            <% for h in range(1, unroll_y+1) -%>                
            const __m256 cmp%%h%%_1 = _mm256_cmp_ps(nc%%h%%_1, mc%%h%%_1, 0x01);
            <% endfor %>        
            <% for h in range(1, unroll_y+1) -%>                
            mc%%h%%_1 = _mm256_blendv_ps(mc%%h%%_1, nc%%h%%_1, cmp%%h%%_1);
            <% endfor %>        
            <% for h in range(1, unroll_y+1) -%>                
            mj%%h%%_1 = _mm256_blendv_ps(mj%%h%%_1, jj1, cmp%%h%%_1);
            <% endfor %>        

            jj1 = _mm256_add_ps(jj1, ONE);
        }
        <% for h in range(1, unroll_y+1) -%>                
        const __m256 mc%%h%% = mc%%h%%_1;
        <% endfor %>        
        <% for h in range(1, unroll_y+1) -%>                
        const __m256 mj%%h%% = mj%%h%%_1;
        <% endfor %>        
        <% for h in range(1, unroll_y+1) -%>                
        const __m256 ep%%h%% = _mm256_load_ps(&E[z][i + %%reg_size*(h - 1)%%]);
        <% endfor %>        
        <% for h in range(1, unroll_y+1) -%>                
        __m256 mc%%h%%fin = _mm256_add_ps(mc%%h%%, ep%%h%%);
        <% endfor %>        
        <% for h in range(1, unroll_y+1) -%>                
        const __m256i mj%%h%%fin = _mm256_cvtps_epi32(mj%%h%%);
        <% endfor %>        
        if(T == 0) {
            <% for h in range(1, unroll_y+1) -%>                
            const __m256 ip%%h%% = _mm256_load_ps(&hmm->initial_probability[i + %%reg_size*(h - 1)%%]);
            <% endfor %>        
            <% for h in range(1, unroll_y+1) -%>                
            mc%%h%%fin = _mm256_add_ps(mc%%h%%fin, ip%%h%%);
            <% endfor %>        
        }
        <% for h in range(1, unroll_y+1) -%>                
        _mm256_store_ps(&cost_matrix[T%2][i + %%reg_size*(h - 1)%%], mc%%h%%fin);        
        <% endfor %>        
        <% for h in range(1, unroll_y+1) -%>                
        _mm256_store_si256((__m256i *) &policy_matrix[T][i + %%reg_size*(h - 1)%%], mj%%h%%fin);
        <% endfor %>        
    }
}