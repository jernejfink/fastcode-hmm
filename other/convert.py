num_lines = sum(1 for line in open("../data/war_and_peace.txt"))
ratio = 0.8

training = int(0.8*num_lines)
with open("../data/war_and_peace.txt") as f:
	with(open("../data/typos_war_and_peace.data", "w")) as w:
		for i, line in enumerate(f):
			without_endl = line[:-1]
			both = without_endl.split(" ")
			correct = both[0]
			typo = both[1]
			
			if(len(correct) != len(typo)):
				raise Exception("Words not of equal length. Line {}".format(i))

			ending = "..\n" if i == training else "_ _\n" 
			new_format = "\n".join(["{} {}".format(x, y) for x, y in zip(correct, typo)]) + "\n" + ending
			w.write(new_format)



