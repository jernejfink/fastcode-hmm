import sys
import numpy
# take as argument list of lines
for fil in sys.stdin:
	flags = fil.split("#")[1]
	comp = flags.split("-")[0]
	opt = flags.split("-")[1]
	# extract constants
	# open 
	f = open("../profiles/"+fil.strip(),"r")
	# take average performance 
	avg_perf = 0.0
	n = 0
	for line in f:
		n+=1
		avg_perf+=float(line.split(" ")[2])
	avg_perf = avg_perf/(float(n))
	print fil.strip(),comp, opt, avg_perf