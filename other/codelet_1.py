line = "const __m256 tp{} = _mm256_set_ps({})"
for i in range(0, 8):
	k = []
	for j in range(7, -1, -1):
		k.extend([j, i])
	args = ", ".join(8*["P[i+{}][{}]"])
	s = args.format(*k)
	print(line.format(i+1, s))