N=27;
% rand('seed', 3);
tran_mat = rand(N, N);
emission_mat = rand(N, N);
emission_mat = emission_mat;
row_sum = sum(emission_mat, 2);
emission_mat = diag(1./row_sum) * emission_mat;

row_sum = sum(tran_mat, 2);
tran_mat = diag(1./row_sum) * tran_mat;

initial_prob = rand(1, N);
col_sum = sum(initial_prob, 2);
initial_prob = initial_prob ./ col_sum;

tran_mat32 = zeros(32, 32);
tran_mat32(1:N, 1:N) = tran_mat;

emission_mat32 = zeros(32, 32);
emission_mat32(1:N, 1:N) = emission_mat;

initial_prob32 = zeros(1, 32);
initial_prob32(1:N) = initial_prob;

TRANS_HAT = [0 initial_prob; zeros(size(tran_mat, 1), 1) tran_mat];

EMIS_HAT = [zeros(1, size(emission_mat, 2)); emission_mat];

emissions=randi(N, 1, N);
estimated_states = hmmviterbi(emissions, TRANS_HAT, EMIS_HAT);

emissions_2 = emissions - 1;
estimated_states_2 = estimated_states - 2;

emissions_longer=randi(N, 1, 27);
estimated_states_longer = hmmviterbi(emissions_longer, TRANS_HAT, EMIS_HAT);

emissions_longer_2 = emissions_longer - 1;
estimated_states_longer_2 = estimated_states_longer - 2;

save '../data/fink_tran_aligned.txt' tran_mat32 -ASCII
save '../data/fink_emis_aligned.txt' emission_mat32 -ASCII
save '../data/fink_init_aligned.txt' initial_prob32 -ASCII
save '../data/fink_emissions_aligned.txt' emissions_2 -ASCII
save '../data/fink_estimates_aligned.txt' estimated_states_2 -ASCII
save '../data/fink_emissions_aligned_longer.txt' emissions_longer_2 -ASCII
save '../data/fink_estimates_aligned_longer.txt' estimated_states_longer_2 -ASCII