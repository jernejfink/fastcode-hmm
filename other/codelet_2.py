token0 = "for(int j = {}; j < k; j += {}) {{"

token1 = """
    const __m256 oc{} = _mm256_set1_ps(cost_matrix[(T+1)%2][j+{}]);

    const __m256 tp1_{} = _mm256_load_ps(&P[j+{}][i]);
    const __m256 tp2_{} = _mm256_load_ps(&P[j+{}][i+8]);

    const __m256 nc1_{} = _mm256_add_ps(oc1, tp1_1);
    const __m256 nc2_{} = _mm256_add_ps(oc1, tp2_1);

    const __m256 cmp1_{} = _mm256_cmp_ps(nc1_{}, mc1_{}, 0x01);
    const __m256 cmp2_{} = _mm256_cmp_ps(nc2_{}, mc2_{}, 0x01);

    mc1_{} = _mm256_blendv_ps(mc1_{}, nc1_{}, cmp1_{});
    mc2_{} = _mm256_blendv_ps(mc2_{}, nc2_{}, cmp2_{});

    mj1_{} = _mm256_blendv_ps(mj1_{}, jj{}, cmp1_{});
    mj2_{} = _mm256_blendv_ps(mj2_{}, jj{}, cmp2_{});"""

token2 = "jj{} = _mm256_add_ps(jj{}, DIFF);"

token3 = "}"

tokenDIFF = "const __m256 DIFF = _mm256_set1_ps({}.);"

strides = [1, 2, 4, 8]
for j in strides:
    print(tokenDIFF.format(j))
    print(token0.format(j, j))
    for k in range(1,j+1):
        print("    " + token1.format(* 30*[k] ) )
    for k in range(1,j+1):
        print("    " + token2.format(k, k))
    print(token3)
