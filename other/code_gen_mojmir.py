import numpy as np 
"""
for i in np.arange(1,9,1):
    print "const __m256 tp1_" +str(i) + "= _mm256_load_ps(&P[i+" + str(i-1) + "][0]);"
    print "const __m256 tp2_" +str(i) + "= _mm256_load_ps(&P[i+" + str(i-1) + "][VV]);"
    print "const __m256 tp3_" +str(i) + "= _mm256_load_ps(&P[i+" + str(i-1) + "][2*VV]);"
    print "const __m256 tp4_" +str(i) + "= _mm256_load_ps(&P[i+" + str(i-1) + "][3*VV]);"
    print
    print "const __m256 s1_" +str(i) + "= _mm256_add_ps(tp1_"+str(i)+",s1);"
    print "const __m256 s2_" +str(i) + "= _mm256_add_ps(tp2_"+str(i)+",s2);"
    print "const __m256 s3_" +str(i) + "= _mm256_add_ps(tp3_"+str(i)+",s3);"
    print "const __m256 s4_" +str(i) + "= _mm256_add_ps(tp4_"+str(i)+",s4);"
    print 
    print "const __m256 A12_"+str(i) + "= _mm256_min_ps(s1_" + str(i) + ",s2_" + str(i) + ");"
    print "const __m256 A34_"+str(i) + "= _mm256_min_ps(s3_" + str(i) + ",s4_" + str(i) + ");"
    print "__m256 A_s_"+str(i) + "= _mm256_min_ps(A12_" + str(i) + ",A34_" + str(i) + ");"
    print


"""
for i in np.arange(1,9,1):

    print "const __m256 mask1_" + str(i) +" = _mm256_cmp_ps(m1_"+str(i)+",s1_" +str(i)+",0);"
    print "const __m256 ind1_" +str(i)+" = _mm256_blendv_ps(ZERO,LADDER,mask1_"+str(i)+");"
    print "const __m256 mask2_" +str(i)+" = _mm256_cmp_ps(m1_"+str(i)+",s2_" + str(i) + ",0);"
    print "const __m256 ind2_" +str(i)+" = _mm256_blendv_ps(ind1_" +str(i)+",LADDER_2,mask2_"+str(i)+");"
    print "const __m256 mask3_" +str(i)+" = _mm256_cmp_ps(m1_"+str(i)+",s3_" + str(i) + ",0);"
    print "const __m256 ind3_" +str(i)+" = _mm256_blendv_ps(ind2_" +str(i)+",LADDER_3,mask3_"+str(i)+");"
    print "const __m256 mask4_" +str(i)+" = _mm256_cmp_ps(m1_"+str(i)+",s4_" + str(i) + ",0);"
    print "__m256 ind" +str(i)+" = _mm256_blendv_ps(ind3_" +str(i)+",LADDER_4,mask4_"+str(i)+");"
    print 
