import plot
import sys

fname = sys.argv[1]
with open(fname, "r") as f:
    data = [line[:-1].split(" ") for line in f.readlines()]
    x = [float(line[0]) for line in data]
    y = [float(line[2]) for line in data]

plot.plot(fname, x, y, "Performance", "performance")