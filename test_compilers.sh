#!/bin/sh
#Iterate over compilers and optimizations
#NOTE: RUN FROM THE BASE DIRECTORY
##########################################
declare -a icc_opt=("-O1" "-O2" "-O3")
declare -a gcc_opt=("-O1" "-O2" "-O3")

## Change to gcc in Makefile
sed -i "1s/.*/CC=gcc/" src/Makefile
for j in "${gcc_opt[@]}"
do
	#Change Optimization in Makefile
	sed -i "3s/.*/OPT=$j/" src/Makefile
	#Run profiles
	FVAR='output_#gcc'${j}"#.txt"
	echo ${FVAR}
	make > /dev/null
	./bin/total_profile profiles/${FVAR}
	make clean_code
done

## Change to icc in Makefile
sed -i "1s/.*/CC=icc/" src/Makefile
for j in "${icc_opt[@]}"
do
	#Change Optimization in Makefile
	sed -i "3s/.*/OPT=$j/" src/Makefile
	#Run profiles
	FVAR='output_#icc'${j}"#.txt"
	echo ${FVAR}
	make > /dev/null
	./bin/total_profile profiles/${FVAR}
	make clean_code
done
